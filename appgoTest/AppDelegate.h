//
//  AppDelegate.h
//  appgoTest
//
//  Created by Omar Doucouré on 15/02/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "SlideNavigationController.h"
#import "LeftMenuViewController.h"
#import "GAI.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) NSMutableArray *articleData;
@property (strong, nonatomic) NSMutableArray *menuData;
//@property (strong, nonatomic) NSMutableArray *categorieData;
@property (strong, nonatomic) NSMutableArray *vedetteData;
@property (strong, nonatomic) NSMutableArray *pubData;
@property (strong, nonatomic) NSMutableArray *accueilData;
@property (strong, nonatomic) NSMutableArray *senegoTv;
@property (strong, nonatomic) NSMutableArray *buzzData;
@property (strong, nonatomic) NSMutableArray *politiqueData;
@property (strong, nonatomic) NSMutableArray *peopleData;
@property (strong, nonatomic) NSMutableArray *sportsData;
@property (strong, nonatomic) NSMutableArray *afriqueData;
@property (strong, nonatomic) NSMutableArray *internationalData;
@property (strong, nonatomic) NSMutableArray *revuedata;
@property (strong, nonatomic) NSMutableArray *videoData;
@property (strong, nonatomic) NSMutableArray *BuzzDujourData;
@property (strong, nonatomic) NSMutableArray *BuzzSemaineData;
@property (strong, nonatomic) NSMutableArray *pushData;


@property (nonatomic, assign) BOOL launchFromPush;

@property (strong, nonatomic) NSString *idMenuDelegate;


- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

