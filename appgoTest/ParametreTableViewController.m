//
//  ParametreTableViewController.m
//  appgoTest
//
//  Created by Omar Doucouré on 31/03/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import "ParametreTableViewController.h"
#import "LeftMenuViewController.h"
#import "CustomCell.h"
#import "PlayNDropViewController.h"
#import <MaryPopin/UIViewController+MaryPopin.h>

@interface ParametreTableViewController () {
    CustomCell *cell;
}

@end

@implementation ParametreTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.backgroundColor =  [UIColor colorWithRed:0.859 green:0.855 blue:0.827 alpha:1] /*#dbdad3*/;
    self.title=@"Parametres";
    
    
   }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier=@"une";
    
    if (indexPath.section == 0) {
        CellIdentifier=@"propos";
    }
    
    if (indexPath.section==1) {
        CellIdentifier=@"recommander";
    }
    
    if (indexPath.section==2) {
        CellIdentifier=@"nuit";
    }
    
    
    //    [self.tableView reloadData];
    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
   
    if (cell == nil) {
        
        cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    
    if (indexPath.section == 2) {
        UISwitch *switchview = [[UISwitch alloc] initWithFrame:CGRectZero];
        switchview.tag = 111;
        [switchview setOn:YES animated:YES];
        cell.accessoryView = switchview;
        
        [switchview addTarget:self action:@selector(updateSwitchAtIndexPath:) forControlEvents:UIControlEventTouchUpInside];

    }
       
    
    return cell;
}

- (void)updateSwitchAtIndexPath:(UISwitch *)switchview {
    if ([switchview isOn]) {
    
    } else {
        NSLog(@"OFF");

    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        UIViewController *popin = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PlayNDropViewController"];
        popin.view.bounds = CGRectMake(0, 0, 300, 400);
        [popin setPopinTransitionStyle:BKTPopinTransitionStyleSnap];
        //[popin setPopinOptions:BKTPopinDisableAutoDismiss];
        BKTBlurParameters *blurParameters = [[BKTBlurParameters alloc] init];
        //blurParameters.alpha = 0.5;
        blurParameters.tintColor = [UIColor colorWithWhite:0 alpha:0.5];
        blurParameters.radius = 0.3;
        [popin setBlurParameters:blurParameters];
        [popin setPopinTransitionDirection:BKTPopinTransitionDirectionTop];
        //popin.presentingController = self;
        
        //Present popin on the desired controller
        //Note that if you are using a UINavigationController, the navigation bar will be active if you present
        // the popin on the visible controller instead of presenting it on the navigation controller
        [self presentPopinController:popin animated:YES completion:^{
            NSLog(@"Popin presented !");
        }];
        
        if (indexPath.section ==1 ) {

         
        }

    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}



@end
