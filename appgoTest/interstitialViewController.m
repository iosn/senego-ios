//
//  interstitialViewController.m
//  appgoTest
//
//  Created by Omar Doucouré on 28/02/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//
@import GoogleMobileAds;
#define GAD_SIMULATOR_ID @"Simulator"


#import "interstitialViewController.h"
#import "Parser.h"
#import "AccueilTableViewController.h"
#import "Reachability.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FeEqualize.h"
#import "UIColor+flat.h"
#import "AudioToolbox/AudioToolbox.h"
#import <AVFoundation/AVFoundation.h>
#import "AppDelegate.h"
#define CORRECT_IMPLEMENTATION


@interface interstitialViewController () <AVAudioPlayerDelegate>  {
    Reachability *internetReachableFoo;
    AppDelegate *appDelegate;
    
}

#ifdef CORRECT_IMPLEMENTATION
@property (nonatomic, strong) AVAudioPlayer* player;
#endif

@property(nonatomic, strong) DFPInterstitial *interstitial;
@property (strong, nonatomic) FeEqualize *equalizer;


@end

@implementation interstitialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //    self.interstitial = [self createAndLoadInterstitial];
    
    _equalizer = [[FeEqualize alloc] initWithView:self.view title:@"Chargement des données en cours..."];
    _equalizer.frame = CGRectMake(0, self.view.frame.size.height - 400, self.view.frame.size.width, 200);
    [self.view addSubview:_equalizer];
    
    [_equalizer showWhileExecutingBlock:^{
        [self myTask];
    } completion:^{
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }];
 
    
}

- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillDismissScreen");
    
    //[viewAdd removeFromSuperview];
    [self.view removeFromSuperview];
}

- (void)myTask
{
    // Do something usefull in here instead of sleeping ...
    //    [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(loadData) userInfo:nil repeats:NO];
    sleep(0);
    [self loadData];
    //    [self createAndLoadInterstitial];
    
}

-(void)goSecondView{
    
    
    AccueilTableViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AccueilTableViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    [UIView  beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationDuration:1];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.navigationController.view cache:YES];
    [UIView commitAnimations];
    NSString* path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"page-flip-6.wav"];
    NSError* error;
    
#ifdef CORRECT_IMPLEMENTATION
    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:&error];
    self.player.delegate = self;
    self.player.volume=0.2;
    [self.player play];
#else
    AVAudioPlayer* player = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:&error];
    [player play];
#endif
    
}


-(void)refreshTableView: (NSString *)status{
    //    if ([status isEqualToString:@"200"]) {
    //                [ZAActivityBar showSuccessWithStatus:@"Mise à jour avec succès"];
    //
    //    } else if ([status isEqualToString:@"500"]) {
    //        [ZAActivityBar showErrorWithStatus:@"Mise à jour échoué. Vérifiez votre accès internet."];
    //    }
    NSLog(@"ddddd");
    
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    if (launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey]) {
        AccueilTableViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AccueilTableViewController"];
        [self.navigationController pushViewController:vc animated:YES];
        [UIView  beginAnimations:nil context:NULL];
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        [UIView setAnimationDuration:1];
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.navigationController.view cache:YES];
        [UIView commitAnimations];
        NSString* path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"page-flip-6.wav"];
        NSError* error;
        
#ifdef CORRECT_IMPLEMENTATION
        self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:&error];
        self.player.delegate = self;
        self.player.volume=0.2;
        [self.player play];
#else
        AVAudioPlayer* player = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:&error];
        [player play];
#endif
    }
    
    return YES;
}
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadData {
    
    internetReachableFoo = [Reachability reachabilityWithHostname:@"www.senego.com"];
    //    [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(createAndLoadInterstitial) userInfo:nil repeats:NO];
    [internetReachableFoo startNotifier];
    [self goSecondView];

    // Internet is reachable
    internetReachableFoo.reachableBlock = ^(Reachability*reach)
    {
        Parser *dataParser = [[Parser alloc] init];

        // Update the UI on the main thread
//        dispatch_sync(dispatch_get_main_queue(), ^{
            //            [NSTimer scheduledTimerWithTimeInterval:0 target:self selector:@selector(goSecondView) userInfo:nil repeats:NO];
            
                [dataParser getCategorieVedetteAddTarget:self andAction:@selector(refreshTableView:)];
                [dataParser getAccueilArticleAddTarget:self andAction:@selector(refreshTableView:)];
            [dataParser getMenuDataTarget];
//            [dataParser pubData];
            //            self.interstitial = [self createAndLoadInterstitial];
        
            
            NSLog(@"Yayyy, we have the interwebs!");
//        });
        
        [dataParser getVideo];
        [dataParser getSenegoTv];
        [dataParser getBuzz];
        [dataParser getPolitique];
        [dataParser getPeople];
        [dataParser getSports];
        [dataParser getAfrique];
        [dataParser getInternational];
        [dataParser getRevue];
        [dataParser getBuzzDuJour];
        [dataParser getBuzzSemaine];
        
    };
    
    // Internet is not reachable
    internetReachableFoo.unreachableBlock = ^(Reachability*reach)
    {
//        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"coached"]) {
            // On first launch, this block will execute
            
//            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Pas de connexion" message:@"Pour le premier lancement de l'application une connexion internet est obligatoire. Veuillez verifier votre connexion, puis reessayez." delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
//            [alert show];
        
            // Set the "hasPerformedFirstLaunch" key so this block won't execute again
//                   }
        
//        else {
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"coached"];
//            [[NSUserDefaults standardUserDefaults] synchronize];

        // Update the UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"OHHHH qui a arrete le net? :(");
            [self goSecondView];
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Pas de connexion" message:@"Vous n'avez pas accès à internet" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alert show];
            
        });
//        }
    };
    
//    [internetReachableFoo startNotifier];
//

}

//- (GADInterstitial *)createAndLoadInterstitial {
//    GADInterstitial *interstitial = [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-2842182435104673/2527999662"];
////    interstitial.adUnitID = @"ca-app-pub-3940256099942544/4411468910";
//    interstitial.delegate = self;
//    GADRequest *request = [GADRequest request];
//    request.contentURL = @"http://googleadsdeveloper.blogspot.com/2013/10/upgrade-to-new-google-admob.html";
//    [interstitial loadRequest:[GADRequest request]];
//    return interstitial;
//}


- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
//        self.interstitial = [self createAndLoadInterstitial];
}

//- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
////    NSLog(@"interstitialDidDismissScreen");
////    AccueilTableViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AccueilTableViewController"];
////    [self.navigationController pushViewController:vc animated:YES];
//}


- (void)interstitialDidReceiveAd:(GADInterstitial *)interstitial {
    // call presentFromRootViewController here...
//    [self.interstitial presentFromRootViewController:self];
    //    [self dismissViewControllerAnimated:YES completion:NULL];
    
}



@end
