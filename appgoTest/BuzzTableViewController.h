//
//  BuzzTableViewController.h
//  appgoTest
//
//  Created by Omar Doucouré on 02/05/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideNavigationController.h"

@class DFPBannerView;
@class CBStoreHouseRefreshControl;

@interface BuzzTableViewController : UITableViewController <SlideNavigationControllerDelegate,UIToolbarDelegate, UIBarPositioningDelegate,UIScrollViewDelegate>


@property (nonatomic, strong) CBStoreHouseRefreshControl *storeHouseRefreshControl;


@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
//@property(nonatomic, weak) IBOutlet DFPBannerView *bannerView;
@property(nonatomic, weak) IBOutlet DFPBannerView *bannerView2;


@end
