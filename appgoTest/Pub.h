//
//  Pub.h
//  appgoTest
//
//  Created by Omar Doucouré on 01/03/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface Pub : NSManagedObject

@property (nonatomic, retain) NSString * statutPub;
@property (nonatomic, retain) NSString * urlPub;
@property (nonatomic, retain) NSString * urlTarget;


@end
