//
//  People.h
//  appgoTest
//
//  Created by Omar Doucouré on 02/05/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface People : NSManagedObject

@property (nonatomic, retain) NSString * attachmentsUrl;
@property (nonatomic, retain) NSString * authorName;
@property (nonatomic, retain) NSString * categoriesId;
@property (nonatomic, retain) NSString * categoriesTitle;
@property (nonatomic, retain) NSString * commentCount;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSString * date;
@property (nonatomic, retain) NSString * excerpt;
@property (nonatomic, retain) NSString * idArticle;
@property (nonatomic, retain) NSString * modified;
@property (nonatomic, retain) NSString * slug;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * thumbnailImagesVedette;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * title_plain;
@property (nonatomic, retain) NSString * nbrElement;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * urlHtml;

@end
