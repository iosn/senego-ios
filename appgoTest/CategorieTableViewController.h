//
//  CategorieTableViewController.h
//  appgoTest
//
//  Created by Omar Doucouré on 18/02/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategorieTableViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
@property (copy, nonatomic) IBOutlet NSString *categorie;
@property (copy, nonatomic) IBOutlet NSString *Titrecategorie;
@property (copy, nonatomic) IBOutlet NSString *emailPop;

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@end
