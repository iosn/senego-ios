//
//  PostComTableViewController.m
//  appgoTest
//
//  Created by Omar Doucouré on 29/03/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import "PostComTableViewController.h"
#import "SBJson.h"


@interface PostComTableViewController ()

@end

@implementation PostComTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width,450)];
    _scrollView.showsVerticalScrollIndicator=YES;
    _scrollView.scrollEnabled=YES;
    _scrollView.userInteractionEnabled=YES;
    [self.view addSubview:_scrollView];
    _scrollView.contentSize = CGSizeMake(self.view.frame.size.width,550);
    [self.scrollView addSubview:self.nom];
    [self.scrollView addSubview:self.email];
    [self.scrollView addSubview:self.commentaire];
    [self.scrollView addSubview:self.envoyez];
    self.title= @"Postez un commentaire.";
    [self.navigationController.toolbar setHidden:YES];
    //    NSLog(@"postid est %@",self.postId);
    
}

-(IBAction)Postons:(id)sender {
    //    _login.delegate = self;
    //    _passWord.delegate = self;
    
    NSURL *suscribeURL;
    suscribeURL = [NSURL URLWithString:@"http://senego.com/api"];
    NSMutableURLRequest *suscribeRequest = [NSMutableURLRequest requestWithURL:suscribeURL];
    NSString *messageBody = [NSString stringWithFormat:@"json=submit_comment&post_id=%@&name=%@&email=%@&content=%@",self.postId,[self.nom text],[self.email text], [self.commentaire text]];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[messageBody length]];
    [suscribeRequest setHTTPMethod:@"POST"];
    [suscribeRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [suscribeRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [suscribeRequest setHTTPBody:[messageBody dataUsingEncoding:NSUTF8StringEncoding]];
    NSError *error = [[NSError alloc] init];
    NSHTTPURLResponse *response = nil;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:suscribeRequest returningResponse:&response error:&error];
    NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSLog(@"voici la reponse %@",responseData);
    SBJsonParser *jsonParser = [SBJsonParser new];
    NSDictionary *jsonData = (NSDictionary *) [jsonParser objectWithString:responseData error:nil];
    
    if([jsonData isKindOfClass:[NSDictionary class]]) {
        NSLog(@"Dictionary");
    }
    else if([jsonData isKindOfClass:[NSArray class]]) {
        NSLog(@"Array");
    }
    NSLog(@"voici le Json %@",jsonData);
    if ([[jsonData objectForKey:@"status" ] isEqualToString:@"ok"]) {
        NSLog(@"c est bon");
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Succés" message:@"Votre commentaire est en attente d'approbation, un délai de 5 à 10 mn est nécessaire !" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
        
        //        [ZAActivityBar showSuccessWithStatus:@"Votre commentaire est en attente d'approbation, un délai de 5 à 10 mn est nécessaire !"];
        [sender resignFirstResponder];
        [self.navigationController popViewControllerAnimated:YES];
        
        
    }
    
    else if ([[jsonData objectForKey:@"error" ] isEqualToString:@"Please enter a valid email address."]){
        
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Email invalide" message:@"Veuillez saiser une adresse mail valide." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
        
    }
    
    //    else {
    //        NSLog(@"c est pas bon");
    //
    //    }
    
    [self.commentaire setReturnKeyType:UIReturnKeySend];
    [self.nom setReturnKeyType:UIReturnKeySend];
    [self.email setReturnKeyType:UIReturnKeySend];
    
    [self.nom addTarget:self
                 action:@selector(Postons:)
       forControlEvents:UIControlEventEditingDidEndOnExit];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.email addTarget:self
                   action:@selector(Postons:)
         forControlEvents:UIControlEventEditingDidEndOnExit];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    
    
    
    
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.scrollView  endEditing:YES];
    [self.view endEditing:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

@end
