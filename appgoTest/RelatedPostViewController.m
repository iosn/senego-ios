//
//  DetailsArticleViewController.m
//  appgoTest
//
//  Created by Omar Doucouré on 17/02/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//
#import <MessageUI/MessageUI.h>

#import "RelatedPostViewController.h"
#import "AppDelegate.h"
#import "Articles.h"
#import "Vedette.h"
#import "Accueil.h"
#import "CustomCell.h"
#import "DBGHTMLEntityDecoder.h"
#import "UITableView+Wave.h"
#import <FBAudienceNetwork/FBAudienceNetwork.h>
#import "Pub.h"
#import <SDWebImage/UIImageView+WebCache.h>
//#import "FBLikeControl.h"
#import "NSString+HTML.h"
#import <MMMaterialDesignSpinner.h>
#import "RTLabel.h"
#import "CommentaireTableViewController.h"
#import "Video.h"
#import "SenegoTv.h"
#import "PostComTableViewController.h"
#import "Buzz.h"
#import "Politique.h"
#import "People.h"
#import "Sports.h"
#import "Afrique.h"
#import "International.h"
#import "Revue.h"
#import "BuzzDuJour.h"
#import "BuzzSemaine.h"
#import "RemoteImageView.h"
#import "CustomCell.h"
#import "Accueil.h"

@import GoogleMobileAds;

//#import <FBSDKCoreKit/FBSDKCoreKit.h>
//
//#import <FBSDKLoginKit/FBSDKLoginKit.h>
//
//#import <FBSDKShareKit/FBSDKShareKit.h>


@interface RelatedPostViewController ()<FBSDKSharingDelegate,UITableViewDataSource,UITableViewDelegate>{
    AppDelegate *appDelegate;
    AAShareBubbles *shareBubbles;
    float radius;
    float bubbleRadius;
    UIView * bubbleContainer;
    UIWebView *cWbw;
    int fontSize;
    UIImageView* pubWS;
    UIScrollView *container;
    MMMaterialDesignSpinner *spinnerView;
    FBSDKLikeControl *like;
    NSString *output;
    NSString *marginTop;
    DFPBannerView *bannerView;
    NSMutableArray* comArray;
    NSMutableArray* detArticle;
    
    
    
    
}

@property (nonatomic, weak) IBOutlet UILabel *adStatusLabel;
@property (nonatomic, strong) FBAdView *adView;



@end

@implementation RelatedPostViewController
@synthesize index,nbrCommentaire,urlFace;
//@synthesize typeArticle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.detailScrollView.delegate = self;
    NSLog(@"related");
    [self getArticleDetails];
    
    //    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
    //    self.navigationItem.rightBarButtonItem = backButton;
    bannerView = [[DFPBannerView alloc] init];
    bannerView.adUnitID = @"ca-app-pub-2842182435104673/1051266467";
    bannerView.rootViewController = self;
    [bannerView loadRequest:[DFPRequest request]];
    GADRequest *request = [GADRequest request];
    [bannerView loadRequest:request];
    bannerView.frame=CGRectMake(0,self.view.frame.size.height -50, self.view.frame.size.width, 50);
    [self.view addSubview:bannerView];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    self.nbrCommentaire.frame = CGRectMake(0, 0, 30, 40);
    [self.nbrCommentaire setFont:[UIFont boldSystemFontOfSize:14]];
    [self.navigationController.toolbar setHidden:NO];
    [cWbw setDataDetectorTypes:UIDataDetectorTypeNone];
    spinnerView = [[MMMaterialDesignSpinner alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 20, 20, 40, 40)];
    //    spinnerView.backgroundColor= [UIColor redColor];
    // Set the line width of the spinner
    spinnerView.lineWidth = 3.5f;
    // Set the tint color of the spinner
    spinnerView.tintColor = [UIColor redColor];
    //    self.loginView.publishPermissions = @[@"publish_actions"];
    //    NSLog(@"%@",self.idArticle);
    // Add it as a subview
    cWbw.dataDetectorTypes = UIDataDetectorTypeNone;
    
    //    NSLog(@"type %@",self.typeArticle);
    //    cWbw.hidden = YES;
    // Start & stop animations
    [spinnerView startAnimating];
    UIImageView* logoImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo-14-02.png"]];
    //    logoImage.frame= CGRectMake(0, 0, 50, 20);
    self.navigationItem.titleView = logoImage;
    // Do any additional setup after loading the view.
    appDelegate =[[UIApplication sharedApplication] delegate];
    //    self.nbrCommentaire= [[UILabel alloc] init];
    [self loadContent:0];
    [self.view addSubview:self.bottomBar];
    [self.view bringSubviewToFront:self.bottomBar];
    self.bottomBar.backgroundColor =[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1] /*#333333*/;
    
    self.bottomBar.frame= CGRectMake(0, self.view.frame.size.height-145, self.view.frame.size.width, 100);
    self.nbrCommentaire.text =[appDelegate.accueilData [0]commentCount];
    BOOL isIPAD = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad);
    FBAdSize adSize = isIPAD ? kFBAdSizeHeight90Banner : kFBAdSizeHeight50Banner;
    
    self.adView = [[FBAdView alloc] initWithPlacementID:@"132113483494443_798257256880059"
                                                 adSize:adSize
                                     rootViewController:self];
    
    self.adView.delegate = self;
    
    
    // Initiate a request to load an ad.
    [self.adView loadAd];
    UIImage* image3 = [UIImage imageNamed:@"Multiply.png"];
    CGRect frameimg = CGRectMake(0, 0, 30, 30);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(fermer)
         forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    [self.detailScrollView addSubview:cWbw];
}

-(void)fermer{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.y + 650 >= scrollView.contentSize.width) {
        // Add a UIButton
        //        UIButton *shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        //        UIImage *shareBtnImage = [UIImage imageNamed:@"facebook_share@2x.png"]  ;
        //        [shareBtn setBackgroundImage:shareBtnImage forState:UIControlStateNormal];
        //        [shareBtn addTarget:self action:@selector(shareFace) forControlEvents:UIControlEventTouchUpInside];
        //        shareBtn.frame = CGRectMake(self.view.frame.size.width / 2 - 3, self.detailScrollView.contentSize.height - 120, self.view.frame.size.width / 2 + 3, 40);
        //
        //        //    shareBtn.backgroundColor = [UIColor orangeColor];
        //        //    [shareBtn setTitle:@"PARTAGER" forState:(UIControlStateNormal)];
        //        [self.detailScrollView addSubview:shareBtn];
        //        [self.detailScrollView bringSubviewToFront:shareBtn];
        //        shareBtn.layer.zPosition = 4;
        //        UIButton *comBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        //        UIImage *backBtnImage = [UIImage imageNamed:@"comment_button.png"]  ;
        //        [comBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
        //        [comBtn addTarget:self action:@selector(postCom:) forControlEvents:UIControlEventTouchUpInside];
        //        comBtn.frame = CGRectMake(29, self.detailScrollView.contentSize.height - 120, self.view.frame.size.width /2 - 32, 40);
        //        self.nbrCommentaire.frame = CGRectMake(0, self.detailScrollView.contentSize.height - 120, 30, 39);
        //        comBtn.layer.zPosition = 4;
        //        self.nbrCommentaire.layer.zPosition = 4;
        //
        //        [self.detailScrollView addSubview:comBtn];
        //        [self.detailScrollView addSubview:self.nbrCommentaire];
        //        [self.detailScrollView bringSubviewToFront:comBtn];
        //        [self.detailScrollView bringSubviewToFront:self.nbrCommentaire];
        //
        
        
        
        
    }
    
}

//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
//
//
//}
//

-(void)openUrlTarget:(UITapGestureRecognizer *) gestureRecognizer
{
    NSLog(@"url clicke");
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[appDelegate.pubData objectAtIndex:index]urlTarget]]];
}

- (void)adViewDidClick:(FBAdView *)adView
{
    //    NSLog(@"Ad was clicked.");
}

- (void)adViewDidFinishHandlingClick:(FBAdView *)adView
{
    //    NSLog(@"Ad did finish click handling.");
}

- (void)adViewDidLoad:(FBAdView *)adView
{
    //    NSLog(@"Ad was loaded.");
    // Now that the ad was loaded, show the view in case it was hidden before.
    self.adView.hidden = NO;
    [spinnerView stopAnimating];
    [spinnerView removeFromSuperview];
    
}

- (void)adView:(FBAdView *)adView didFailWithError:(NSError *)error
{
    self.adStatusLabel.text = @"Ad failed to load. Check console for details.";
    //    NSLog(@"Ad failed to load with error: %@", error);
    
    // Hide the unit since no ad is shown.
    self.adView.hidden = YES;
}

- (void)adViewWillLogImpression:(FBAdView *)adView
{
    //    NSLog(@"Ad impression is being captured.");
    
    
    
}

- (FBSDKShareLinkContent *)getShareLinkContentWithContentURL:(NSURL *)objectURL
{
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = objectURL;
    return content;
}



- (IBAction)postCom:(id)sender {
    //    cWbw.hidden= YES;
    if ([self.nbrCommentaire.text isEqualToString:@"0"]) {
        PostComTableViewController *post = [self.storyboard instantiateViewControllerWithIdentifier:@"PostComTableViewController"];
        post.postId=self.idArticle;
        [self.navigationController pushViewController:post animated:YES];
        
    }
    
    else {
        
        CommentaireTableViewController * com = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentaireTableViewController"];
        com.index=index;
        //        com.typeArticle= @"Buzz";
        com.titreArticle = self.idArticle;
        com.categorie = self.categorie;
        [self.navigationController pushViewController:com animated:YES];
        
    }
    
    //    [UIView  beginAnimations:nil context:NULL];
    //    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    //    [UIView setAnimationDuration:1.5];
    //    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.navigationController.view cache:NO];
    //    [UIView commitAnimations];
}


-(void)fadeIn:(UIView*)viewToFadeIn withDuration:(NSTimeInterval)duration  andWait:(NSTimeInterval)wait
{
    [UIView beginAnimations: @"Fade In" context:nil];
    
    // wait for time before begin
    [UIView setAnimationDelay:wait];
    
    // druation of animation
    [UIView setAnimationDuration:duration];
    viewToFadeIn.alpha = 1;
    [UIView commitAnimations];
    
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"post"])
    {
        
        if ([self.nbrCommentaire.text isEqualToString:@"0"]) {
            PostComTableViewController *post = [self.storyboard instantiateViewControllerWithIdentifier:@"PostComTableViewController"];
            post.postId= self.idArticle;
            [self.navigationController pushViewController:post animated:YES];
            
        }
        
        else {
            
            CommentaireTableViewController *dvController = [segue destinationViewController];
            dvController.index=index;
            dvController.typeArticle= @"categorie";
            dvController.titreArticle = self.idArticle;
            dvController.categorie = self.categorie;
            [self.navigationController pushViewController:dvController animated:YES];
            
        }
        //        [self.navigationController pushViewController:dvController animated:YES];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 2;
}




- (void)webViewDidFinishLoad:(UIWebView *)aWebView
{
    if (aWebView.tag!=1025) {
        
        
        aWebView.scrollView.scrollEnabled = NO;
        CGRect frame = aWebView.frame;
        frame.size.height=[[aWebView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] floatValue];
        aWebView.frame = frame;
        container=(UIScrollView *)aWebView.superview;
        NSString *range ;
        range =[[detArticle valueForKeyPath:@"post"] valueForKey:@"content"];
        NSLog(@"range = %@",range);
        
        NSError *error = NULL;
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"iframe" options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:range options:0 range:NSMakeRange(0, [range length])];    NSLog(@"Found %lu",(unsigned long)numberOfMatches);
        output = [cWbw stringByEvaluatingJavaScriptFromString:@"document.getElementById(\"omar\").offsetHeight;"];
        
        if ([[range substringToIndex:10] isEqualToString:@"<p><iframe"]) {
            NSLog(@"string does not contain bla");
            if (numberOfMatches == 2) {
                NSLog(@"numberOfMatches = 2");
                
                container.contentSize=CGSizeMake(container.frame.size.width, aWebView.scrollView.contentSize.height );
            }
            else if (numberOfMatches ==4) {
                NSLog(@"numberOfMatches = 4");
                container.contentSize=CGSizeMake(container.frame.size.width, aWebView.scrollView.contentSize.height );
                //
            }
            
        } else {
            container.contentSize=CGSizeMake(container.frame.size.width, aWebView.scrollView.contentSize.height +  [[cWbw stringByEvaluatingJavaScriptFromString:@"document.getElementById(\"omar\").offsetHeight;"] integerValue] );
            
        }
        //        if (frame.size.height ==767.000000) {
        //            container.contentSize=CGSizeMake(container.frame.size.width, aWebView.scrollView.contentSize.height - 80);
        //        }
        //
        //        else {
        
        //        }
        
        //        container.backgroundColor = [UIColor blueColor];
        //        NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'", fontSize];
        //        [cWbw stringByEvaluatingJavaScriptFromString:jsString];
        
        
        cWbw.hidden= NO;
        
        //        [self.detailScrollView bringSubviewToFront:cWbw];
        cWbw.layer.zPosition= 1;
        //        [self.detailScrollView addSubview:self.adView];
        self.adView.layer.zPosition=0;
        self.adView.frame = CGRectMake(8, [output integerValue]+165, self.view.frame.size.width -20, 50);
        [cWbw addSubview:self.adView];
        [cWbw bringSubviewToFront:self.adView];
        marginTop= output;
        RemoteImageView *imageView = [[RemoteImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 150)];
        NSString *articleImage = [NSString stringWithFormat:@"%@", [[[[detArticle valueForKey:@"post"] valueForKey:@"thumbnail_images"] valueForKey:@"full"] valueForKey:@"url"]];
        imageView.imageURL =  [NSURL URLWithString:[articleImage   stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        
        UIImageView *shadow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"product-shadow.png"]];
        shadow.frame= CGRectMake(3, 152, imageView.frame.size.width - 2, 21);
        //        shadow.backgroundColor= [UIColor redColor];
        //        [imageView addSubview:shadow];
        [cWbw addSubview:imageView];
    }
    
    
    
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section

{
    if (section == 0) {
        return nil;
    }
    
    return @"Articles en relation";
}




-(void)shareFace{
    //    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    //    content.contentURL = [NSURL URLWithString:[appDelegate.accueilData [index]urlHtml]];
    //    [FBSDKShareDialog showFromViewController:self
    //                                 withContent:content
    //                                    delegate:nil];
    
    //[FBSDKShareAPI shareWithContent:content delegate:nil];
    
    FBSDKShareDialog *shareDialog = [self getShareDialogWithContentURL:[NSURL URLWithString:[appDelegate.accueilData [index]urlHtml]]];
    shareDialog.delegate = self;
    [shareDialog show];
}

- (FBSDKShareDialog *)getShareDialogWithContentURL:(NSURL *)objectURL
{
    FBSDKShareDialog *shareDialog = [[FBSDKShareDialog alloc] init];
    shareDialog.shareContent = [self getShareLinkContentWithContentURL:objectURL];
    return shareDialog;
}

-(void)loadContent:(int) i{
    for (UIWebView *wb in self.detailScrollView.subviews) {
        [wb removeFromSuperview];
    }
    cWbw= [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.detailScrollView.frame.size.width-13, self.detailScrollView.frame.size.height + 100 ) ];
    cWbw.opaque = YES;
    fontSize=97;
    int imgWidth=self.view.frame.size.width;
    //    UILabel *titre=@"jjj";
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        fontSize=22;
        imgWidth=800;
    }
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    
    
    [cWbw setDataDetectorTypes:UIDataDetectorTypeNone];
    //    NSString*   str3 = [[[appDelegate.accueilData objectAtIndex:index] content] stringByReplacingOccurrencesOfString:@"//www.dailymotion.com" withString:@"https://www.dailymotion.com"];
    [cWbw setScalesPageToFit:NO];
    
    NSString *titlePlain = [[detArticle valueForKeyPath:@"post"]  valueForKey:@"title_plain"];
    NSString *authorName = [[[detArticle valueForKey:@"post"]  valueForKey:@"author"] valueForKey:@"name"];
    NSString *dateArticle = [[detArticle valueForKey:@"post"]  valueForKey:@"date"];
    
    [cWbw loadHTMLString:[NSString stringWithFormat:@"<html>"
                          "<head>"
                          "<style type='text/css'>body {font-family: 'Play'; color:#444444; line-height:26px} img {width:%d; height:auto  } iframe {width:105%%; height:180} .text { margin-top : 56px ; font-size: 18px}</style>"
                          "</head>"
                          "<body><div id='omar'></font><h2 style='line-height:29px; margin-top:160px'><font face='Arial' size='5'>%@</font></h2><font face='Arial' size='2'>Par %@ | le %@</font></div><div class='text'><font face='Arial'>%@</div></body>"
                          "</html>",imgWidth,titlePlain,authorName,dateArticle,[[detArticle valueForKeyPath:@"post"] valueForKey:@"content"]] baseURL:baseURL];
    cWbw.delegate=self;
    //    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style"];
    //    self.detailScrollView.backgroundColor = [UIColor redColor];
    
}


-(void)getArticleDetails{
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://senego.com/api/get_post/?post_id=%@",self.idArticle]]];
    
    id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    detArticle = [[NSMutableArray alloc] init];
    detArticle =  jsonObjects;
    NSLog(@"related article %@",[[jsonObjects valueForKeyPath:@"posts"] valueForKey:@"content"] );
    
}


//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    return 20;
//}


- (IBAction)ChangeFontSize:(id)sender {
    
    switch ([sender tag]) {
        case 0: // A-
            fontSize = (fontSize > 0) ? fontSize - 10 : fontSize;
            
            break;
        case 1: // A+
            fontSize = (fontSize < 160) ? fontSize +10 : fontSize;
            break;
    }
    
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'",fontSize];
    [cWbw stringByEvaluatingJavaScriptFromString:jsString];
    
    CGRect frame = cWbw.frame;
    frame.size.height=[[cWbw stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] floatValue];
    cWbw.frame = frame;
    //    container=(UIScrollView *)cWbw.superview;
    //    container.contentSize=CGSizeMake(container.frame.size.width, cWbw.scrollView.contentSize.height );
    
    
}



-(void)aaShareBubbles:(AAShareBubbles *)shareBubbles tappedBubbleWithType:(int)bubbleType
{
    switch (bubbleType) {
        case AAShareBubbleTypeFacebook:{
            // Put together the dialog parameters
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           [appDelegate.accueilData [index] title_plain], @"name",
                                           [appDelegate.accueilData [index] urlHtml], @"link",
                                           [appDelegate.accueilData [index] attachmentsUrl], @"picture",
                                           nil];
            
            // Show the feed dialog
            //            [FBWebDialogs presentFeedDialogModallyWithSession:nil
            //                                                   parameters:params
            //                                                      handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
            //                                                          if (error) {
            //                                                              // An error occurred, we need to handle the error
            //                                                              // See: https://developers.facebook.com/docs/ios/errors
            //                                                              NSLog(@"Error publishing story: %@", error.description);
            //                                                          } else {
            //                                                              if (result == FBWebDialogResultDialogNotCompleted) {
            //                                                                  // User cancelled.
            //                                                                  NSLog(@"User cancelled.");
            //                                                              }
            //                                                          }
            //                                                      }];
        }
            break;
        case AAShareBubbleTypeTwitter:
            NSLog(@"Twitter");
            break;
        case AAShareBubbleTypeGooglePlus:
            NSLog(@"Google+");
            break;
        case AAShareBubbleTypeTumblr:
            NSLog(@"Tumblr");
            break;
        case AAShareBubbleTypeVk:
            NSLog(@"Vkontakte (vk.com)");
            break;
        case AAShareBubbleTypeLinkedIn:
            NSLog(@"LinkedIn");
            break;
        case AAShareBubbleTypeYoutube:
            NSLog(@"Youtube");
            break;
        case AAShareBubbleTypeVimeo:
            NSLog(@"Vimeo");
            break;
        case AAShareBubbleTypeReddit:
            NSLog(@"Reddit");
            break;
        default:
            break;
    }
}


- (IBAction)shareTapped:(id)sender
{
    if(shareBubbles) {
        shareBubbles = nil;
    }
    bubbleContainer  = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height / 2 - 50, self.view.frame.size.width , 300)];
    bubbleContainer.backgroundColor=[UIColor clearColor];
    [self.view addSubview:bubbleContainer];
    shareBubbles = [[AAShareBubbles alloc] initWithPoint:_shareButton.center radius:radius inView:bubbleContainer];
    shareBubbles.delegate = self;
    //    shareBubbles.bubbleRadius = bubbleRadius;
    shareBubbles.showFacebookBubble = YES;
    shareBubbles.showTwitterBubble = YES;
    shareBubbles.showGooglePlusBubble = YES;
    //    shareBubbles.showTumblrBubble = YES;
    //    shareBubbles.showVkBubble = YES;
    //    shareBubbles.showLinkedInBubble = YES;
    //    shareBubbles.showYoutubeBubble = YES;
    //    shareBubbles.showVimeoBubble = YES;
    //    shareBubbles.showRedditBubble = YES;
    //    shareBubbles.showPinterestBubble = YES;
    shareBubbles.showInstagramBubble = YES;
    //    shareBubbles.showWhatsappBubble = YES;
    
    //    [shareBubbles addCustomButtonWithIcon:[UIImage imageNamed:@"custom-vine-icon"]
    //                          backgroundColor:[UIColor colorWithRed:0.0 green:164.0/255.0 blue:120.0/255.0 alpha:1.0]
    //                              andButtonId:CUSTOM_BUTTON_ID];
    
    [shareBubbles show];
}

-(void)aaShareBubblesDidHide:(AAShareBubbles*)bubbles {
    NSLog(@"All Bubbles hidden");
    bubbleContainer.hidden = YES;
}

-(void)getRelatedArticle{
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://senego.com/relateds/getRelatedPosts.php?url=%@",[appDelegate.accueilData[index] urlHtml]]]];
    
    id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    comArray =  jsonObjects;
    //    NSLog(@"related article %@",[comArray valueForKeyPath:@""]);
    
    
}


@end
