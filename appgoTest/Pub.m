//
//  Pub.m
//  appgoTest
//
//  Created by Omar Doucouré on 01/03/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import "Pub.h"

@implementation Pub

@dynamic statutPub;
@dynamic urlPub;
@dynamic urlTarget;

@end
