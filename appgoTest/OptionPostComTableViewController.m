//
//  OptionPostComTableViewController.m
//  appgoTest
//
//  Created by Omar Doucouré on 30/03/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import "OptionPostComTableViewController.h"
#import "CustomCell.h"
#import "LeftMenuViewController.h"


@interface OptionPostComTableViewController ()

@end

@implementation OptionPostComTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Envoyez nous une info";
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    [self.navigationController.toolbar setHidden:NO];

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier=@"une";
    
    if (indexPath.section == 0) {
        CellIdentifier=@"photo";
    }
    
    if (indexPath.section==1) {
        CellIdentifier=@"video";
    }
    
    if (indexPath.section==2) {
        CellIdentifier=@"texte";
    }
    
    
    //    [self.tableView reloadData];
    CustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    if (cell == nil) {
        cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 130;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
        }
        
        // image picker needs a delegate,
        [imagePickerController setDelegate:self];
        
        // Place image picker on the screen
        [self presentModalViewController:imagePickerController animated:YES];
    }
}

-(IBAction)chooseFromLibrary:(id)sender
{
    
    UIImagePickerController *imagePickerController= [[UIImagePickerController alloc] init];
    [imagePickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    
    // image picker needs a delegate so we can respond to its messages
    [imagePickerController setDelegate:self];
    
    // Place image picker on the screen
    [self presentModalViewController:imagePickerController animated:YES];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    //Zoomed or scrolled image if picker.editing = YES;
    UIImage *editedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    // Original Image
    UIImage *OriginalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    // You can directly use this image but in case you want to store it some where
    NSString *docDirPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath =  [docDirPath stringByAppendingPathComponent:@"myImage.png"];
    NSLog (@"File Path = %@", filePath);
    
    // Get PNG data from following method
    NSData *myData =     UIImagePNGRepresentation(editedImage);
    // It is better to get JPEG data because jpeg data will store the location and other related information of image.
    [myData writeToFile:filePath atomically:YES];
    
    // Now you can use filePath as path of your image. For retrieving the image back from the path
    UIImage *imageFromFile = [UIImage imageWithContentsOfFile:filePath];
    
    
    
//    [self dismissModalViewControllerAnimated:YES];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSLog(@"image == %@",image);
    NSArray * recipients = [NSArray arrayWithObjects:@"infos@senego.com", nil];
    
    //create the MFMailComposeViewController
    MFMailComposeViewController * composer = [[MFMailComposeViewController alloc] init];
    composer.mailComposeDelegate = self;
    [composer setToRecipients:recipients];
    
    //get the filepath from resources
    
    //read the file using NSData
    NSData * fileData = [NSData dataWithContentsOfFile:filePath];

    NSString *mimeType = @"image/png";
    
    //add attachement
    [composer addAttachmentData:fileData mimeType:mimeType fileName:filePath];
    

    [self dismissViewControllerAnimated:YES completion:^{
        [self presentViewController:composer animated:YES completion:NULL];
    }];
//    });

//    [myImageView setImage:image];    // "myImageView" name of any UIImageView.
    NSLog(@"fermee");
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)sendEmail {
    //email subject
    NSString * subject = @"send mail test";
    //email body
    NSString * body = @"How did you find the Android-IOS-Tutorials Website ?";
    //recipient(s)
    NSArray * recipients = [NSArray arrayWithObjects:@"contact@androidiostutorials.com", nil];
    
    //create the MFMailComposeViewController
    MFMailComposeViewController * composer = [[MFMailComposeViewController alloc] init];
    composer.mailComposeDelegate = self;
    [composer setSubject:subject];
    [composer setMessageBody:body isHTML:NO];
    //[composer setMessageBody:body isHTML:YES]; //if you want to send an HTML message
    [composer setToRecipients:recipients];
    
    //get the filepath from resources
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"logo" ofType:@"png"];
    
    //read the file using NSData
    NSData * fileData = [NSData dataWithContentsOfFile:filePath];
    // Set the MIME type
    /*you can use :
     - @"application/msword" for MS Word
     - @"application/vnd.ms-powerpoint" for PowerPoint
     - @"text/html" for HTML file
     - @"application/pdf" for PDF document
     - @"image/jpeg" for JPEG/JPG images
     */
    NSString *mimeType = @"image/png";
    
    //add attachement
    [composer addAttachmentData:fileData mimeType:mimeType fileName:filePath];
    
    //present it on the screen
    
    [self presentViewController:composer animated:YES completion:NULL];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
