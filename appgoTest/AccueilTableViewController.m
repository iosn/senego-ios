//
//  AccueilTableViewController.m
//  appgoTest
//
//  Created by Omar Doucouré on 15/02/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

@import GoogleMobileAds;
#define GAD_SIMULATOR_ID @"Simulator"

#import "AccueilTableViewController.h"
#import "Parser.h"
#import "AppDelegate.h"
#import "Articles.h"
#import "CustomCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DetailsArticleViewController.h"
#import "LeftMenuViewController.h"
#import "Vedette.h"
#import "UITableView+Wave.h"
#import "NSString+HTML.h"
#import "DBGHTMLEntityDecoder.h"
#import "ODRefreshControl.h"
#import "Accueil.h"
#import "RemoteImageView.h"
#import "DetailsPushViewController.h"
#import "BuzzDuJourTableViewController.h"
#import "AccueilTableViewController.h"
#import "VideoTableViewController.h"
#import "SenegoTvTableViewController.h"
#import "BuzzDuJourTableViewController.h"
#import "PolitiqueTableViewController.h"
#import "RevueTableViewController.h"
#import "BuzzSemaineTableViewController.h"
#import "SportsTableViewController.h"
#import "InternationalTableViewController.h"
#import "AfriqueTableViewController.h"
#import "BuzzTableViewController.h"
#import "PeopleTableViewController.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#define kGAIScreenName @"TopNewsViewController"


@interface AccueilTableViewController () <GADInterstitialDelegate>{
    AppDelegate* appDelegate;
    NSString* typeOmar;
    DFPBannerView *bannerView;
    NSURL *imgUrl;
}
@property(nonatomic, strong) GADInterstitial *interstitial;

@end

@implementation AccueilTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:@"Accueil"];
    NSString *savedValueO = [[NSUserDefaults standardUserDefaults]
                             stringForKey:@"accueil"];
    if (!savedValueO) {
        NSLog(@"firstlaunch");
        Parser *dataParser = [[Parser alloc] init];
        dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            // code here
            [dataParser getCategorieVedetteAddTarget:self andAction:@selector(refreshTableView:)];
            [dataParser getAccueilArticleAddTarget:self andAction:@selector(refreshTableView:)];
        });
    }
    else {
        Parser *dataParser = [[Parser alloc] init];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            // code here
            [dataParser getCategorieVedetteAddTarget:self andAction:@selector(refreshTableView:)];
            [dataParser getAccueilArticleAddTarget:self andAction:@selector(refreshTableView:)];
        });
        
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        Parser *dataParser = [[Parser alloc] init];
        NSLog(@"secondfirstlaunch");

        [dataParser getVideo];
        [dataParser getSenegoTv];
        [dataParser getBuzz];
        [dataParser getPolitique];
        [dataParser getPeople];
        [dataParser getSports];
        [dataParser getAfrique];
        [dataParser getInternational];
        [dataParser getRevue];
        [dataParser getBuzzDuJour];
        [dataParser getBuzzSemaine];
        
    });
    
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"fromPush"];
    
    NSString *savedValue2 = [[NSUserDefaults standardUserDefaults]
                             stringForKey:@"categorie"];
    
    NSString *idPush = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"pushId"];
    if ([savedValue isEqualToString:@"true"] && [savedValue2 isEqualToString:@"false"] && ![idPush isEqualToString:@"1"]) {
        //        UIAlertView * alert = [[UIAlertView alloc ] initWithTitle:@"test" message:@"test" delegate:self cancelButtonTitle:nil otherButtonTitles:@"ok", nil];
        //        [alert show];
        DetailsPushViewController * det = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailsPushViewController"];
        det.typeArticle = @"push";
        
        det.idArticle = idPush;
        det.index = 0;
        [self.navigationController pushViewController:det animated:YES];
        
    }
    
    if ([savedValue2 isEqualToString:@"true"] && [savedValue isEqualToString:@"true"]) {
        //        UIAlertView * alert = [[UIAlertView alloc ] initWithTitle:@"test" message:@"je viens d une categorie" delegate:self cancelButtonTitle:nil otherButtonTitles:@"ok", nil];
        //        [alert show];
        
        
        if ([idPush isEqualToString:@"1"]) {
            
            //                AccueilTableViewController * buzz = [self.storyboard  instantiateViewControllerWithIdentifier:@"AccueilTableViewController"];
            //                [self.navigationController pushViewController:buzz animated:YES];
            
        }
        
        else  if ([idPush isEqualToString:@"2"]) {
            VideoTableViewController * buzz = [self.storyboard  instantiateViewControllerWithIdentifier:@"VideoTableViewController"];
            [self.navigationController pushViewController:buzz animated:YES];
            
        }
        
        else  if ([idPush isEqualToString:@"3"]) {
            
            SenegoTvTableViewController * buzz = [self.storyboard  instantiateViewControllerWithIdentifier:@"SenegoTvTableViewController"];
            [self.navigationController pushViewController:buzz animated:YES];
            
        }
        
        else   if ([idPush isEqualToString:@"4"]) {
            
            BuzzTableViewController * buzz = [self.storyboard  instantiateViewControllerWithIdentifier:@"BuzzTableViewController"];
            [self.navigationController pushViewController:buzz animated:YES];
            
        }
        
        
        else  if ([idPush isEqualToString:@"5"]) {
            
            PolitiqueTableViewController * buzz = [self.storyboard  instantiateViewControllerWithIdentifier:@"PolitiqueTableViewController"];
            [self.navigationController pushViewController:buzz animated:YES];
            
        }
        
        else  if ([idPush isEqualToString:@"6"]) {
            
            PeopleTableViewController * buzz = [self.storyboard  instantiateViewControllerWithIdentifier:@"PeopleTableViewController"];
            [self.navigationController pushViewController:buzz animated:YES];
            
        }
        
        else  if ([idPush isEqualToString:@"7"]) {
            
            SportsTableViewController * buzz = [self.storyboard  instantiateViewControllerWithIdentifier:@"SportsTableViewController"];
            [self.navigationController pushViewController:buzz animated:YES];
            
        }
        
        else    if ([idPush isEqualToString:@"8"]) {
            AfriqueTableViewController * buzz = [self.storyboard  instantiateViewControllerWithIdentifier:@"AfriqueTableViewController"];
            [self.navigationController pushViewController:buzz animated:YES];
            
        }
        
        else    if ([idPush isEqualToString:@"9"]) {
            
            InternationalTableViewController * buzz = [self.storyboard  instantiateViewControllerWithIdentifier:@"InternationalTableViewController"];
            [self.navigationController pushViewController:buzz animated:YES];
            
        }
        
        else  if ([idPush isEqualToString:@"10"]) {
            
            RevueTableViewController * buzz = [self.storyboard  instantiateViewControllerWithIdentifier:@"RevueTableViewController"];
            [self.navigationController pushViewController:buzz animated:YES];
            
        }
        
        else   if ([idPush isEqualToString:@"11"]) {
            BuzzDuJourTableViewController * buzz = [self.storyboard  instantiateViewControllerWithIdentifier:@"BuzzDuJourTableViewController"];
            [self.navigationController pushViewController:buzz animated:YES];
            
        }
        
        else     if ([idPush isEqualToString:@"12"]) {
            BuzzSemaineTableViewController * buzz = [self.storyboard instantiateViewControllerWithIdentifier:@"BuzzSemaineTableViewController"];
            [self.navigationController pushViewController:buzz animated:YES];
            
        }
        
        
    }
    
    
    //    ODRefreshControl *refreshControl = [[ODRefreshControl alloc] initInScrollView:self.tableView];
    //    [refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
    appDelegate= [[UIApplication sharedApplication] delegate];
    NSString *accueil = @"Accueil";
    NSPredicate* predicate =[NSPredicate predicateWithFormat:@"categoriesTitle == %@",accueil];
    appDelegate.accueilData=[[NSMutableArray alloc] initWithArray:[Accueil MR_findAllWithPredicate:predicate]];
    FBAdView *adView = [[FBAdView alloc] initWithPlacementID:@"ca-app-pub-2842182435104673/1051266467"
                                                      adSize:kFBAdSizeHeight50Banner
                                          rootViewController:self];
    [adView loadAd];
    [self.view addSubview:adView];
    bannerView = [[DFPBannerView alloc] init];
    bannerView.adUnitID = @"ca-app-pub-2842182435104673/1051266467";
    bannerView.rootViewController = self;
    [bannerView loadRequest:[DFPRequest request]];
    GADRequest *request = [GADRequest request];
    [bannerView loadRequest:request];
    bannerView.frame=CGRectMake(0,self.view.frame.size.height -113, self.view.frame.size.width, 50);
    //    self.navigationController.toolbar.frame = CGRectMake(0, self.view.frame.size.height-50, self.view.frame.size.width, 70);
    [self.interstitial loadRequest:request];
    [self.navigationController.toolbar setHidden:NO];
    [self.view addSubview:bannerView];
    //    [self.navigationController.toolbar addSubview:bannerView];
    self.navigationController.toolbar.backgroundColor=[UIColor redColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]  initWithTitle:@"Retour" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor orangeColor];
    
    //    GADRequest *request;
    //    request.testDevices = @[GAD_SIMULATOR_ID];
    //    [self.bannerView loadRequest:request];
    
    self.navigationController.navigationBar.barTintColor =[UIColor whiteColor] /*#32add6*/;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Play-Bold" size:42],
      NSFontAttributeName, nil]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor orangeColor]};
    //    appDelegate= [[UIApplication sharedApplication] delegate];
    //    appDelegate.articleData= [[NSMutableArray alloc]initWithArray:[Articles MR_findAll] ];
    appDelegate.vedetteData = [[NSMutableArray alloc] initWithArray:[Vedette MR_findAll]];
    //
    typeOmar = @"autres";
    //    [dataParser getMenuDataTarget];
    self.tableView.backgroundColor =  [UIColor colorWithRed:0.859 green:0.855 blue:0.827 alpha:1] /*#dbdad3*/;
    [self.tableView reloadData];
    
    //    self.title = @"SENEGO";
    UIImageView* logoImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo-14-02.png"]];
    self.refreshControl = [UIRefreshControl new];
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Chargement"];
    self.tableView.contentOffset = CGPointMake(0, -self.refreshControl.frame.size.height);
    //    [self.refreshControl beginRefreshing];
    [self.tableView setContentSize:CGSizeMake(self.tableView.frame.size.width,3251)];
    [self.tableView setFrame:CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, 250)];
    [self.tableView setScrollEnabled:YES];
    [self.refreshControl addTarget: self
                            action: @selector( refreshTableView: )
                  forControlEvents: UIControlEventValueChanged];
    
    self.navigationItem.titleView = logoImage;
    
    
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGRect fixedFrame = bannerView.frame;
    fixedFrame.origin.y = self.view.frame.size.height -50 + scrollView.contentOffset.y;
    bannerView.frame = fixedFrame;
}


- (void)dropViewDidBeginRefreshing:(ODRefreshControl *)refreshControl
{
    
    double delayInSeconds =0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        Parser *dataParser = [[Parser alloc] init];
        //    [dataParser getMenuDataTarget];
        [dataParser getAccueilArticleAddTarget:self andAction:@selector(refreshTableView:)];
        [self.tableView reloadDataAnimateWithWave:RightToLeftWaveAnimation];
        [refreshControl endRefreshing];
        
    });
    
    
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    NSLog(@"rotation");
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

//- (BOOL)slideNavigationControllerShouldDisplayRightMenu
//{
//    return YES;
//}

#pragma mark - IBActions -




-(void)refreshTableView: (NSString *)status{
    //    Parser *dataParser = [[Parser alloc] init];
    //    [dataParser getMenuDataTarget];
    //    [dataParser getAccueil];
    
    //    [dataParser categorieVedette];
    //            [dataParser geTotalArticleAddTarget:self andAction:@selector(refreshTableView:)];
    //    [dataParser pubData];
    NSLog(@"je refresh");
    [self.refreshControl endRefreshing];
    
    [self.tableView reloadData];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (section == 1) {
        appDelegate= [[UIApplication sharedApplication] delegate];
        //        appDelegate.articleData= [[NSMutableArray alloc]initWithArray:[Articles MR_findAll] ];
        NSString *accueil = @"Accueil";
        NSPredicate* predicate =[NSPredicate predicateWithFormat:@"categoriesTitle == %@",accueil];
        appDelegate.accueilData=[[NSMutableArray alloc] initWithArray:[Accueil MR_findAllWithPredicate:predicate]];
        //        NSLog(@"nob article %d",appDelegate.categorieData.count);
        return appDelegate.accueilData.count;
        
    }
    
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier=@"une";
    int row = [indexPath row];
    
    if (indexPath.section == 0) {
        CellIdentifier=@"une";
    }
    
    if (indexPath.section==1) {
        CellIdentifier=@"autres";
    }
    
    
    //    [self.tableView reloadData];
    CustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    UIView *separatorTop = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width+10, 8)];
    separatorTop.backgroundColor = [UIColor colorWithRed:0.859 green:0.855 blue:0.827 alpha:1] /*#dbdad3*/;
    UIView *separatorRight = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 10, 0, 10, cell.frame.size.height)];
    separatorRight.backgroundColor = [UIColor colorWithRed:0.859 green:0.855 blue:0.827 alpha:1] /*#dbdad3*/;
    UIView *separatorLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, cell.frame.size.height)];
    separatorLeft.backgroundColor = [UIColor colorWithRed:0.859 green:0.855 blue:0.827 alpha:1] /*#dbdad3*/;
    
    
    // j'appel la classe Custom cell, puis je lui donne un cell Id pour une reutilisation.
    
    if (cell == nil) {
        cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    appDelegate= [[UIApplication sharedApplication] delegate];
    NSString *accueil = @"Accueil";
    NSPredicate* predicate =[NSPredicate predicateWithFormat:@"categoriesTitle == %@",accueil];
    appDelegate.accueilData=[[NSMutableArray alloc] initWithArray:[Accueil MR_findAllWithPredicate:predicate]];
    appDelegate.vedetteData= [[NSMutableArray alloc]initWithArray:[Vedette MR_findAll] ];
    //    NSString * test = [NSString stringWithFormat:@"%@",[[appDelegate.vedetteData objectAtIndex:0] title_plain] ];
    
    if (indexPath.section==0) {
        UIView *  caption = [[UIView alloc] initWithFrame:CGRectMake(0, 170, self.view.frame.size.width, 90) ];
        caption.backgroundColor = [UIColor blackColor];
        
        NSString *encodedString = [[appDelegate.vedetteData objectAtIndex:indexPath.row] title_plain] ;
        DBGHTMLEntityDecoder *decoder = [[DBGHTMLEntityDecoder alloc] init];
        NSString *decodedString = [decoder decodeString:encodedString];
        //        caption.alpha= 0.5;
        //        [cell.img addSubview:caption];
        cell.titre.text=decodedString;
        //        [caption addSubview:cell.titre];
        cell.titre.alpha = 1;
        cell.img.imageURL = [NSURL URLWithString:[[[appDelegate.vedetteData objectAtIndex:0]attachmentsUrl] stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        //
        //         [cell.img sd_setImageWithURL:[NSURL URLWithString:[[appDelegate.vedetteData objectAtIndex:0]attachmentsUrl]]
        //                      placeholderImage:[UIImage imageNamed:@"a1aWNvbnMvMS9mMjdhNDZmOTBhNDAyOGYzMjFjZTc1ZTVjMmZiMDdkMQ=="]
        //                               options:SDWebImageRefreshCached];
        //    NSLog(@"test  %@",[[appDelegate.articleData objectAtIndex:indexPath.row]attachmentsUrl]);
        NSString* date = [appDelegate.vedetteData [indexPath.row] date];
        
        //        cell.img.frame = CGRectMake(18, 14, self.view.frame.size.width-37, cell.contentView.frame.size.height-29);
        cell.auteur.text = [NSString stringWithFormat:@"Par %@ le %@",[appDelegate.vedetteData [0]authorName],[date substringToIndex:13]];
        //        cell.titre.frame = CGRectMake(20, 10, self.view.frame.size.width-30, 50);
        //        cell.contentView.backgroundColor = [UIColor clearColor];
        //        cell.auteur.frame = CGRectMake(20, 65, self.view.frame.size.width-30, 20);
        //        [caption addSubview:cell.auteur];
        
    }
    
    
    if (indexPath.section == 1) {
        [cell.contentView addSubview:separatorLeft];
        [cell.contentView addSubview:separatorRight];
        
        [cell.contentView addSubview:separatorTop];
        
        
        cell.img.imageURL = [NSURL URLWithString:[[[appDelegate.accueilData objectAtIndex:indexPath.row]attachmentsUrl] stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        imgUrl = cell.img.imageURL;
        
        //        [cell.img sd_setImageWithURL:[NSURL URLWithString:[[appDelegate.accueilData objectAtIndex:indexPath.row]attachmentsUrl]]  placeholderImage:[UIImage imageNamed:@"a1aWNvbnMvMS9mMjdhNDZmOTBhNDAyOGYzMjFjZTc1ZTVjMmZiMDdkMQ=="] options:SDWebImageCacheMemoryOnly];
        NSString *encodedString = [[appDelegate.accueilData objectAtIndex:indexPath.row] title_plain] ;
        DBGHTMLEntityDecoder *decoder = [[DBGHTMLEntityDecoder alloc] init];
        NSString *decodedString = [decoder decodeString:encodedString];
        typeOmar=@"autre";
        cell.titre.text=decodedString;
        NSString* date = [appDelegate.accueilData [indexPath.row] date];
        cell.auteur.text=[NSString stringWithFormat:@"Par %@ le %@",[appDelegate.accueilData [indexPath.row]authorName],[date substringToIndex:13]];
        cell.img.alpha = .0f;
        // Then fades it away after 2 seconds (the cross-fade animation will take 0.5s)
        [UIView animateWithDuration:0.5 delay:.0 options:0 animations:^{
            // Animate the alpha value of your imageView from 1.0 to 0.0 here
            cell.img.alpha = 1.0f;
        } completion:^(BOOL finished) {
            // Once the animation is completed and the alpha has gone to 0.0, hide the view for good
            cell.img.hidden = NO;
        }];
        
        cell.categorie.text=[[appDelegate.accueilData objectAtIndex:indexPath.row] categoriesTitle];
        if ([[[appDelegate.accueilData objectAtIndex:indexPath.row] categoriesTitle] isEqualToString:@"Vidéos"]) {
            UIImageView* playerVideo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"play_button.png"]];
            playerVideo.frame= CGRectMake(50, 30, 30, 30);
            [cell.contentView addSubview:playerVideo];
        }
        //        cell.auteur.text= [auteur]
        if (![[[appDelegate.accueilData objectAtIndex:indexPath.row] commentCount] isEqualToString:@"0"]) {
            cell.nbrdecom.text= [[appDelegate.accueilData objectAtIndex:indexPath.row] commentCount];
            [[cell.nbrdecom layer] setCornerRadius:7];
            cell.nbrdecom.layer.masksToBounds = YES;
            
            cell.nbrdecom.hidden= NO;
            cell.iconCom.hidden= NO;
            
        }
        else {
            cell.nbrdecom.hidden= YES;
            cell.iconCom.hidden= YES;
        }
        
        //            NSLog(@"test  %@",[[appDelegate.articleData objectAtIndex:indexPath.row]attachmentsUrl]);
        
        
        
        //        NSLog(@"%@",newString);
        
    }
    
    
    // Configure the cell...
    
    return cell;
    
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    CGFloat height = 50.0; // this should be the height of your admob view
//
//    return height;
//}
//
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    self.bannerView.adUnitID = @"ca-app-pub-2842182435104673/1051266467";
//    self.bannerView.rootViewController = self;
//    [self.bannerView loadRequest:[DFPRequest request]];
//    GADRequest *request = [GADRequest request];
//    [self.bannerView loadRequest:request];
//    UIView *headerView = self.bannerView; // init your view or reference your admob view
//
//    return headerView;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Hauteur de chaque cellule.
    if (indexPath.section == 0) {
        return 226;
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return 200;
    }
    
    
    return 120;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    //1. Setup the CATransform3D structure
    CATransform3D rotation;
    rotation = CATransform3DMakeRotation( (15.0*M_PI)/180, 0.0, 0.7, 0.4);
    CGFloat test =(14.0*M_PI)/180;
    rotation.m34 = 1.0/ 2600;
    
    
    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = rotation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    //!!!FIX for issue #1 Cell position wrong------------
    if(cell.layer.position.x != 0){
        cell.layer.position = CGPointMake(0, cell.layer.position.y);
    }
    
    //4. Define the final state (After the animation) and commit the animation
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.5];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailsArticleViewController *reader = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailsArticleViewController"];
    
    if (indexPath.section == 0) {
        reader.index=indexPath.row;
        reader.typeArticle = @"une";
        
        reader.urlFace.text= [[appDelegate.vedetteData objectAtIndex:indexPath.row] urlHtml];
        NSLog(@"urlface %@",[[appDelegate.vedetteData objectAtIndex:indexPath.row] urlHtml]);
        reader.titreArticle=[[appDelegate.vedetteData objectAtIndex:indexPath.row] title];
        reader.nbrCommentaire.text=[[appDelegate.vedetteData objectAtIndex:indexPath.row] commentCount];
        reader.idArticle = [[appDelegate.vedetteData objectAtIndex:indexPath.row] idArticle];
        NSLog(@"%@",reader.idArticle);
        
        
    }
    else {
        reader.index=indexPath.row;
        reader.typeArticle = @"Accueil";
        reader.imageUrl = imgUrl;
        reader.urlFace.text= [[appDelegate.accueilData objectAtIndex:indexPath.row] urlHtml];
        //    NSLog(@"%@",[[appDelegate.categorieData objectAtIndex:indexPath.row] urlHtml]);
        reader.titreArticle=[[appDelegate.accueilData objectAtIndex:indexPath.row] title];
        reader.nbrCommentaire.text=[[appDelegate.accueilData objectAtIndex:indexPath.row] commentCount];
        reader.idArticle = [[appDelegate.accueilData objectAtIndex:indexPath.row] idArticle];
        NSLog(@"omar %@",[[appDelegate.accueilData objectAtIndex:indexPath.row] idArticle]);
    }
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:reader];
    
    [self.navigationController presentViewController:navBar animated:YES completion:nil];
    
    
    
}





@end
