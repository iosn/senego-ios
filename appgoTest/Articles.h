//
//  Articles.h
//  appgoTest
//
//  Created by Omar Doucouré on 15/02/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface Articles : NSManagedObject

@property (nonatomic, retain) NSString *attachmentsCaption ;
@property (nonatomic, retain) NSString *attachmentsDescription ;
@property (nonatomic, retain) NSString * attachmentsId;
@property (nonatomic, retain) NSString * attachmentsMime;
@property (nonatomic, retain) NSString * attachmentsParent;
@property (nonatomic, retain) NSString * attachmentsSlug;
@property (nonatomic, retain) NSString * attachmentsTitle;
@property (nonatomic, retain) NSString * attachmentsUrl;
@property (nonatomic, retain) NSString * authorDescription;
@property (nonatomic, retain) NSString * authorFistName;
@property (nonatomic, retain) NSString * authorId;
@property (nonatomic, retain) NSString * authorLastName;
@property (nonatomic, retain) NSString * authorName;
@property (nonatomic, retain) NSString *authorNickname ;
@property (nonatomic, retain) NSString * authorSlug;
@property (nonatomic, retain) NSString * authorUrl;
@property (nonatomic, retain) NSString * categories;
@property (nonatomic, retain) NSString * categoriesDescription;
@property (nonatomic, retain) NSString * categoriesId;
@property (nonatomic, retain) NSString * categoriesParent;
@property (nonatomic, retain) NSString * categoriesSlug;
@property (nonatomic, retain) NSString * categoriesTitle;
@property (nonatomic, retain) NSArray * comments;

@property (nonatomic, retain) NSString * commentCount;
@property (nonatomic, retain) NSString * commentsContent;
@property (nonatomic, retain) NSString * commentsDate;
@property (nonatomic, retain) NSString * commentsId;
@property (nonatomic, retain) NSString * commentsName;
@property (nonatomic, retain) NSString * commentsParent;
@property (nonatomic, retain) NSString * commentStatus;
@property (nonatomic, retain) NSString * commentsUrl;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSString * date;
@property (nonatomic, retain) NSString * essbpcfacebook;
@property (nonatomic, retain) NSString * essbpctwitter;
@property (nonatomic, retain) NSString * excerpt;
@property (nonatomic, retain) NSString * idArticle;
@property (nonatomic, retain) NSString * imagesArticleList;
@property (nonatomic, retain) NSString * imagesFeature;
@property (nonatomic, retain) NSString * imagesFull;
@property (nonatomic, retain) NSString * imagesMedium;
@property (nonatomic, retain) NSString * imagesPlusCommente;
@property (nonatomic, retain) NSString * imagesPost;
@property (nonatomic, retain) NSString * imagesThumbList;
@property (nonatomic, retain) NSString * imagesThumbnail;
@property (nonatomic, retain) NSString * imagesVedette;
@property (nonatomic, retain) NSString * modified;
@property (nonatomic, retain) NSString * nextUrl;
@property (nonatomic, retain) NSString * previousUrl;
@property (nonatomic, retain) NSString * slug;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * thumbnail;
@property (nonatomic, retain) NSString * thumbnailImagesArticleListe;
@property (nonatomic, retain) NSString * thumbnailImagesFeatured;
@property (nonatomic, retain) NSString * thumbnailImagesFull;
@property (nonatomic, retain) NSString * thumbnailImagesMedium;
@property (nonatomic, retain) NSString * thumbnailImagesPlusCommente;
@property (nonatomic, retain) NSString * thumbnailImagesPost;
@property (nonatomic, retain) NSString * thumbnailImagesThumbnail;
@property (nonatomic, retain) NSString * thumbnailImagesVedette;
@property (nonatomic, retain) NSString * thumbnailSize;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * title_plain;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * xyzSmap;




@end
