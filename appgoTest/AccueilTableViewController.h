//
//  AccueilTableViewController.h
//  appgoTest
//
//  Created by Omar Doucouré on 15/02/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "GAITrackedViewController.h"


@class DFPBannerView;
@class CBStoreHouseRefreshControl;


@interface AccueilTableViewController : UITableViewController <SlideNavigationControllerDelegate,UIToolbarDelegate, UIBarPositioningDelegate,UIScrollViewDelegate>

@property (nonatomic, strong) CBStoreHouseRefreshControl *storeHouseRefreshControl;
@property (weak, nonatomic) IBOutlet UIView *caption;

@property (nonatomic, strong) IBOutlet UISwitch *limitPanGestureSwitch;
@property (nonatomic, strong) IBOutlet UISwitch *slideOutAnimationSwitch;
@property (nonatomic, strong) IBOutlet UISwitch *shadowSwitch;
@property (nonatomic, strong) IBOutlet UISwitch *panGestureSwitch;
@property (nonatomic, strong) IBOutlet UISegmentedControl *portraitSlideOffsetSegment;
@property (nonatomic, strong) IBOutlet UISegmentedControl *landscapeSlideOffsetSegment;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *ds;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
//@property(nonatomic, weak) IBOutlet DFPBannerView *bannerView;
@property(nonatomic, weak) IBOutlet DFPBannerView *bannerView2;
@property(nonatomic,retain)NSString *screenName;




@end
