//
//  MenuViewController.m
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"
#import "UITableView+Wave.h"
#import "MenuData.h"
#import "AppDelegate.h"
#import "Parser.h"
#import "CategorieTableViewController.h"
#import "CustomCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AccueilTableViewController.h"
#import "OptionPostComTableViewController.h"
#import "ParametreTableViewController.h"
#import "VideoTableViewController.h"
#import "SenegoTvTableViewController.h"
#import "BuzzTableViewController.h"
#import "PolitiqueTableViewController.h"
#import "PeopleTableViewController.h"
#import "SportsTableViewController.h"
#import "AfriqueTableViewController.h"
#import "InternationalTableViewController.h"
#import "RevueTableViewController.h"
#import "BuzzDuJourTableViewController.h"
#import "BuzzSemaineTableViewController.h"
#import "MailViewController.h"


@interface LeftMenuViewController (){
    AppDelegate* appDelegate;
}

@end
@implementation LeftMenuViewController
@synthesize categorie;
#pragma mark - UIViewController Methods -

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self.slideOutAnimationEnabled = YES;
    
    return [super initWithCoder:aDecoder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView reloadDataAnimateWithWave:RightToLeftWaveAnimation];
    [self.tableView reloadData];
    appDelegate= [[UIApplication sharedApplication] delegate];
    appDelegate.menuData= [[NSMutableArray alloc]initWithArray:[MenuData MR_findAll] ];
    //    Parser* dataParser = [[Parser alloc] init];
    //    [dataParser getMenuDataTarget];
    
    self.tableView.separatorColor = [UIColor orangeColor];
    
//    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"leftMenu.jpg"]];
//    self.tableView.backgroundView = imageView;
//    self.tableView.backgroundColor = [UIColor whiteColor] ;
    
    self.view.layer.borderWidth = .6;
    self.view.layer.borderColor = [UIColor orangeColor].CGColor;
}

#pragma mark - UITableView Delegate & Datasrouce -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
//        appDelegate= [[UIApplication sharedApplication] delegate];
//        appDelegate.menuData= [[NSMutableArray alloc]initWithArray:[MenuData MR_findAll] ];
        
        return 12;
    }
    return 1;
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 0)];
//    view.backgroundColor = [UIColor orangeColor];
//    self.tableView.backgroundColor = [UIColor whiteColor] ;
//    return view;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier;
    if (indexPath.section==0) {
        CellIdentifier = @"leftMenuCell";
    }
    if (indexPath.section == 1) {
        CellIdentifier = @"Envoyer";
    }
    if (indexPath.section == 2) {
        CellIdentifier = @"Contacter";
        
    }
    if (indexPath.section == 3) {
        CellIdentifier = @"Parametre";
        
    }
    
    appDelegate.menuData = [[NSMutableArray alloc]initWithArray:@[@"Accueil",@"Vidéos",@"Senego TV",@"Buzz",@"Politique",@"People",@"Sports",@"Afrique",@"International",@"Revue de Presse",@"Buzz du jour",@"Buzz de la semaine"]];
    NSArray * logoMenu = @[@"home1491.png",
                           @"videos.png",
                           @"senego_tv@2x",
                           @"buzz@2x",
                           @"politique@2x",
                           @"people@2x",
                           @"sports@2x.png",
                           @"afrique@2x",
                           @"international@2x",
                           @"revue_presse@2x",
                           @"buzz_jour@2x",
                           @"buzz_semaine@2x"];
    
    CustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (indexPath.section == 0) {
        appDelegate= [[UIApplication sharedApplication] delegate];
//        appDelegate.menuData = [[NSMutableArray alloc]initWithArray:[MenuData MR_findAll]];
        cell.titre.text = appDelegate.menuData [indexPath.row] ;
//        [cell.img sd_setImageWithURL:[NSURL URLWithString:logoMenu[indexPath.row]]  placeholderImage:[UIImage imageNamed:@"loading18.gif"]];
        cell.img.image = [UIImage imageNamed:logoMenu [indexPath.row]];
        
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 55;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section== 1) {
        
        OptionPostComTableViewController* acc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"OptionPostComTableViewController"];
        [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:acc2
                                                              withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                      andCompletion:nil];
    }
    
    if (indexPath.section ==2) {
//        appDelegate.idMenuDelegate = [appDelegate.menuData [indexPath.row] titre];
        
        
        MailViewController* acc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"MailViewController"];
        [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:acc2
                                                              withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                      andCompletion:nil];
        
        
           }
    
    if (indexPath.section == 3) {
        ParametreTableViewController* acc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"ParametreTableViewController"];
        [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:acc2
                                                              withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                      andCompletion:nil];

    }
    

    //    [self performSegueWithIdentifier:@"push" sender:nil];
    
    //	UIViewController *vc;
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(passData) name:@"reload_menu" object:appDelegate.idMenuDelegate];
    if (indexPath.row == 0) {
        AccueilTableViewController* acc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"AccueilTableViewController"];
        [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:acc2
                                                              withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                      andCompletion:nil];
    }
    
    if (indexPath.row == 1) {
        VideoTableViewController* acc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"VideoTableViewController"];
        [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:acc2
                                                              withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                      andCompletion:nil];
    }
    
    if (indexPath.row == 2) {
        SenegoTvTableViewController* acc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"SenegoTvTableViewController"];
        [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:acc2
                                                              withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                      andCompletion:nil];
    }
    
    if (indexPath.row == 3) {
        BuzzTableViewController* acc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"BuzzTableViewController"];
        [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:acc2
                                                              withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                      andCompletion:nil];
    }
    
    if (indexPath.row == 4) {
        PolitiqueTableViewController* acc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"PolitiqueTableViewController"];
        [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:acc2
                                                              withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                      andCompletion:nil];
    }
    
    if (indexPath.row == 5) {
        PeopleTableViewController* acc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"PeopleTableViewController"];
        [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:acc2
                                                              withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                      andCompletion:nil];
    }
    
    if (indexPath.row == 6) {
        SportsTableViewController* acc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"SportsTableViewController"];
        [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:acc2
                                                              withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                      andCompletion:nil];
    }
    
    if (indexPath.row == 7) {
        AfriqueTableViewController* acc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"AfriqueTableViewController"];
        [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:acc2
                                                              withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                      andCompletion:nil];
    }
    
    
    if (indexPath.row == 8) {
        InternationalTableViewController* acc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"InternationalTableViewController"];
        [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:acc2
                                                              withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                      andCompletion:nil];
    }
    
    if (indexPath.row == 9) {
        RevueTableViewController* acc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"RevueTableViewController"];
        [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:acc2
                                                              withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                      andCompletion:nil];
    }

    if (indexPath.row == 10) {
        BuzzDuJourTableViewController* acc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"BuzzDuJourTableViewController"];
        [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:acc2
                                                              withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                      andCompletion:nil];
    }

    if (indexPath.row == 11) {
        BuzzSemaineTableViewController* acc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"BuzzSemaineTableViewController"];
        [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:acc2
                                                              withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                      andCompletion:nil];
    }



    
}


- (void)buttonPressed:(UIButton *)button
{
    // Create image picker controller
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    // Set source to the camera
    imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
    
    // Delegate is self
    imagePicker.delegate = self;
    
    // Allow editing of image ?
    imagePicker.allowsImageEditing = NO;
    
    // Show image picker
    [self presentModalViewController:imagePicker animated:YES];
}


- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            [self dismissModalViewControllerAnimated:YES];
            
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            [self dismissModalViewControllerAnimated:YES];
            
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // Access the uncropped image from info dictionary
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    // Dismiss the camera
    [self dismissModalViewControllerAnimated:YES];
    
    // Pass the image from camera to method that will email the same
    // A delay is needed so camera view can be dismissed
    [self performSelector:@selector(emailImage:) withObject:image afterDelay:1.0];
    
    // Release picker
}



@end
