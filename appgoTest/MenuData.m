//
//  Menu.m
//  appgoTest
//
//  Created by Omar Doucouré on 19/02/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import "MenuData.h"

@implementation MenuData

@dynamic imageMenu;
@dynamic idMenu;
@dynamic titre;
@dynamic urlMenu;

@end
