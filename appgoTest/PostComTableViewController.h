//
//  PostComTableViewController.h
//  appgoTest
//
//  Created by Omar Doucouré on 29/03/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostComTableViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *nom;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSString *postId;

@property (weak, nonatomic) IBOutlet UIButton *envoyez;
@property (weak, nonatomic) IBOutlet UITextView *commentaire;

@end
