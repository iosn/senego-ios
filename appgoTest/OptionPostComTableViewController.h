//
//  OptionPostComTableViewController.h
//  appgoTest
//
//  Created by Omar Doucouré on 30/03/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface OptionPostComTableViewController : UITableViewController <MFMailComposeViewControllerDelegate>

@end
