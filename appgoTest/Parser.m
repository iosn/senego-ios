//
//  Parser.m
//  appgoTest
//
//  Created by Omar Doucouré on 15/02/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import "Parser.h"
//#import "AFJSONRequestOperation.h"
#import "AppDelegate.h"
#import "Articles.h"
#import "MenuData.h"
#import "Accueil.h"
#import "Vedette.h"
#import "Pub.h"
#import "Video.h"
#import "SenegoTv.h"
#import "Buzz.h"
#import "Politique.h"
#import "People.h"
#import "Sports.h"
#import "Afrique.h"
#import "International.h"
#import "Revue.h"
#import "BuzzDuJour.h"
#import "BuzzSemaine.h"


@interface Parser (){
    AppDelegate* appDelegate;
    NSURL *suscribeURL;
    
}

@end

@implementation Parser
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"


-(void)getBuzzSemaine{
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://senego.com/api/semaine/"]];
    if (jsonData == nil) {
        
    }
    else {
        [BuzzSemaine MR_truncateAll];
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        NSManagedObjectContext *localContext2 = [NSManagedObjectContext MR_contextForCurrentThread];
        
        NSMutableArray *tmpData =[[NSMutableArray alloc] init];
        NSString *total = [jsonObjects objectForKey:@"count"];
        for (int i = 0; i < [total intValue]; i++) {
            BuzzSemaine *oneNews = [BuzzSemaine MR_createInContext:localContext2];
            oneNews.attachmentsUrl= [NSString stringWithFormat:@"%@", [[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"full"] valueForKey:@"url"]];
            oneNews.nbrElement= [NSString stringWithFormat:@"%@",total];
            NSDictionary *intId =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"id"];
            NSString* idArticle = [[NSString alloc] initWithFormat:@"%@",intId];
            oneNews.idArticle = idArticle;
            oneNews.type = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"type"];
            oneNews.slug = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"slug"];
            oneNews.urlHtml = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"url"];
            oneNews.status =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"status"];
            oneNews.title =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title"];
            oneNews.title_plain = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title_plain"];
            oneNews.content = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"content"];
            oneNews.excerpt = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"excerpt"];
            oneNews.date = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"date"];
            oneNews.modified = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"modified"];
            NSDictionary *auteur =  [[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"author"];
            oneNews.authorName =  [auteur objectForKey:@"name"];
            oneNews.thumbnailImagesVedette = [NSString stringWithFormat:@"%@",[[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"thumb-list"] valueForKey:@"post-thumb"]];
            id commentCount =[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comment_count"] ;
            NSString* nbrComment = [NSString stringWithFormat:@"%@",commentCount];
            oneNews.commentCount=nbrComment;
            oneNews.categoriesId= @"0";
            oneNews.categoriesTitle =  @"Buzz de la semaine";
            [tmpData addObject:oneNews];
            [localContext2 MR_saveToPersistentStoreAndWait];
            appDelegate.BuzzSemaineData  = [[NSMutableArray alloc]initWithArray:tmpData];
        }
        NSLog(@"buzz semaine finished");
    }
}

-(void)getBuzzDuJour{
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://senego.com/api/vingt_quartre_heures/"]];
    if (jsonData == nil) {
        
    }
    else {
        [BuzzDuJour MR_truncateAll];
        
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        NSManagedObjectContext *localContext2 = [NSManagedObjectContext MR_contextForCurrentThread];
        
        NSMutableArray *tmpData =[[NSMutableArray alloc] init];
        NSString *total = [jsonObjects objectForKey:@"count"];
        for (int i = 0; i < [total intValue]; i++) {
            BuzzDuJour *oneNews = [BuzzDuJour MR_createInContext:localContext2];
            oneNews.attachmentsUrl= [NSString stringWithFormat:@"%@", [[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"full"] valueForKey:@"url"]];        oneNews.nbrElement= [NSString stringWithFormat:@"%@",total];
            NSDictionary *intId =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"id"];
            NSString* idArticle = [[NSString alloc] initWithFormat:@"%@",intId];
            oneNews.idArticle = idArticle;
            oneNews.type = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"type"];
            oneNews.slug = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"slug"];
            oneNews.urlHtml = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"url"];
            oneNews.status =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"status"];
            oneNews.title =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title"];
            oneNews.title_plain = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title_plain"];
            oneNews.content = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"content"];
            oneNews.excerpt = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"excerpt"];
            oneNews.date = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"date"];
            oneNews.modified = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"modified"];
            NSDictionary *auteur =  [[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"author"];
            oneNews.authorName =  [auteur objectForKey:@"name"];
            oneNews.thumbnailImagesVedette = [NSString stringWithFormat:@"%@",[[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"thumb-list"] valueForKey:@"post-thumb"]];
            id commentCount =[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comment_count"] ;
            NSString* nbrComment = [NSString stringWithFormat:@"%@",commentCount];
            oneNews.commentCount=nbrComment;
            oneNews.categoriesId= @"0";
            oneNews.categoriesTitle =  @"Buzz du jour";
            [tmpData addObject:oneNews];
            [localContext2 MR_saveToPersistentStoreAndWait];
            appDelegate.BuzzDujourData  = [[NSMutableArray alloc]initWithArray:tmpData];
        }
        NSLog(@"buzz semaine finished");
    }
}

-(void)getRevue{
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://senego.com/api/get_category_posts/?category_slug=revue-de-presse"]];
    if (jsonData == nil) {
        
    }
    else {
        [Revue MR_truncateAll];
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        NSManagedObjectContext *localContext2 = [NSManagedObjectContext MR_contextForCurrentThread];
        
        NSMutableArray *tmpData =[[NSMutableArray alloc] init];
        NSString *total = [jsonObjects objectForKey:@"count"];
        for (int i = 0; i < [total intValue]; i++) {
            Revue *oneNews = [Revue MR_createInContext:localContext2];
            oneNews.attachmentsUrl= [NSString stringWithFormat:@"%@", [[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"full"] valueForKey:@"url"]];        oneNews.nbrElement= [NSString stringWithFormat:@"%@",total];
            NSDictionary *intId =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"id"];
            NSString* idArticle = [[NSString alloc] initWithFormat:@"%@",intId];
            oneNews.idArticle = idArticle;
            oneNews.type = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"type"];
            oneNews.slug = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"slug"];
            oneNews.urlHtml = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"url"];
            oneNews.status =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"status"];
            oneNews.title =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title"];
            oneNews.title_plain = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title_plain"];
            oneNews.content = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"content"];
            oneNews.excerpt = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"excerpt"];
            oneNews.date = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"date"];
            oneNews.modified = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"modified"];
            NSDictionary *auteur =  [[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"author"];
            oneNews.authorName =  [auteur objectForKey:@"name"];
            oneNews.thumbnailImagesVedette = [NSString stringWithFormat:@"%@",[[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"thumb-list"] valueForKey:@"post-thumb"]];
            id commentCount =[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comment_count"] ;
            NSString* nbrComment = [NSString stringWithFormat:@"%@",commentCount];
            oneNews.commentCount=nbrComment;
            oneNews.categoriesId= @"20";
            oneNews.categoriesTitle =  @"Revue de presse";
            [tmpData addObject:oneNews];
            [localContext2 MR_saveToPersistentStoreAndWait];
            appDelegate.revuedata  = [[NSMutableArray alloc]initWithArray:tmpData];
        }
    }
    NSLog(@"revue finished");
}



-(void)getInternational{
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://senego.com/api/get_category_posts/?category_slug=international"]];
    if (jsonData == nil) {
        
    }
    
    else {
        [International MR_truncateAll];
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        NSManagedObjectContext *localContext2 = [NSManagedObjectContext MR_contextForCurrentThread];
        
        NSMutableArray *tmpData =[[NSMutableArray alloc] init];
        NSString *total = [jsonObjects objectForKey:@"count"];
        for (int i = 0; i < [total intValue]; i++) {
            International *oneNews = [International MR_createInContext:localContext2];
            oneNews.attachmentsUrl= [NSString stringWithFormat:@"%@", [[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"full"] valueForKey:@"url"]];        oneNews.nbrElement= [NSString stringWithFormat:@"%@",total];
            NSDictionary *intId =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"id"];
            NSString* idArticle = [[NSString alloc] initWithFormat:@"%@",intId];
            oneNews.idArticle = idArticle;
            oneNews.type = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"type"];
            oneNews.slug = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"slug"];
            oneNews.urlHtml = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"url"];
            oneNews.status =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"status"];
            oneNews.title =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title"];
            oneNews.title_plain = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title_plain"];
            oneNews.content = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"content"];
            oneNews.excerpt = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"excerpt"];
            oneNews.date = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"date"];
            oneNews.modified = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"modified"];
            NSDictionary *auteur =  [[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"author"];
            oneNews.authorName =  [auteur objectForKey:@"name"];
            oneNews.thumbnailImagesVedette = [NSString stringWithFormat:@"%@",[[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"thumb-list"] valueForKey:@"post-thumb"]];
            id commentCount =[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comment_count"] ;
            NSString* nbrComment = [NSString stringWithFormat:@"%@",commentCount];
            oneNews.commentCount=nbrComment;
            oneNews.categoriesId= @"14";
            oneNews.categoriesTitle =  @"International";
            [tmpData addObject:oneNews];
            [localContext2 MR_saveToPersistentStoreAndWait];
            appDelegate.internationalData  = [[NSMutableArray alloc]initWithArray:tmpData];
        }
    }
    NSLog(@"International finished");
}


-(void)getAfrique{
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://senego.com/api/get_category_posts/?category_slug=afrique"]];
    if (jsonData == nil) {
        
    }
    else {
        [Afrique MR_truncateAll];
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        NSManagedObjectContext *localContext2 = [NSManagedObjectContext MR_contextForCurrentThread];
        
        NSMutableArray *tmpData =[[NSMutableArray alloc] init];
        NSString *total = [jsonObjects objectForKey:@"count"];
        for (int i = 0; i < [total intValue]; i++) {
            Afrique *oneNews = [Afrique MR_createInContext:localContext2];
            oneNews.attachmentsUrl= [NSString stringWithFormat:@"%@", [[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"full"] valueForKey:@"url"]];        oneNews.nbrElement= [NSString stringWithFormat:@"%@",total];
            NSDictionary *intId =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"id"];
            NSString* idArticle = [[NSString alloc] initWithFormat:@"%@",intId];
            oneNews.idArticle = idArticle;
            oneNews.type = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"type"];
            oneNews.slug = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"slug"];
            oneNews.urlHtml = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"url"];
            oneNews.status =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"status"];
            oneNews.title =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title"];
            oneNews.title_plain = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title_plain"];
            oneNews.content = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"content"];
            oneNews.excerpt = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"excerpt"];
            oneNews.date = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"date"];
            oneNews.modified = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"modified"];
            NSDictionary *auteur =  [[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"author"];
            oneNews.authorName =  [auteur objectForKey:@"name"];
            oneNews.thumbnailImagesVedette = [NSString stringWithFormat:@"%@",[[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"thumb-list"] valueForKey:@"post-thumb"]];
            id commentCount =[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comment_count"] ;
            NSString* nbrComment = [NSString stringWithFormat:@"%@",commentCount];
            oneNews.commentCount=nbrComment;
            oneNews.categoriesId= @"3";
            oneNews.categoriesTitle =  @"Afrique";
            [tmpData addObject:oneNews];
            [localContext2 MR_saveToPersistentStoreAndWait];
            appDelegate.afriqueData  = [[NSMutableArray alloc]initWithArray:tmpData];
        }
    }
    NSLog(@"Afrique finished");
    
}


-(void)getSports{
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://senego.com/api/get_category_posts/?category_slug=sport"]];
    if (jsonData == nil) {
        
    }
    else {
        [Sports MR_truncateAll];
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        NSManagedObjectContext *localContext2 = [NSManagedObjectContext MR_contextForCurrentThread];
        
        NSMutableArray *tmpData =[[NSMutableArray alloc] init];
        NSString *total = [jsonObjects objectForKey:@"count"];
        for (int i = 0; i < [total intValue]; i++) {
            Sports *oneNews = [Sports MR_createInContext:localContext2];
            oneNews.attachmentsUrl= [NSString stringWithFormat:@"%@", [[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"full"] valueForKey:@"url"]];        oneNews.nbrElement= [NSString stringWithFormat:@"%@",total];
            NSDictionary *intId =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"id"];
            NSString* idArticle = [[NSString alloc] initWithFormat:@"%@",intId];
            oneNews.idArticle = idArticle;
            oneNews.type = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"type"];
            oneNews.slug = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"slug"];
            oneNews.urlHtml = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"url"];
            oneNews.status =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"status"];
            oneNews.title =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title"];
            oneNews.title_plain = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title_plain"];
            oneNews.content = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"content"];
            oneNews.excerpt = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"excerpt"];
            oneNews.date = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"date"];
            oneNews.modified = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"modified"];
            NSDictionary *auteur =  [[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"author"];
            oneNews.authorName =  [auteur objectForKey:@"name"];
            oneNews.thumbnailImagesVedette = [NSString stringWithFormat:@"%@",[[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"thumb-list"] valueForKey:@"post-thumb"]];
            id commentCount =[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comment_count"] ;
            NSString* nbrComment = [NSString stringWithFormat:@"%@",commentCount];
            oneNews.commentCount=nbrComment;
            oneNews.categoriesId= @"22";
            oneNews.categoriesTitle =  @"Sports";
            [tmpData addObject:oneNews];
            [localContext2 MR_saveToPersistentStoreAndWait];
            appDelegate.sportsData  = [[NSMutableArray alloc]initWithArray:tmpData];
        }
        NSLog(@"Sports finished");
    }
}

-(void)getPeople{
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://senego.com/api/get_category_posts/?category_slug=people"]];
    if (jsonData == nil) {
        
    }
    else {
        [People MR_truncateAll];
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        NSManagedObjectContext *localContext2 = [NSManagedObjectContext MR_contextForCurrentThread];
        
        NSMutableArray *tmpData =[[NSMutableArray alloc] init];
        NSString *total = [jsonObjects objectForKey:@"count"];
        for (int i = 0; i < [total intValue]; i++) {
            People *oneNews = [People MR_createInContext:localContext2];
            oneNews.attachmentsUrl= [NSString stringWithFormat:@"%@", [[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"full"] valueForKey:@"url"]];
            oneNews.nbrElement= [NSString stringWithFormat:@"%@",total];
            NSDictionary *intId =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"id"];
            NSString* idArticle = [[NSString alloc] initWithFormat:@"%@",intId];
            oneNews.idArticle = idArticle;
            oneNews.type = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"type"];
            oneNews.slug = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"slug"];
            oneNews.urlHtml = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"url"];
            oneNews.status =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"status"];
            oneNews.title =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title"];
            oneNews.title_plain = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title_plain"];
            oneNews.content = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"content"];
            oneNews.excerpt = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"excerpt"];
            oneNews.date = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"date"];
            oneNews.modified = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"modified"];
            NSDictionary *auteur =  [[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"author"];
            oneNews.authorName =  [auteur objectForKey:@"name"];
            oneNews.thumbnailImagesVedette = [NSString stringWithFormat:@"%@",[[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"thumb-list"] valueForKey:@"post-thumb"]];
            id commentCount =[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comment_count"] ;
            NSString* nbrComment = [NSString stringWithFormat:@"%@",commentCount];
            oneNews.commentCount=nbrComment;
            oneNews.categoriesId= @"17";
            oneNews.categoriesTitle =  @"People";
            [tmpData addObject:oneNews];
            [localContext2 MR_saveToPersistentStoreAndWait];
            appDelegate.peopleData  = [[NSMutableArray alloc]initWithArray:tmpData];
        }
        NSLog(@"People finished");
    }
    
}

-(void)getPolitique {
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://senego.com/api/get_category_posts/?category_slug=politique"]];
    if (jsonData == nil) {
        
    }
    else {
        [Politique MR_truncateAll];
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        NSManagedObjectContext *localContext2 = [NSManagedObjectContext MR_contextForCurrentThread];
        
        NSMutableArray *tmpData =[[NSMutableArray alloc] init];
        NSString *total = [jsonObjects objectForKey:@"count"];
        for (int i = 0; i < [total intValue]; i++) {
            Politique *oneNews = [Politique MR_createInContext:localContext2];
            oneNews.attachmentsUrl= [NSString stringWithFormat:@"%@", [[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"full"] valueForKey:@"url"]];        oneNews.nbrElement= [NSString stringWithFormat:@"%@",total];
            NSDictionary *intId =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"id"];
            NSString* idArticle = [[NSString alloc] initWithFormat:@"%@",intId];
            oneNews.idArticle = idArticle;
            oneNews.type = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"type"];
            oneNews.slug = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"slug"];
            oneNews.urlHtml = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"url"];
            oneNews.status =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"status"];
            oneNews.title =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title"];
            oneNews.title_plain = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title_plain"];
            oneNews.content = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"content"];
            oneNews.excerpt = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"excerpt"];
            oneNews.date = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"date"];
            oneNews.modified = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"modified"];
            NSDictionary *auteur =  [[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"author"];
            oneNews.authorName =  [auteur objectForKey:@"name"];
            oneNews.thumbnailImagesVedette = [NSString stringWithFormat:@"%@",[[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"thumb-list"] valueForKey:@"post-thumb"]];
            id commentCount =[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comment_count"] ;
            NSString* nbrComment = [NSString stringWithFormat:@"%@",commentCount];
            oneNews.commentCount=nbrComment;
            oneNews.categoriesId= @"18";
            oneNews.categoriesTitle =  @"Politique";
            [tmpData addObject:oneNews];
            [localContext2 MR_saveToPersistentStoreAndWait];
            appDelegate.politiqueData  = [[NSMutableArray alloc]initWithArray:tmpData];
        }
        NSLog(@"Poltique finished");
        
    }
    
}

-(void)getBuzz {
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://senego.com/api/get_category_posts/?category_slug=buzz"]];
    if (jsonData == nil) {
        
    }
    else {
        [Buzz MR_truncateAll];
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        NSManagedObjectContext *localContext2 = [NSManagedObjectContext MR_contextForCurrentThread];
        
        NSMutableArray *tmpData =[[NSMutableArray alloc] init];
        NSString *total = [jsonObjects objectForKey:@"count"];
        for (int i = 0; i < [total intValue]; i++) {
            Buzz *oneNews = [Buzz MR_createInContext:localContext2];
            oneNews.attachmentsUrl= [NSString stringWithFormat:@"%@", [[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"full"] valueForKey:@"url"]];        oneNews.nbrElement= [NSString stringWithFormat:@"%@",total];
            NSDictionary *intId =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"id"];
            NSString* idArticle = [[NSString alloc] initWithFormat:@"%@",intId];
            oneNews.idArticle = idArticle;
            oneNews.type = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"type"];
            oneNews.slug = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"slug"];
            oneNews.urlHtml = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"url"];
            oneNews.status =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"status"];
            oneNews.title =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title"];
            oneNews.title_plain = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title_plain"];
            oneNews.content = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"content"];
            oneNews.excerpt = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"excerpt"];
            oneNews.date = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"date"];
            oneNews.modified = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"modified"];
            NSDictionary *auteur =  [[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"author"];
            oneNews.authorName =  [auteur objectForKey:@"name"];
            oneNews.thumbnailImagesVedette = [NSString stringWithFormat:@"%@",[[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"thumb-list"] valueForKey:@"post-thumb"]];
            id commentCount =[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comment_count"] ;
            NSString* nbrComment = [NSString stringWithFormat:@"%@",commentCount];
            oneNews.commentCount=nbrComment;
            oneNews.categoriesId= @"25";
            oneNews.categoriesTitle =  @"Buzz";
            [tmpData addObject:oneNews];
            [localContext2 MR_saveToPersistentStoreAndWait];
            appDelegate.buzzData  = [[NSMutableArray alloc]initWithArray:tmpData];
        }
        NSLog(@"Buzz finished");
        
    }
    
}

-(void)getCategorieVedetteAddTarget:(id)target andAction:(SEL)action
{
    
    //    UIApplication *application = [UIApplication sharedApplication];
    //
    //    __block UIBackgroundTaskIdentifier bgTask = [application beginBackgroundTaskWithExpirationHandler:^{
    //        [application endBackgroundTask:bgTask];
    //        bgTask = UIBackgroundTaskInvalid;
    //    }];
    //    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
    
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://senego.com/api/get_category_posts/?category_slug=vedette&count=1"]];
    if (jsonData == nil) {
        
    }
    else {
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        NSManagedObjectContext *localContext2 = [NSManagedObjectContext MR_contextForCurrentThread];
        
        NSMutableArray *tmpData =[[NSMutableArray alloc] init];
        [Vedette MR_truncateAll];
        //    NSString *total = [jsonObjects objectForKey:@"count"];
        for (int i = 0; i < 1; i++) {
            Vedette *oneNews = [Vedette MR_createInContext:localContext2];
            NSDictionary *intId =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"id"];
            
            NSString* idArticle = [[NSString alloc] initWithFormat:@"%@",intId];
            oneNews.attachmentsUrl= [NSString stringWithFormat:@"%@", [[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"full"] valueForKey:@"url"]];
            
            //            NSLog(@"%@",contentArticle);
            oneNews.idArticle = idArticle;
            oneNews.type = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"type"];;
            oneNews.slug = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"slug"];;
            //        oneNews.url = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"url"];;
            oneNews.status =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"status"];
            oneNews.title =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title"];
            oneNews.title_plain = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title_plain"];
            oneNews.content = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"content"];
            oneNews.excerpt = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"excerpt"];
            oneNews.date = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"date"];
            oneNews.modified = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"modified"];
            oneNews.comments= [[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comments"] ;
            oneNews.urlHtml = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"url"];
            
            NSDictionary *auteur =  [[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"author"];
            
            NSArray *categorie =  [[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"categories"] valueForKey:@"id"];
            
            oneNews.authorName =  [auteur objectForKey:@"name"];
            oneNews.thumbnailImagesVedette = [NSString stringWithFormat:@"%@",[[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"thumb-list"] valueForKey:@"post-thumb"]];
            id commentCount =[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comment_count"] ;
            NSString* nbrComment = [NSString stringWithFormat:@"%@",commentCount];
            oneNews.commentCount=nbrComment;
            //        oneNews.comments= [[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comments"] ;
            
            //        for (int c = 0; c < categorie.count; c++) {
            //            oneNews.categoriesId= [NSString stringWithFormat:@"%@",categorie[c]];
            //        }
            [tmpData addObject:oneNews];
            [localContext2 MR_saveToPersistentStoreAndWait];
            
        }
        appDelegate.vedetteData  = [[NSMutableArray alloc]initWithArray:tmpData];
        //    } completion:^(BOOL success, NSError *error) {
        //        [application endBackgroundTask:bgTask];
        //        bgTask = UIBackgroundTaskInvalid;
        //    }];
        NSLog(@"vedette finished");
        
    }
    [target performSelector:action withObject:@"200"];
    
}

-(void)getSenegoTv{
    
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://senego.com/api/get_category_posts/?category_slug=senego-tv"]];
    if (jsonData == nil) {
        
    }
    else {
        [SenegoTv MR_truncateAll];
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        NSManagedObjectContext *localContext2 = [NSManagedObjectContext MR_contextForCurrentThread];
        
        NSMutableArray *tmpData =[[NSMutableArray alloc] init];
        NSString *total = [jsonObjects objectForKey:@"count"];
        for (int i = 0; i < [total intValue]; i++) {
            SenegoTv *oneNews = [SenegoTv MR_createInContext:localContext2];
            oneNews.attachmentsUrl= [NSString stringWithFormat:@"%@", [[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"full"] valueForKey:@"url"]];        oneNews.nbrElement= [NSString stringWithFormat:@"%@",total];
            NSDictionary *intId =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"id"];
            NSString* idArticle = [[NSString alloc] initWithFormat:@"%@",intId];
            oneNews.idArticle = idArticle;
            oneNews.type = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"type"];
            oneNews.slug = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"slug"];
            oneNews.urlHtml = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"url"];
            oneNews.status =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"status"];
            oneNews.title =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title"];
            oneNews.title_plain = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title_plain"];
            oneNews.content = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"content"];
            oneNews.excerpt = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"excerpt"];
            oneNews.date = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"date"];
            oneNews.modified = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"modified"];
            NSDictionary *auteur =  [[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"author"];
            oneNews.authorName =  [auteur objectForKey:@"name"];
            oneNews.thumbnailImagesVedette = [NSString stringWithFormat:@"%@",[[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"thumb-list"] valueForKey:@"post-thumb"]];
            id commentCount =[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comment_count"] ;
            NSString* nbrComment = [NSString stringWithFormat:@"%@",commentCount];
            oneNews.commentCount=nbrComment;
            oneNews.categoriesId= @"24";
            oneNews.categoriesTitle =  @"Senego TV";
            [tmpData addObject:oneNews];
            [localContext2 MR_saveToPersistentStoreAndWait];
            appDelegate.senegoTv  = [[NSMutableArray alloc]initWithArray:tmpData];
        }
        NSLog(@"senego tv finished");
    }
    
}

-(void)getVideo{
    
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://senego.com/api/get_category_posts/?category_slug=video"]];
    if (jsonData == nil) {
        
    }
    else {
        [Video MR_truncateAll];
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        NSManagedObjectContext *localContext2 = [NSManagedObjectContext MR_contextForCurrentThread];
        
        NSMutableArray *tmpData =[[NSMutableArray alloc] init];
        NSString *total = [jsonObjects objectForKey:@"count"];
        for (int i = 0; i < [total intValue]; i++) {
            Video *oneNews = [Video MR_createInContext:localContext2];
            oneNews.attachmentsUrl= [NSString stringWithFormat:@"%@", [[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"full"] valueForKey:@"url"]];        oneNews.nbrElement= [NSString stringWithFormat:@"%@",total];
            NSDictionary *intId =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"id"];
            NSString* idArticle = [[NSString alloc] initWithFormat:@"%@",intId];
            oneNews.idArticle = idArticle;
            oneNews.type = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"type"];
            oneNews.slug = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"slug"];
            oneNews.urlHtml = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"url"];
            oneNews.status =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"status"];
            oneNews.title =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title"];
            oneNews.title_plain = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title_plain"];
            oneNews.content = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"content"];
            oneNews.excerpt = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"excerpt"];
            oneNews.date = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"date"];
            oneNews.modified = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"modified"];
            NSDictionary *auteur =  [[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"author"];
            oneNews.authorName =  [auteur objectForKey:@"name"];
            oneNews.thumbnailImagesVedette = [NSString stringWithFormat:@"%@",[[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"thumb-list"] valueForKey:@"post-thumb"]];
            id commentCount =[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comment_count"] ;
            NSString* nbrComment = [NSString stringWithFormat:@"%@",commentCount];
            oneNews.commentCount=nbrComment;
            oneNews.categoriesId= @"24";
            oneNews.categoriesTitle =  @"Video";
            [tmpData addObject:oneNews];
            [localContext2 MR_saveToPersistentStoreAndWait];
            appDelegate.videoData  = [[NSMutableArray alloc]initWithArray:tmpData];
        }
        
        NSLog(@"video finished");
    }
}

-(void)getAccueilArticleAddTarget:(id)target andAction:(SEL)action{
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://senego.com/api/get_recent_posts/"]];
    if (jsonData == nil) {
        
    }
    else {
        [Accueil MR_truncateAll];
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        NSManagedObjectContext *localContext2 = [NSManagedObjectContext MR_contextForCurrentThread];
        //    if ([jsonData isKindOfClass:[NSDictionary class]]) {
        //        NSLog(@"dictionnary");
        //    }
        //    else if ([jsonObjects isKindOfClass:[NSArray class]]){
        //        NSLog(@"array");
        //    }
        //
        NSMutableArray *tmpData =[[NSMutableArray alloc] init];
        NSString *total = [jsonObjects objectForKey:@"count"];
        for (int i = 0; i < [total intValue]; i++) {
            Accueil *oneNews = [Accueil MR_createInContext:localContext2];
            //        NSDictionary *attachement =  [[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] objectForKey:@"full"];
            oneNews.attachmentsUrl= [NSString stringWithFormat:@"%@", [[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"full"] valueForKey:@"url"]];
            
            oneNews.nbrElement= [NSString stringWithFormat:@"%@",total];
            NSDictionary *intId =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"id"];
            NSString* idArticle = [[NSString alloc] initWithFormat:@"%@",intId];
            oneNews.idArticle = idArticle;
            oneNews.type = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"type"];
            oneNews.slug = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"slug"];
            oneNews.urlHtml = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"url"];
            oneNews.status =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"status"];
            oneNews.title =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title"];
            oneNews.title_plain = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title_plain"];
            oneNews.content = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"content"];
            oneNews.excerpt = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"excerpt"];
            oneNews.date = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"date"];
            oneNews.modified = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"modified"];
            oneNews.authorName =  [[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"author"] objectForKey:@"name"];
            oneNews.thumbnailImagesVedette = [NSString stringWithFormat:@"%@",[[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"thumb-list"] valueForKey:@"post-thumb"]];
            id commentCount =[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comment_count"] ;
            NSString* nbrComment = [NSString stringWithFormat:@"%@",commentCount];
            oneNews.commentCount=nbrComment;
            oneNews.categoriesId= @"8615";
            oneNews.categoriesTitle =  @"Accueil";
            [tmpData addObject:oneNews];
            [localContext2 MR_saveToPersistentStoreAndWait];
            appDelegate.accueilData  = [[NSMutableArray alloc]initWithArray:tmpData];
        }
        
        NSLog(@"accueil finished");
    }
    [target performSelector:action withObject:@"200"];
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"accueil"];
    if (!savedValue) {
        NSString *valueToSave = @"YES";
        [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"accueil"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    
}

//-(void)getCategorieAddTarget{
//
//    //    NSLog(@"%@",[appDelegate.menuData [0]urlMenu]);
//    NSMutableArray* menuUrl = [[NSMutableArray alloc] initWithArray:[MenuData MR_findAll]];
//    //    NSLog(@"%@",[controlArray [0]title_plain]);
//    UIApplication *application = [UIApplication sharedApplication];
//
//    __block UIBackgroundTaskIdentifier bgTask = [application beginBackgroundTaskWithExpirationHandler:^{
//        [application endBackgroundTask:bgTask];
//        bgTask = UIBackgroundTaskInvalid;
//    }];
//    //        Categories* cat = [Categories MR_findAll];
//    //        NSLog(@"categorie = %@",cat.title);
//    [Categories MR_truncateAll];
//
//    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
//
//        //    [Categories MR_truncateAll];
//
//        for (int j = 0; j <= 11; j++) {
//            //                    NSLog(@"BOUCLE %@",[menuUrl [j]titre]);
//            //            NSLog(@"index %d",j);
//
//            NSError *error = nil;
//            NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[menuUrl [j]urlMenu]]];
//            NSLog(@"%@",[menuUrl [j]urlMenu]);
//
//            id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
//            NSManagedObjectContext *localContext2 = [NSManagedObjectContext MR_contextForCurrentThread];
//
//            NSMutableArray *tmpData =[[NSMutableArray alloc] init];
//            //            NSLog(@"%@",[menuUrl [j]titre]);
//            NSString *total = [jsonObjects objectForKey:@"count"];
//            //            NSLog(@"total = %@",total);
//            for (int i = 0; i < [total intValue]; i++) {
//                NSMutableArray* article = [[NSMutableArray alloc] initWithArray:[Categories MR_findAll]];
//                //                for (int j = 0; j <= article.count; j++) {
//
//
//                //                if (![[[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title"] isEqualToString:[article objectAtIndex:j]]) {
//
//
//                Categories *oneNews = [Categories MR_createInContext:localContext2];
//                NSArray *attachement =  [[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"post-thumb"];
//                oneNews.attachmentsUrl= [attachement valueForKey:@"url"];
//
//                oneNews.nbrElement= [NSString stringWithFormat:@"%@",total];
//
//
//                NSDictionary *intId =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"id"];
//
//                NSString* idArticle = [[NSString alloc] initWithFormat:@"%@",intId];
//                //                            NSLog(@"%@",contentArticle);
//                oneNews.idArticle = idArticle;
//                oneNews.type = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"type"];
//                oneNews.slug = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"slug"];
//                oneNews.urlHtml = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"url"];
//                oneNews.status =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"status"];
//                oneNews.title =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title"];
//                oneNews.title_plain = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title_plain"];
//                oneNews.content = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"content"];
//                oneNews.excerpt = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"excerpt"];
//                oneNews.date = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"date"];
//                oneNews.modified = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"modified"];
//
//
//                NSDictionary *auteur =  [[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"author"];
//
//                NSArray *categorie =  [[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"categories"] valueForKey:@"id"];
//
//                oneNews.authorName =  [auteur objectForKey:@"name"];
//                oneNews.thumbnailImagesVedette = [[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"thumb-list"] valueForKey:@"post-thumb"];
//                id commentCount =[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comment_count"] ;
//                NSString* nbrComment = [NSString stringWithFormat:@"%@",commentCount];
//                //                NSArray* commentContent=[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comments"] valueForKey:@"content"] ;
//                //                NSArray* commentDate=[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comments"] valueForKey:@"date"] ;
//                //                NSArray* commentName=[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comments"] valueForKey:@"name"] ;
//
//                //                for (int d= 0; d < [commentCount integerValue]; d++) {
//                ////                    NSLog(@"commentaire %ld",(long)[commentCount integerValue]);
//                //                    oneNews.commentsContent=[NSString stringWithFormat:@"%@",commentContent[d]];
//                //                    oneNews.commentsDate=[NSString stringWithFormat:@"%@",commentDate[d]];
//                //            oneNews.comments=[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comments"];
//                //                }
//
//                oneNews.commentCount=nbrComment;
//                //                for (int c = 0; c < categorie.count; c++) {
//                oneNews.categoriesId= [menuUrl [j]idMenu];
//                oneNews.categoriesTitle =  [menuUrl [j]titre];
//
//                //                }
//                //                NSLog(@"titre = %@",oneNews.categoriesTitle);
//
//                [tmpData addObject:oneNews];
//                [localContext2 MR_saveToPersistentStoreAndWait];
//
//                //            }
//                //                NSPredicate* predicate = [NSPredicate predicateWithFormat:@"categoriesTitle == People"];
//                //                NSLog(@"%lu",(unsigned long)[Categories MR_countOfEntities]);
//                //                }
//                //                }
//                appDelegate.categorieData  = [[NSMutableArray alloc]initWithArray:tmpData];
//            }
//            //        }
//        }
//    } completion:^(BOOL success, NSError *error) {
//        [application endBackgroundTask:bgTask];
//        bgTask = UIBackgroundTaskInvalid;
//
//
//    }];
//    //
//}

//-(void)geTotalArticleAddTarget:(id)target andAction:(SEL)action{
//    UIApplication *application = [UIApplication sharedApplication];
//
//    __block UIBackgroundTaskIdentifier bgTask = [application beginBackgroundTaskWithExpirationHandler:^{
//        [application endBackgroundTask:bgTask];
//        bgTask = UIBackgroundTaskInvalid;
//    }];
//    [Categories MR_truncateAll];
//
//    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
//
//
//    NSError *error = nil;
//    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://senego.com/api/get_recent_posts/"]];
//
//    id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
//    NSManagedObjectContext *localContext2 = [NSManagedObjectContext MR_contextForCurrentThread];
//
//    NSMutableArray *tmpData =[[NSMutableArray alloc] init];
//    [Articles MR_truncateAll];
//    NSString *total = [jsonObjects objectForKey:@"count"];
//
//    for (int i = 0; i < [total  intValue]; i++) {
//        Articles *oneNews = [Articles MR_createInContext:localContext2];
//        NSArray *attachement =  [[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"] valueForKey:@"full"];
//        oneNews.thumbnailImagesVedette = [[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"thumbnail_images"]valueForKey:@"thumb-list"] valueForKey:@"url"];
//
//        NSDictionary *intId =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"id"];
//
//        NSString* idArticle = [[NSString alloc] initWithFormat:@"%@",intId];
//        //            NSLog(@"%@",contentArticle);
//        oneNews.idArticle = idArticle;
//        oneNews.type = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"type"];;
//        oneNews.slug = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"slug"];;
//        oneNews.url = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"url"];;
//        oneNews.status =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"status"];
//        oneNews.title =  [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title"];
//        oneNews.title_plain = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"title_plain"];
//        oneNews.content = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"content"];
//        oneNews.excerpt = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"excerpt"];
//        oneNews.date = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"date"];
//        oneNews.modified = [[[jsonObjects valueForKeyPath:@"posts"] objectAtIndex:i] objectForKey:@"modified"];
//
//
//        NSDictionary *auteur =  [[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"author"];
//
//        NSArray *categorieId =  [[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"categories"] valueForKey:@"id"];
//
//    oneNews.attachmentsUrl= [attachement valueForKey:@"url"];
//    oneNews.authorName =  [auteur objectForKey:@"name"];
//    id commentCount =[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comment_count"] ;
//    NSString* nbrComment = [NSString stringWithFormat:@"%@",commentCount];
//    oneNews.comments= [[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"comments"] ;
//
//    oneNews.commentCount=nbrComment;
//
//        for (int c = 0; c < categorieId.count; c++) {
//            oneNews.categoriesId= [NSString stringWithFormat:@"%@",categorieId[c]];
//            oneNews.categoriesTitle =  [[[[[jsonObjects valueForKey:@"posts"] objectAtIndex:i] valueForKey:@"categories"] valueForKey:@"title"] objectAtIndex:c];
//        }
//              [tmpData addObject:oneNews];
//        [localContext2 MR_saveToPersistentStoreAndWait];
//
//    }
//    appDelegate.articleData  = [[NSMutableArray alloc]initWithArray:tmpData];
//
////
//    } completion:^(BOOL success, NSError *error) {
//        [application endBackgroundTask:bgTask];
//        bgTask = UIBackgroundTaskInvalid;
//
//    }];
//
//}

-(void)getMenuDataTarget{
    //    UIApplication *application = [UIApplication sharedApplication];
    //
    //    __block UIBackgroundTaskIdentifier bgTask = [application beginBackgroundTaskWithExpirationHandler:^{
    //        [application endBackgroundTask:bgTask];
    //        bgTask = UIBackgroundTaskInvalid;
    //    }];
    suscribeURL = [NSURL URLWithString:@"http://push.sportiso.com/menu.json"];
    NSMutableURLRequest *suscribeRequest = [NSMutableURLRequest requestWithURL:suscribeURL];
    
    NSError *error = [[NSError alloc] init];
    NSHTTPURLResponse *response = nil;
    
    NSData *urlData=[NSURLConnection sendSynchronousRequest:suscribeRequest returningResponse:&response error:&error];
    
    NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    //    NSLog(@"status = %ld",(long)[response statusCode]);
    //    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
    if ([response statusCode] >=200 && [response statusCode] <300)
    {
        
        [MenuData MR_truncateAll];
        
        NSError *error = nil;
        NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://push.sportiso.com/menu.json"]];
        if (jsonData == nil) {
            
        }
        
        else {
            
            id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
            NSManagedObjectContext *localContext2 = [NSManagedObjectContext MR_contextForCurrentThread];
            NSMutableArray *tmpData =[[NSMutableArray alloc] init];
            
            for (int i = 0; i < [[jsonObjects objectForKey:@"menu"] count]; i++) {
                MenuData *oneNews = [MenuData MR_createInContext:localContext2];
                oneNews.titre = [[[jsonObjects valueForKeyPath:@"menu"] objectAtIndex:i] objectForKey:@"label"];
                id idNumber =[[[jsonObjects valueForKeyPath:@"menu"] objectAtIndex:i] objectForKey:@"id"];
                oneNews.idMenu = [NSString stringWithFormat:@"%@",idNumber];
                oneNews.imageMenu =  [[[jsonObjects valueForKeyPath:@"menu"] objectAtIndex:i] objectForKey:@"pictos"];
                oneNews.urlMenu =  [[[jsonObjects valueForKeyPath:@"menu"] objectAtIndex:i] objectForKey:@"url"];
                //        NSLog(@"%@",oneNews.imageMenu);
                //
                [tmpData addObject:oneNews];
                
                [localContext2 MR_saveToPersistentStoreAndWait];
                
            }
            appDelegate.menuData  = [[NSMutableArray alloc]initWithArray:tmpData];
        }
        
        //    } completion:^(BOOL success, NSError *error) {
        //        [application endBackgroundTask:bgTask];
        //        bgTask = UIBackgroundTaskInvalid;
        //    }];
        //
        
    }
}

-(void)pubData{
    
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://push.sportiso.com/pub.json"]];
    if (jsonData == nil) {
        
    }
    else {
        
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        NSManagedObjectContext *localContext2 = [NSManagedObjectContext MR_contextForCurrentThread];
        NSMutableArray *tmpData =[[NSMutableArray alloc] init];
        [Pub MR_truncateAll];
        Pub *oneNews = [Pub MR_createInContext:localContext2];
        oneNews.statutPub = [jsonObjects valueForKeyPath:@"statut"]  ;
        oneNews.urlPub = [jsonObjects valueForKeyPath:@"image"]  ;
        oneNews.urlTarget = [jsonObjects valueForKeyPath:@"urlTarget"]  ;
        
        //                              NSLog(@"le statut est %@",oneNews.statut);
        //
        [tmpData addObject:oneNews];
        
        [localContext2 MR_saveToPersistentStoreAndWait];
        
        
        appDelegate.pubData  = [[NSMutableArray alloc]initWithArray:tmpData];
        
    }
    
}


@end
