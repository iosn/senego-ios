//
//  BuzzDuJourTableViewController.m
//  appgoTest
//
//  Created by Omar Doucouré on 03/05/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import "BuzzDuJourTableViewController.h"
@import GoogleMobileAds;
#define GAD_SIMULATOR_ID @"Simulator"

#import "Parser.h"
#import "AppDelegate.h"
#import "Sports.h"
#import "CustomCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DetailsArticleViewController.h"
#import "LeftMenuViewController.h"
#import "UITableView+Wave.h"
#import "NSString+HTML.h"
#import "DBGHTMLEntityDecoder.h"
#import "ODRefreshControl.h"
#import "RemoteImageView.h"
#import "BuzzDuJour.h"

@interface BuzzDuJourTableViewController () <GADInterstitialDelegate>{
    AppDelegate* appDelegate;
    NSString* typeOmar;
    DFPBannerView *bannerView;
}
@property(nonatomic, strong) GADInterstitial *interstitial;

@end

@implementation  BuzzDuJourTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    //    [self resizeTable];
    
    ODRefreshControl *refreshControl = [[ODRefreshControl alloc] initInScrollView:self.tableView];
    [refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
    
    appDelegate= [[UIApplication sharedApplication] delegate];
    NSString *accueil = @"Buzz du jour";
    NSPredicate* predicate =[NSPredicate predicateWithFormat:@"categoriesTitle == %@",accueil];
    appDelegate.BuzzDujourData=[[NSMutableArray alloc] initWithArray:[BuzzDuJour MR_findAllWithPredicate:predicate]];
    FBAdView *adView = [[FBAdView alloc] initWithPlacementID:@"ca-app-pub-2842182435104673/1051266467"
                                                      adSize:kFBAdSizeHeight50Banner
                                          rootViewController:self];
    [adView loadAd];
    [self.view addSubview:adView];
    bannerView = [[DFPBannerView alloc] init];
    bannerView.adUnitID = @"ca-app-pub-2842182435104673/1051266467";
    bannerView.rootViewController = self;
    [bannerView loadRequest:[DFPRequest request]];
    GADRequest *request = [GADRequest request];
    [bannerView loadRequest:request];
    bannerView.frame=CGRectMake(0,self.view.frame.size.height -113, self.view.frame.size.width, 50);
    [self.view addSubview:bannerView];
    //    self.navigationController.toolbar.frame = CGRectMake(0, self.view.frame.size.height-50, self.view.frame.size.width, 70);
    [self.interstitial loadRequest:request];

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]  initWithTitle:@"Retour" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor orangeColor];
    
    //    GADRequest *request;
    //    request.testDevices = @[GAD_SIMULATOR_ID];
    //    [self.bannerView loadRequest:request];
    
    self.navigationController.navigationBar.barTintColor =[UIColor whiteColor] /*#32add6*/;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Play-Bold" size:42],
      NSFontAttributeName, nil]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor orangeColor]};
    //    appDelegate= [[UIApplication sharedApplication] delegate];
    //    appDelegate.articleData= [[NSMutableArray alloc]initWithArray:[Articles MR_findAll] ];
    //    appDelegate.vedetteData = [[NSMutableArray alloc] initWithArray:[Vedette MR_findAll]];
    
    //
    typeOmar = @"autres";
    //    [dataParser getMenuDataTarget];
    self.tableView.backgroundColor =  [UIColor colorWithRed:0.859 green:0.855 blue:0.827 alpha:1] /*#dbdad3*/;
    [self.tableView reloadData];
    
    //    self.title = @"SENEGO";
    UIImageView* logoImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo-14-02.png"]];
    //    logoImage.frame= CGRectMake(0, 0, 50, 20);
    self.navigationItem.titleView = logoImage;
    
    
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGRect fixedFrame = bannerView.frame;
    fixedFrame.origin.y = self.view.frame.size.height -50 + scrollView.contentOffset.y;
    bannerView.frame = fixedFrame;
}

- (void)dropViewDidBeginRefreshing:(ODRefreshControl *)refreshControl
{
    
    double delayInSeconds =0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        Parser *dataParser = [[Parser alloc] init];
        //    [dataParser getMenuDataTarget];
        [dataParser getBuzzDuJour];
        [self.tableView reloadDataAnimateWithWave:RightToLeftWaveAnimation];
        [refreshControl endRefreshing];
        
    });
    
    
}



-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}


-(void)refreshTableView: (NSString *)status{
    Parser *dataParser = [[Parser alloc] init];
    [dataParser getBuzzDuJour];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (section == 1) {
        appDelegate= [[UIApplication sharedApplication] delegate];
        //        appDelegate.articleData= [[NSMutableArray alloc]initWithArray:[Articles MR_findAll] ];
        NSString *accueil = @"Buzz du jour";
        NSPredicate* predicate =[NSPredicate predicateWithFormat:@"categoriesTitle == %@",accueil];
        appDelegate.BuzzDujourData=[[NSMutableArray alloc] initWithArray:[BuzzDuJour MR_findAllWithPredicate:predicate]];
        return appDelegate.BuzzDujourData.count-1;
        
    }
    
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier=@"une";
    int row = [indexPath row];
    
    if (indexPath.section == 0) {
        CellIdentifier=@"une";
    }
    
    if (indexPath.section==1) {
        CellIdentifier=@"autres";
    }
    
    CustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    UIView *separatorTop = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width+10, 8)];
    separatorTop.backgroundColor = [UIColor colorWithRed:0.859 green:0.855 blue:0.827 alpha:1] /*#dbdad3*/;
    UIView *separatorRight = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width -10, 0, 10, cell.frame.size.height)];
    separatorRight.backgroundColor = [UIColor colorWithRed:0.859 green:0.855 blue:0.827 alpha:1] /*#dbdad3*/;
    UIView *separatorLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, cell.frame.size.height)];
    separatorLeft.backgroundColor = [UIColor colorWithRed:0.859 green:0.855 blue:0.827 alpha:1] /*#dbdad3*/;
    
    [cell.contentView addSubview:separatorLeft];
    [cell.contentView addSubview:separatorRight];
    
    [cell.contentView addSubview:separatorTop];
    
    if (cell == nil) {
        cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    appDelegate= [[UIApplication sharedApplication] delegate];
    NSString *accueil = @"Buzz du jour";
    NSPredicate* predicate =[NSPredicate predicateWithFormat:@"categoriesTitle == %@",accueil];
    appDelegate.BuzzDujourData=[[NSMutableArray alloc] initWithArray:[BuzzDuJour MR_findAllWithPredicate:predicate]];
    
    if (indexPath.section==0) {
        NSString *encodedString = [[appDelegate.BuzzDujourData objectAtIndex:indexPath.row] title_plain] ;
        DBGHTMLEntityDecoder *decoder = [[DBGHTMLEntityDecoder alloc] init];
        NSString *decodedString = [decoder decodeString:encodedString];
        
        cell.titre.text=decodedString;
        cell.img.imageURL = [NSURL URLWithString:[[[appDelegate.BuzzDujourData objectAtIndex:0]attachmentsUrl] stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]]  ;
        
        
        //        [cell.img sd_setImageWithURL:[NSURL URLWithString:[[appDelegate.sportsData objectAtIndex:0]attachmentsUrl]]  placeholderImage:[UIImage imageNamed:@"a1aWNvbnMvMS9mMjdhNDZmOTBhNDAyOGYzMjFjZTc1ZTVjMmZiMDdkMQ=="]];
        NSString* date = [appDelegate.BuzzDujourData [indexPath.row] date];
        
        cell.auteur.text = [NSString stringWithFormat:@"Par %@ le %@",[appDelegate.BuzzDujourData [0]authorName],[date substringToIndex:13]];
        
    }
    
    
    if (indexPath.section == 1) {
        cell.img.imageURL = [NSURL URLWithString:[[[appDelegate.BuzzDujourData objectAtIndex:indexPath.row +1]attachmentsUrl] stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]]  ;
        
        
        //        [cell.img sd_setImageWithURL:[NSURL URLWithString:[[appDelegate.sportsData objectAtIndex:indexPath.row+1]attachmentsUrl]]  placeholderImage:[UIImage imageNamed:@"a1aWNvbnMvMS9mMjdhNDZmOTBhNDAyOGYzMjFjZTc1ZTVjMmZiMDdkMQ=="]];
        NSString *encodedString = [[appDelegate.BuzzDujourData objectAtIndex:indexPath.row+1] title_plain] ;
        DBGHTMLEntityDecoder *decoder = [[DBGHTMLEntityDecoder alloc] init];
        NSString *decodedString = [decoder decodeString:encodedString];
        typeOmar=@"autre";
        cell.titre.text=decodedString;
        NSString* date = [appDelegate.BuzzDujourData [indexPath.row] date];
        cell.auteur.text=[NSString stringWithFormat:@"Par %@ le %@",[appDelegate.BuzzDujourData [indexPath.row+1]authorName],[date substringToIndex:13]];
        cell.img.alpha = .0f;
        // Then fades it away after 2 seconds (the cross-fade animation will take 0.5s)
        [UIView animateWithDuration:0.5 delay:.0 options:0 animations:^{
            // Animate the alpha value of your imageView from 1.0 to 0.0 here
            cell.img.alpha = 1.0f;
        } completion:^(BOOL finished) {
            // Once the animation is completed and the alpha has gone to 0.0, hide the view for good
            cell.img.hidden = NO;
        }];
        
        if (![[[appDelegate.BuzzDujourData objectAtIndex:indexPath.row+1] commentCount] isEqualToString:@"0"]) {
            cell.nbrdecom.text= [[appDelegate.BuzzDujourData objectAtIndex:indexPath.row] commentCount];
            [[cell.nbrdecom layer] setCornerRadius:7];
            cell.nbrdecom.layer.masksToBounds = YES;
            
            cell.nbrdecom.hidden= NO;
            cell.iconCom.hidden= NO;
            
        }
        else {
            cell.nbrdecom.hidden= YES;
            cell.iconCom.hidden= YES;
        }
        
        
    }
    
    
    // Configure the cell...
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Hauteur de chaque cellule.
    if (indexPath.section == 0) {
        return 271;
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return 200;
    }
    
    
    return 103;
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//
//
//    //1. Setup the CATransform3D structure
//    CATransform3D rotation;
//    rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
//    CGFloat test =(90.0*M_PI)/180;
//    rotation.m34 = 1.0/ -600;
//
//
//    //2. Define the initial state (Before the animation)
//    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
//    cell.layer.shadowOffset = CGSizeMake(10, 10);
//    cell.alpha = 0;
//
//    cell.layer.transform = rotation;
//    cell.layer.anchorPoint = CGPointMake(0, 0.5);
//
//    //!!!FIX for issue #1 Cell position wrong------------
//    if(cell.layer.position.x != 0){
//        cell.layer.position = CGPointMake(0, cell.layer.position.y);
//    }
//
//    //4. Define the final state (After the animation) and commit the animation
//    [UIView beginAnimations:@"rotation" context:NULL];
//    [UIView setAnimationDuration:0.5];
//    cell.layer.transform = CATransform3DIdentity;
//    cell.alpha = 1;
//    cell.layer.shadowOffset = CGSizeMake(0, 0);
//    [UIView commitAnimations];
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailsArticleViewController *reader = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailsArticleViewController"];
    
    if (indexPath.section == 0) {
        reader.index=indexPath.row;
        reader.urlFace.text= [[appDelegate.BuzzDujourData objectAtIndex:indexPath.row] urlHtml];
        //    NSLog(@"%@",[[appDelegate.categorieData objectAtIndex:indexPath.row] urlHtml]);
        reader.titreArticle=[[appDelegate.BuzzDujourData objectAtIndex:indexPath.row] title];
        reader.nbrCommentaire.text=[[appDelegate.BuzzDujourData objectAtIndex:indexPath.row] commentCount];
        reader.idArticle = [[appDelegate.BuzzDujourData objectAtIndex:indexPath.row] idArticle];
        
        
    }
    else {
        reader.index=indexPath.row +1;
        reader.urlFace.text= [[appDelegate.BuzzDujourData objectAtIndex:indexPath.row +1] urlHtml];
        //    NSLog(@"%@",[[appDelegate.categorieData objectAtIndex:indexPath.row] urlHtml]);
        reader.titreArticle=[[appDelegate.BuzzDujourData objectAtIndex:indexPath.row +1] title];
        reader.nbrCommentaire.text=[[appDelegate.BuzzDujourData objectAtIndex:indexPath.row +1] commentCount];
        reader.idArticle = [[appDelegate.BuzzDujourData objectAtIndex:indexPath.row + 1] idArticle];
        
    }
    
    reader.typeArticle = @"Buzz du jour";
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:reader];
    
    [self.navigationController presentViewController:navBar animated:YES completion:nil];
}



@end