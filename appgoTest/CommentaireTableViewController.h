//
//  CommentaireTableViewController.h
//  
//
//  Created by Omar Doucouré on 28/03/2015.
//
//

#import <UIKit/UIKit.h>

@interface CommentaireTableViewController : UITableViewController

@property (retain, nonatomic) IBOutlet UILabel *nbrCommentaire;
@property (weak, nonatomic) IBOutlet NSString *typeArticle;
@property (weak, nonatomic) IBOutlet NSString *categorie;
@property (weak, nonatomic) IBOutlet UIButton *nbrComm;
@property (weak, nonatomic) IBOutlet NSString *titreArticle;

@property (nonatomic) int index;

@end
