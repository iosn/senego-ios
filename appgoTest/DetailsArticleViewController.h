//
//  DetailsArticleViewController.h
//  appgoTest
//
//  Created by Omar Doucouré on 17/02/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AAShareBubbles.h"
#import <FBAudienceNetwork/FBAudienceNetwork.h>
//#import <FacebookSDK/FacebookSDK.h>
//#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import <FBSDKShareKit/FBSDKShareKit.h>
//
//#import <FBSDKShareKit/FBSDKShareKit.h>
@class DFPBannerView;


@interface DetailsArticleViewController : UIViewController <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *detailScrollView;
@property (nonatomic) int index;
@property (weak, nonatomic) IBOutlet NSString *titreArticle;
//@property (nonatomic, strong) IBOutlet FBLoginView *loginView;
@property (weak, nonatomic) IBOutlet NSString *idArticle;
@property (strong, nonatomic) IBOutlet UITableView *customTableView;

@property (weak, nonatomic) IBOutlet UIView *bottomBar;
@property (weak, nonatomic) IBOutlet NSString *typeArticle;
@property (weak, nonatomic) IBOutlet NSString *categorie;
@property (weak, nonatomic) IBOutlet UIButton *nbrComm;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UISlider *bubbleRadiusSlider;
@property (weak, nonatomic) IBOutlet UILabel *radiusLabel;
@property (weak, nonatomic) IBOutlet UILabel *bubbleRadiusLabel;
@property (weak, nonatomic) IBOutlet UIButton *augmenterTexte;
@property (retain, nonatomic) IBOutlet UILabel *nbrCommentaire;
@property (retain, nonatomic) IBOutlet NSURL *imageUrl;

@property (weak, nonatomic) IBOutlet UIButton *postCommentaire;


@property (nonatomic, strong) IBOutlet FBSDKLikeControl *pageLikeControl;


@property (strong, nonatomic) IBOutlet UIImageView *adIconImageView;
@property (strong, nonatomic) IBOutlet UIImageView *adCoverImageView;
@property (strong, nonatomic) IBOutlet UILabel *adTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *adBodyLabel;
@property (strong, nonatomic) IBOutlet UIButton *adCallToActionButton;
@property (strong, nonatomic) IBOutlet UILabel *adSocialContextLabel;
@property (strong, nonatomic) IBOutlet UIView *adStarRatingView;
@property (strong, nonatomic) IBOutlet UILabel *sponsoredLabel;
@property (strong, nonatomic) IBOutlet UILabel *urlFace;


@property (strong, nonatomic) IBOutlet UIView *adUIView;


//@property (weak, nonatomic) IBOutlet UITableView *customTableView;


- (IBAction)shareTapped:(id)sender;

//@property (nonatomic, strong) IBOutlet FBLikeControl *pageLikeControl;


@end
