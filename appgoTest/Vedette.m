//
//  Vedette.m
//  appgoTest
//
//  Created by Omar Doucouré on 23/02/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import "Vedette.h"

@implementation Vedette

@dynamic attachmentsCaption;
@dynamic  attachmentsDescription;
@dynamic  attachmentsId;
@dynamic  attachmentsMime;
@dynamic  attachmentsParent;
@dynamic  attachmentsSlug;
@dynamic  attachmentsTitle;
@dynamic  attachmentsUrl;
@dynamic  authorDescription;
@dynamic  authorFistName;
@dynamic  authorId;
@dynamic  authorLastName;
@dynamic  authorName;
@dynamic  authorNickname;
@dynamic  authorSlug;
@dynamic  authorUrl;
@dynamic  categories;
@dynamic  categoriesDescription;
@dynamic  categoriesId;
@dynamic  categoriesParent;
@dynamic  categoriesSlug;
@dynamic  categoriesTitle;
@dynamic  commentCount;
@dynamic  commentsContent;
@dynamic  commentsDate;
@dynamic  commentsId;
@dynamic  commentsName;
@dynamic  commentsParent;
@dynamic  commentStatus;
@dynamic  commentsUrl;
@dynamic  content;
@dynamic  description;
@dynamic  date;
@dynamic  debugDescription;
@dynamic  deleted;
@dynamic  modified;
@dynamic  nextUrl;
@dynamic  previousUrl;
@dynamic  slug;
@dynamic  status;
@dynamic  essbpcfacebook;
@dynamic  essbpctwitter;
@dynamic  excerpt;
@dynamic  idArticle;
@dynamic  imagesArticleList;
@dynamic  imagesFeature;
@dynamic  imagesFull;
@dynamic  imagesMedium;
@dynamic  imagesPlusCommente;
@dynamic  imagesPost;
@dynamic  imagesThumbList;
@dynamic  imagesThumbnail;
@dynamic  imagesVedette;
@dynamic  thumbnail;
@dynamic  thumbnailImagesArticleListe;
@dynamic  thumbnailImagesFeatured;
@dynamic  thumbnailImagesFull;
@dynamic  thumbnailImagesMedium;
@dynamic  thumbnailImagesPlusCommente;
@dynamic  thumbnailImagesPost;
@dynamic  thumbnailImagesThumbnail;
@dynamic  thumbnailImagesVedette;
@dynamic  thumbnailSize;
@dynamic  title;
@dynamic  title_plain;
@dynamic  type;
@dynamic  urlHtml;
@dynamic  xyzSmap;
@dynamic comments;



@end
