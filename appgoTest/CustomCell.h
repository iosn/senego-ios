//
//  CustomCell.h
//  appgoTest
//
//  Created by Omar Doucouré on 15/02/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RemoteImageView.h"

@interface CustomCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titre;
@property (strong, nonatomic) IBOutlet UILabel *titre2;
@property (strong, nonatomic) IBOutlet UILabel *nbrdecom;
@property (strong, nonatomic) IBOutlet UILabel *categorie;

@property (strong, nonatomic) IBOutlet UILabel *commentaires;
@property (strong, nonatomic) IBOutlet UILabel *auteur;
@property (strong, nonatomic) IBOutlet RemoteImageView *img;
@property (strong, nonatomic) IBOutlet UIImageView *img2;
@property (strong, nonatomic) IBOutlet UIImageView *iconCom;


@end
