//
//  Parser.h
//  appgoTest
//
//  Created by Omar Doucouré on 15/02/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Parser : NSObject

-(void)geTotalArticleAddTarget:(id)target andAction:(SEL)action;
-(void)getMenuDataTarget;
-(void)getCategorieAddTarget;
-(void)getCategorieVedetteAddTarget:(id)target andAction:(SEL)action;
-(void)pubData;
-(void)getAccueilArticleAddTarget:(id)target andAction:(SEL)action;
-(void)getVideo;
-(void)getSenegoTv;
-(void)getBuzz;
-(void)getPolitique;
-(void)getPeople;
-(void)getSports;
-(void)getAfrique;
-(void)getInternational;
-(void)getRevue;
-(void)getBuzzDuJour;
-(void)getBuzzSemaine;








@end
