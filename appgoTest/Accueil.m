//
//  Accueil.m
//  appgoTest
//
//  Created by Omar Doucouré on 20/04/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import "Accueil.h"

@implementation Accueil

@dynamic  attachmentsUrl;
@dynamic authorName;
@dynamic categoriesId;
@dynamic categoriesTitle;
@dynamic commentCount;
@dynamic content;
@dynamic date;
@dynamic excerpt;
@dynamic  idArticle;
@dynamic modified;
@dynamic slug;
@dynamic  status;
@dynamic  thumbnailImagesVedette;
@dynamic title;
@dynamic title_plain;
@dynamic nbrElement;
@dynamic  type;
@dynamic  urlHtml;

@end
