//
//  Menu.h
//  appgoTest
//
//  Created by Omar Doucouré on 19/02/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface MenuData : NSManagedObject

@property (nonatomic, retain) NSString * titre;
@property (nonatomic, retain) NSString * idMenu;
@property (nonatomic, retain) NSString * imageMenu;
@property (nonatomic, retain) NSString * urlMenu;


@end
