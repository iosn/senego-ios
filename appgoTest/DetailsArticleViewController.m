//
//  DetailsArticleViewController.m
//  appgoTest
//
//  Created by Omar Doucouré on 17/02/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//
#import <MessageUI/MessageUI.h>

#import "DetailsArticleViewController.h"
#import "AppDelegate.h"
#import "Articles.h"
#import "Vedette.h"
#import "Accueil.h"
#import "CustomCell.h"
#import "DBGHTMLEntityDecoder.h"
#import "UITableView+Wave.h"
#import <FBAudienceNetwork/FBAudienceNetwork.h>
#import "Pub.h"
#import <SDWebImage/UIImageView+WebCache.h>
//#import "FBLikeControl.h"
#import "NSString+HTML.h"
#import "RTLabel.h"
#import "CommentaireTableViewController.h"
#import "Video.h"
#import "SenegoTv.h"
#import "PostComTableViewController.h"
#import "Buzz.h"
#import "Politique.h"
#import "People.h"
#import "Sports.h"
#import "Afrique.h"
#import "International.h"
#import "Revue.h"
#import "BuzzDuJour.h"
#import "BuzzSemaine.h"
#import "RemoteImageView.h"
#import "CustomCell.h"
#import "Accueil.h"
#import "RelatedPostViewController.h"

@import GoogleMobileAds;

#import <FBSDKCoreKit/FBSDKCoreKit.h>
//
//#import <FBSDKLoginKit/FBSDKLoginKit.h>
//
#import <FBSDKShareKit/FBSDKShareKit.h>


@interface DetailsArticleViewController ()<FBSDKSharingDelegate,UITableViewDataSource,UITableViewDelegate>{
    AppDelegate *appDelegate;
    float radius;
    float bubbleRadius;
    UIView * bubbleContainer;
    UIWebView *cWbw;
    int fontSize;
    UIImageView* pubWS;
    UIScrollView *container;
    FBSDKLikeControl *like;
    NSString *output;
    NSString *marginTop;
    DFPBannerView *bannerView;
    NSMutableArray* comArray;
    NSMutableArray* detArticle;
    
    
    
    
}

@property (nonatomic, weak) IBOutlet UILabel *adStatusLabel;
@property (nonatomic, strong) FBAdView *adView;



@end

@implementation DetailsArticleViewController
@synthesize index,nbrCommentaire,urlFace;
//@synthesize typeArticle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.customTableView.dataSource = self;
        self.customTableView.delegate = self;
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    //    content.contentURL = [NSURL
    //                          URLWithString:@"https://www.facebook.com/FacebookDevelopers"];
    //    FBSDKShareButton *shareButton = [[FBSDKShareButton alloc] init];
    //    shareButton.shareContent = content;
    //    shareButton.center = self.view.center;
    //    [self.view addSubview:shareButton];
    
    self.customTableView.hidden = YES;
    self.detailScrollView.delegate = self;
    bannerView = [[DFPBannerView alloc] init];
    bannerView.adUnitID = @"ca-app-pub-2842182435104673/1051266467";
    bannerView.rootViewController = self;
    [bannerView loadRequest:[DFPRequest request]];
    GADRequest *request = [GADRequest request];
    [bannerView loadRequest:request];
    bannerView.frame=CGRectMake(0,self.view.frame.size.height -50, self.view.frame.size.width, 50);
    [self.view addSubview:bannerView];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    self.nbrCommentaire.frame = CGRectMake(0, 0, 30, 40);
    [self.nbrCommentaire setFont:[UIFont boldSystemFontOfSize:14]];
    [self.navigationController.toolbar setHidden:NO];
    [cWbw setDataDetectorTypes:UIDataDetectorTypeNone];
    cWbw.dataDetectorTypes = UIDataDetectorTypeNone;
    
    UIImageView* logoImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo-14-02.png"]];
    //    logoImage.frame= CGRectMake(0, 0, 50, 20);
    self.navigationItem.titleView = logoImage;
    // Do any additional setup after loading the view.
    appDelegate =[[UIApplication sharedApplication] delegate];
    //    self.nbrCommentaire= [[UILabel alloc] init];
    [self loadContent:0];
    [self.view addSubview:self.bottomBar];
    [self.view bringSubviewToFront:self.bottomBar];
    self.bottomBar.backgroundColor =[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1] /*#333333*/;
    
    self.bottomBar.frame= CGRectMake(0, self.view.frame.size.height-145, self.view.frame.size.width, 100);
    radius = 100;
    bubbleRadius = 20;
    //    _radiusSlider.value = radius;
    _bubbleRadiusSlider.value = bubbleRadius;
    self.nbrCommentaire.text =[appDelegate.accueilData [index]commentCount];
    BOOL isIPAD = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad);
    FBAdSize adSize = isIPAD ? kFBAdSizeHeight90Banner : kFBAdSizeHeight50Banner;
    
    self.adView = [[FBAdView alloc] initWithPlacementID:@"132113483494443_798257256880059"
                                                 adSize:adSize
                                     rootViewController:self];
    
    self.adView.delegate = self;
    
    
    // Initiate a request to load an ad.
    [self.adView loadAd];
    
    // Reposition the adView to the bottom of the screen
    UIImage* image3 = [UIImage imageNamed:@"Multiply.png"];
    CGRect frameimg = CGRectMake(0, 0, 30, 30);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(fermer)
         forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    
    
    // Add adView to the view hierarchy.
    
    appDelegate =[[UIApplication sharedApplication] delegate];
    appDelegate.pubData = [[NSMutableArray alloc] initWithArray:[Pub MR_findAll]];
    //    NSLog(@"url est de %@",[[appDelegate.pubData objectAtIndex:0]urlPub]);
    
    pubWS = [[UIImageView alloc] initWithFrame:CGRectMake(8, 430, 300, 250)];
    //    if ([[[appDelegate.pubData objectAtIndex:0]statutPub] isEqualToString:@"1"]) {
    //        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self    action:@selector(openUrlTarget:)];
    //        pubWS.userInteractionEnabled = YES;
    //        [pubWS addGestureRecognizer:tap];
    //        [cWbw bringSubviewToFront:pubWS];
    //        pubWS.layer.zPosition= 3;
    //        [cWbw addSubview:pubWS];
    //        [self.adView removeFromSuperview];
    //        //        pubWS.backgroundColor= [UIColor redColor];
    //
    //        [pubWS sd_setImageWithURL:[NSURL URLWithString:[[appDelegate.pubData objectAtIndex:0]urlPub]]  placeholderImage:[UIImage imageNamed:@"loading18.gif"]];
    //
    //    }
    
    
    
    
    self.adStatusLabel.text = @"Loading an ad...";
    
    [self.detailScrollView addSubview:cWbw];
    [self getRelatedArticle];
}

-(void)fermer{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"HistoryCell";
    
    if (indexPath.section == 0) {
        cellIdentifier = @"like";
    }
    
    if (indexPath.section == 1) {
        cellIdentifier = @"HistoryCell";
    }
    
    CustomCell *cell = (CustomCell *)[theTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    if (indexPath.section == 0) {
        UIButton *shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *shareBtnImage = [UIImage imageNamed:@"facebook_share@2x.png"]  ;
        [shareBtn setBackgroundImage:shareBtnImage forState:UIControlStateNormal];
        [shareBtn addTarget:self action:@selector(shareFace) forControlEvents:UIControlEventTouchUpInside];
        if (IS_IPHONE_6 || IS_IPHONE_6_PLUS) {
            shareBtn.frame = CGRectMake(202, 5, self.view.frame.size.width / 2 -27, 30);
        }
        else shareBtn.frame = CGRectMake(180, 5, self.view.frame.size.width / 2 -27, 30);
        shareBtn.layer.zPosition = 4;
        
        
        UIButton *tweetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *tweetBtnImage = [UIImage imageNamed:@"Twitter_Logo_Hd_Png_03.png"]  ;
        [tweetBtn setBackgroundImage:tweetBtnImage forState:UIControlStateNormal];
        [tweetBtn addTarget:self action:@selector(shareFace) forControlEvents:UIControlEventTouchUpInside];
        if (IS_IPHONE_6 || IS_IPHONE_6_PLUS) {
            tweetBtn.frame = CGRectMake(163, 5, 30, 30);
            
        }
        else tweetBtn.frame = CGRectMake(139, 5, 30, 30);
        tweetBtn.layer.zPosition = 4;
        
        
        UIButton *comBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        comBtn.backgroundColor  = [UIColor orangeColor];
        //        UIImage *backBtnImage = [UIImage imageNamed:@"comment_button.png"];
        //        [comBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
        //        comBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [comBtn setTitle:@"Commenter" forState:UIControlStateNormal];
        [comBtn addTarget:self action:@selector(postCom:) forControlEvents:UIControlEventTouchUpInside];
        comBtn.frame = CGRectMake(9, 5, self.view.frame.size.width /2 - 42, 30);
        comBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        
        self.nbrCommentaire.frame = CGRectMake(10,5, 30,30);
        self.nbrCommentaire.backgroundColor = [UIColor redColor];
        self.nbrCommentaire.alpha = 0.6;
        //            comBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        comBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 34, 0, 0);
        comBtn.layer.zPosition = 4;
        self.nbrCommentaire.layer.zPosition = 4;
        [cell.contentView addSubview:comBtn];
        [cell.contentView addSubview:shareBtn];
        [cell.contentView addSubview:tweetBtn];
        
        [cell.contentView addSubview:nbrCommentaire];
        
    }
    
    
    cell.titre.frame= CGRectMake(120, 10, self.view.frame.size.width- 130, 80);
    
    
    if (indexPath.row == 0) {
        if ([comArray valueForKeyPath:@"post0"] !=[NSNull null]) {
            NSString *encodedString = [[comArray valueForKeyPath:@"post0"] valueForKey:@"title"];
            DBGHTMLEntityDecoder *decoder = [[DBGHTMLEntityDecoder alloc] init];
            NSString *decodedString = [decoder decodeString:encodedString];
            cell.titre.text=decodedString;
            NSString * test = [NSString stringWithFormat:@"%@", [[[[comArray valueForKey:@"post0"] valueForKey:@"thumbnail_images"] valueForKey:@"full"] valueForKey:@"url"]];
            cell.img.imageURL = [NSURL URLWithString:[test stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        }
        
        else {
            cell.titre.text =@"aucun article connexe";
        }
        
        
    }
    if (indexPath.row == 1) {
        if ([comArray valueForKeyPath:@"post1"] !=[NSNull null]) {
            NSString *encodedString = [[comArray valueForKeyPath:@"post1"] valueForKey:@"title"];
            DBGHTMLEntityDecoder *decoder = [[DBGHTMLEntityDecoder alloc] init];
            NSString *decodedString = [decoder decodeString:encodedString];
            cell.titre.text=decodedString;
            NSString * test = [NSString stringWithFormat:@"%@", [[[[comArray valueForKey:@"post1"] valueForKey:@"thumbnail_images"] valueForKey:@"full"] valueForKey:@"url"]];
            cell.img.imageURL = [NSURL URLWithString:[test stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        }
        else {
            cell.titre.text =@"aucun article connexe";
        }
        
        
    }
    
    //
    if (indexPath.row == 2) {
        if ([comArray valueForKeyPath:@"post2"] !=[NSNull null]) {
            
            NSString *encodedString = [[comArray valueForKeyPath:@"post2"] valueForKey:@"title"];
            DBGHTMLEntityDecoder *decoder = [[DBGHTMLEntityDecoder alloc] init];
            NSString *decodedString = [decoder decodeString:encodedString];
            cell.titre.text=decodedString;
            NSString * test = [NSString stringWithFormat:@"%@", [[[[comArray valueForKey:@"post2"] valueForKey:@"thumbnail_images"] valueForKey:@"full"] valueForKey:@"url"]];
            cell.img.imageURL = [NSURL URLWithString:[test stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
            
        }
        
        else {
            cell.titre.text =@"aucun article connexe";
        }
        
    }
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RelatedPostViewController *reader = [self.storyboard instantiateViewControllerWithIdentifier:@"RelatedPostViewController"];
    if (indexPath.section == 0) {
        return;
    }
    if (indexPath.row ==0) {
        if ([comArray valueForKeyPath:@"post0"] !=[NSNull null]) {
            NSDictionary *intId =  [[comArray valueForKeyPath:@"post0"]  valueForKey:@"id"];
            NSString* articleId = [[NSString alloc] initWithFormat:@"%@",intId];
            reader.idArticle = articleId;
            
        }
        
        
    }
    if (indexPath.row == 1) {
        if ([comArray valueForKeyPath:@"post1"] !=[NSNull null]) {
            reader.idArticle = [[comArray valueForKeyPath:@"post1"] valueForKey:@"id"];
            
        }
        
    }
    if (indexPath.row == 2) {
        if ([comArray valueForKeyPath:@"post2"] !=[NSNull null]) {
            reader.idArticle = [[comArray valueForKeyPath:@"post2"] valueForKey:@"id"];
            
            
        }
        
    }
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:reader];
    
    [self.navigationController presentViewController:navBar animated:YES completion:nil];
    
    reader.typeArticle = @"related";
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section ==0) {
        return 80;
    }
    return 110;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return  1;
    }
    return 3;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.y + 650 >= scrollView.contentSize.width) {
        
        [self.detailScrollView addSubview:self.customTableView];
        [_detailScrollView bringSubviewToFront:self.customTableView];
        self.customTableView.layer.zPosition = 3;
        self.customTableView.scrollEnabled = NO;
        self.customTableView.bounces = NO;
        self.customTableView.backgroundColor = [UIColor clearColor];
        NSString *range ;
        range   = [NSString stringWithFormat:@"%@",[[appDelegate.accueilData objectAtIndex:index] content]];
        
        NSError *error = NULL;
        
//        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"iframe" options:NSRegularExpressionCaseInsensitive error:&error];
//        NSUInteger numberOfMatches = [regex numberOfMatchesInString:range options:0 range:NSMakeRange(0, [range length])];    NSLog(@"Found %lu",(unsigned long)numberOfMatches);

                
                self.customTableView.frame = CGRectMake(0, [[cWbw stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] floatValue], self.view.frame.size.width, 490);
              
                
    }
    
}

//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
//
//
//}
//

-(void)openUrlTarget:(UITapGestureRecognizer *) gestureRecognizer
{
    NSLog(@"url clicke");
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[appDelegate.pubData objectAtIndex:index]urlTarget]]];
}

- (void)adViewDidClick:(FBAdView *)adView
{
    //    NSLog(@"Ad was clicked.");
}

- (void)adViewDidFinishHandlingClick:(FBAdView *)adView
{
    //    NSLog(@"Ad did finish click handling.");
}

- (void)adViewDidLoad:(FBAdView *)adView
{
    //    NSLog(@"Ad was loaded.");
    // Now that the ad was loaded, show the view in case it was hidden before.
    self.adView.hidden = NO;
    
}

- (void)adView:(FBAdView *)adView didFailWithError:(NSError *)error
{
    self.adStatusLabel.text = @"Ad failed to load. Check console for details.";
    //    NSLog(@"Ad failed to load with error: %@", error);
    
    // Hide the unit since no ad is shown.
    self.adView.hidden = YES;
}

- (void)adViewWillLogImpression:(FBAdView *)adView
{
    //    NSLog(@"Ad impression is being captured.");
    
    
    
}

- (FBSDKShareLinkContent *)getShareLinkContentWithContentURL:(NSURL *)objectURL
{
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = objectURL;
    return content;
}



- (IBAction)postCom:(id)sender {
    //    cWbw.hidden= YES;
    if ([self.nbrCommentaire.text isEqualToString:@"0"]) {
        PostComTableViewController *post = [self.storyboard instantiateViewControllerWithIdentifier:@"PostComTableViewController"];
        post.postId=self.idArticle;
        [self.navigationController pushViewController:post animated:YES];
        
    }
    
    else {
        
        CommentaireTableViewController * com = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentaireTableViewController"];
        com.index=index;
        //        com.typeArticle= @"Buzz";
        com.titreArticle = self.idArticle;
        com.categorie = self.categorie;
        [self.navigationController pushViewController:com animated:YES];
        
    }
    
    //    [UIView  beginAnimations:nil context:NULL];
    //    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    //    [UIView setAnimationDuration:1.5];
    //    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.navigationController.view cache:NO];
    //    [UIView commitAnimations];
}


-(void)fadeIn:(UIView*)viewToFadeIn withDuration:(NSTimeInterval)duration  andWait:(NSTimeInterval)wait
{
    [UIView beginAnimations: @"Fade In" context:nil];
    
    // wait for time before begin
    [UIView setAnimationDelay:wait];
    
    // druation of animation
    [UIView setAnimationDuration:duration];
    viewToFadeIn.alpha = 1;
    [UIView commitAnimations];
    
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"post"])
    {
        
        if ([self.nbrCommentaire.text isEqualToString:@"0"]) {
            PostComTableViewController *post = [self.storyboard instantiateViewControllerWithIdentifier:@"PostComTableViewController"];
            post.postId= self.idArticle;
            [self.navigationController pushViewController:post animated:YES];
            
        }
        
        else {
            
            CommentaireTableViewController *dvController = [segue destinationViewController];
            dvController.index=index;
            dvController.typeArticle= @"categorie";
            dvController.titreArticle = self.idArticle;
            dvController.categorie = self.categorie;
            [self.navigationController pushViewController:dvController animated:YES];
            
        }
        //        [self.navigationController pushViewController:dvController animated:YES];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 2;
}




- (void)webViewDidFinishLoad:(UIWebView *)aWebView
{
    if (aWebView.tag!=1025) {
        
        
        self.customTableView.hidden= NO;
        aWebView.scrollView.scrollEnabled = NO;
        CGRect frame = aWebView.frame;
        frame.size.height=[[aWebView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] floatValue];
        aWebView.frame = frame;
        container=(UIScrollView *)aWebView.superview;
        NSString *range ;
        if ([self.typeArticle isEqualToString:@"related"]) {
            range =[[detArticle valueForKeyPath:@"posts"] valueForKey:@"content"];
        }
        
        else {
            range   = [NSString stringWithFormat:@"%@",[[appDelegate.accueilData objectAtIndex:index] content]];
        }
        
        NSError *error = NULL;
        
        
                    container.contentSize=CGSizeMake(container.frame.size.width, aWebView.scrollView.contentSize.height +  [[cWbw stringByEvaluatingJavaScriptFromString:@"document.getElementById(\"omar\").offsetHeight;"] integerValue] + 490);
                    
        
            //        container.backgroundColor = [UIColor blueColor];
            //        NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'", fontSize];
            //        [cWbw stringByEvaluatingJavaScriptFromString:jsString];
        }
        
        cWbw.hidden= NO;
        
        //        [self.detailScrollView bringSubviewToFront:cWbw];
        cWbw.layer.zPosition= 1;
        //        [self.detailScrollView addSubview:self.adView];
        self.adView.layer.zPosition=0;
        self.adView.frame = CGRectMake(8, [output integerValue]+270, self.view.frame.size.width -20, 50);
        [cWbw addSubview:self.adView];
        [cWbw bringSubviewToFront:self.adView];
        marginTop= output;
        RemoteImageView *imageView = [[RemoteImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)];
        if ([self.typeArticle isEqualToString:@"related"]) {
            NSString *articleImage = [NSString stringWithFormat:@"%@", [[[[detArticle valueForKey:@"posts"] valueForKey:@"thumbnail_images"] valueForKey:@"full"] valueForKey:@"url"]];
            imageView.imageURL =  [NSURL URLWithString:[articleImage   stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        }
        
        else {
            imageView.imageURL =  [NSURL URLWithString:[[[appDelegate.accueilData objectAtIndex:index]attachmentsUrl] stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        }
        UIImageView *shadow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"product-shadow.png"]];
        shadow.frame= CGRectMake(3, 152, imageView.frame.size.width - 2, 21);
        //        shadow.backgroundColor= [UIColor redColor];
        //        [imageView addSubview:shadow];
        [cWbw addSubview:imageView];
    
    
    
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section

{
    if (section == 0) {
        return nil;
    }
    
    return @"Articles en relation";
}




-(void)shareFace{
    //    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    //    content.contentURL = [NSURL URLWithString:[appDelegate.accueilData [index]urlHtml]];
    //    [FBSDKShareDialog showFromViewController:self
    //                                 withContent:content
    //                                    delegate:nil];
    
    //[FBSDKShareAPI shareWithContent:content delegate:nil];
    
    //    FBSDKShareDialog *shareDialog = [self getShareDialogWithContentURL:[NSURL URLWithString:[appDelegate.accueilData [index]urlHtml]]];
    //    shareDialog.delegate = self;
    //    [shareDialog show];
    NSString* text=@"Bonjour";
    NSURL *myWebsite = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[appDelegate.accueilData [index]urlHtml]]];
    //  UIImage * myImage =[UIImage imageNamed:@"myImage.png"];
    NSArray* sharedObjects=@[text,myWebsite];
    UIActivityViewController * activityViewController=[[UIActivityViewController alloc]initWithActivityItems:sharedObjects applicationActivities:nil];
    
    activityViewController.popoverPresentationController.sourceView = self.view;
    [self presentViewController:activityViewController animated:YES completion:nil];
}

- (FBSDKShareDialog *)getShareDialogWithContentURL:(NSURL *)objectURL
{
    FBSDKShareDialog *shareDialog = [[FBSDKShareDialog alloc] init];
    shareDialog.shareContent = [self getShareLinkContentWithContentURL:objectURL];
    return shareDialog;
}

-(void)loadContent:(int) i{
    for (UIWebView *wb in self.detailScrollView.subviews) {
        [wb removeFromSuperview];
    }
    cWbw= [[UIWebView alloc] initWithFrame:CGRectMake(0, 64, self.detailScrollView.frame.size.width-13, self.detailScrollView.frame.size.height + 190 ) ];
    cWbw.opaque = YES;
    fontSize=97;
    int imgWidth=self.view.frame.size.width;
    //    UILabel *titre=@"jjj";
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        fontSize=22;
        imgWidth=800;
    }
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    if ([self.typeArticle isEqualToString:@"push"]) {
        appDelegate =[[UIApplication sharedApplication] delegate];
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"idArticle = %@",_idArticle];
        appDelegate.accueilData = [[NSMutableArray alloc] initWithArray:[Accueil MR_findAllWithPredicate:predicate]];
    }
    
    if ([self.typeArticle isEqualToString:@"related"]) {
        appDelegate =[[UIApplication sharedApplication] delegate];
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"title = %@",_idArticle];
        appDelegate.accueilData = [[NSMutableArray alloc] initWithArray:[Accueil MR_findAllWithPredicate:predicate]];
    }
    
    
    
    if ([self.typeArticle isEqualToString:@"une"]) {
        appDelegate =[[UIApplication sharedApplication] delegate];
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"idArticle = %@",_idArticle];
        appDelegate.accueilData = [[NSMutableArray alloc] initWithArray:[Vedette MR_findAllWithPredicate:predicate]];
    }
    if ([self.typeArticle isEqualToString:@"Accueil"]) {
        
        appDelegate.accueilData=[[NSMutableArray alloc] initWithArray:[Accueil MR_findAll]];
    }
    
    if ([self.typeArticle isEqualToString:@"Video"]) {
        appDelegate.accueilData=[[NSMutableArray alloc] initWithArray:[Video MR_findAll]];
    }
    
    if ([self.typeArticle isEqualToString:@"Senego Tv"]) {
        appDelegate.accueilData=[[NSMutableArray alloc] initWithArray:[SenegoTv MR_findAll]];
    }
    
    if ([self.typeArticle isEqualToString:@"Buzz"]) {
        appDelegate.accueilData=[[NSMutableArray alloc] initWithArray:[Buzz MR_findAll]];
    }
    
    if ([self.typeArticle isEqualToString:@"Politique"]) {
        appDelegate.accueilData=[[NSMutableArray alloc] initWithArray:[Politique MR_findAll]];
    }
    
    if ([self.typeArticle isEqualToString:@"People"]) {
        appDelegate.accueilData=[[NSMutableArray alloc] initWithArray:[People MR_findAll]];
    }
    
    if ([self.typeArticle isEqualToString:@"Sports"]) {
        appDelegate.accueilData=[[NSMutableArray alloc] initWithArray:[Sports MR_findAll]];
    }
    
    if ([self.typeArticle isEqualToString:@"Afrique"]) {
        appDelegate.accueilData=[[NSMutableArray alloc] initWithArray:[Afrique MR_findAll]];
    }
    
    if ([self.typeArticle isEqualToString:@"International"]) {
        appDelegate.accueilData=[[NSMutableArray alloc] initWithArray:[International MR_findAll]];
    }
    
    if ([self.typeArticle isEqualToString:@"Revue de presse"]) {
        appDelegate.accueilData=[[NSMutableArray alloc] initWithArray:[Revue MR_findAll]];
    }
    
    if ([self.typeArticle isEqualToString:@"Buzz du jour"]) {
        appDelegate.accueilData=[[NSMutableArray alloc] initWithArray:[BuzzDuJour MR_findAll]];
    }
    
    if ([self.typeArticle isEqualToString:@"Buzz de la semaine"]) {
        appDelegate.accueilData=[[NSMutableArray alloc] initWithArray:[BuzzSemaine MR_findAll]];
    }
    
    [cWbw setDataDetectorTypes:UIDataDetectorTypeNone];
    //    NSString*   str3 = [[[appDelegate.accueilData objectAtIndex:index] content] stringByReplacingOccurrencesOfString:@"//www.dailymotion.com" withString:@"https://www.dailymotion.com"];
    [cWbw setScalesPageToFit:NO];
    NSString * contenu = [NSString stringWithFormat:@"%@",[[appDelegate.accueilData objectAtIndex:index] content]];
    NSString * daily = [contenu stringByReplacingOccurrencesOfString:@"//www.dailymotion.com" withString:@"https://www.dailymotion.com"];
    
    [cWbw loadHTMLString:[NSString stringWithFormat:@"<html>"
                          "<head>"
                          "<style type='text/css'>body {font-family: 'Play'; color:#444444; line-height:26px} img {width:%d; height:auto  } iframe {width:105%%; height:180} .text { margin-top : 76px ; font-size: 18px}</style>"
                          "</head>"
                          "<body></font><h2 style='line-height:29px; margin-top:210px'><div id='omar'><font face='Arial' size='5'>%@</font></h2></div><font face='Arial' size='2'>Par %@ | le %@</font></div><div class='text'><font face='Arial'>%@</body>"
                          "</html>",imgWidth,[[appDelegate.accueilData objectAtIndex:index] title_plain],[[appDelegate.accueilData objectAtIndex:index] authorName],[[appDelegate.accueilData objectAtIndex:index] date],daily] baseURL:baseURL];
    cWbw.delegate=self;
    //    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style"];
    //    self.detailScrollView.backgroundColor = [UIColor redColor];
    
}



//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
//

//    return 20;
//}


- (IBAction)ChangeFontSize:(id)sender {
    
    switch ([sender tag]) {
        case 0: // A-
            fontSize = (fontSize > 0) ? fontSize - 10 : fontSize;
            
            break;
        case 1: // A+
            fontSize = (fontSize < 160) ? fontSize +10 : fontSize;
            break;
    }
    
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'",fontSize];
    [cWbw stringByEvaluatingJavaScriptFromString:jsString];
    
    CGRect frame = cWbw.frame;
    frame.size.height=[[cWbw stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] floatValue];
    cWbw.frame = frame;
    //    container=(UIScrollView *)cWbw.superview;
    //    container.contentSize=CGSizeMake(container.frame.size.width, cWbw.scrollView.contentSize.height );
    
    
}




-(void)getRelatedArticle{
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://senego.com/relateds/getRelatedPosts.php?url=%@",[appDelegate.accueilData[index] urlHtml]]]];
    
    //    NSLog(@"related article %@",[appDelegate.accueilData[index] urlHtml]);
    id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    comArray =  jsonObjects;
    
    
}


@end
