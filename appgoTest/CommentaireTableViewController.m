//
//  CommentaireTableViewController.m
//
//
//  Created by Omar Doucouré on 28/03/2015.
//
//

#import "CommentaireTableViewController.h"
#import "AppDelegate.h"
#import "Accueil.h"
#import "CustomCell.h"
#import "Vedette.h"
#import "NSString+HTML.h"
#import "DBGHTMLEntityDecoder.h"
#import "PostComTableViewController.h"
#import "RJModel.h"
#import "DSTableViewWithDynamicHeight.h"
#import "ViewControllerDummyCustomCell.h"

#define IDENTIFIER_CUSTOM   @"CustomCell"
#define IDENTIFIER_DEFAULT  @"DefaultCell"
#define IDENTIFIER_SUBTITLE @"SubtitleCell"
#define NUM_ROWS            20
//#define PADDING             20
#define SUBTITLE_FONT_SIZE  12
#define TITLE_FONT_SIZE     12




@interface CommentaireTableViewController (){
    AppDelegate* appDelegate;
    NSMutableArray* comArray;
    NSMutableArray* heightsArray;
    NSString *decodedString;
    
    
}
@property (strong, nonatomic) NSMutableDictionary *offscreenCells;
@property (assign, nonatomic) BOOL isInsertingRow;
//@property (strong, nonatomic) RJModel *model;


@end

@implementation CommentaireTableViewController
@synthesize index,nbrCommentaire;

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [((DSTableViewWithDynamicHeight *)self.tableView) handleOrientationChange];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.toolbar setHidden:YES];
    self.nbrCommentaire.text =[appDelegate.accueilData [index]commentCount];
    self.tableView.backgroundColor =  [UIColor colorWithRed:0.859 green:0.855 blue:0.827 alpha:1] /*#dbdad3*/;
    self.title= @"Commentaires";
    //    NSLog(@"%@",self.titreArticle);
    
    UIBarButtonItem * item= [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Edit Filled-25.png"] style:UIBarButtonItemStylePlain target:self action:@selector(posterCom)];
    
    self.navigationItem.rightBarButtonItem = item;
    //    UIButton* posterUnCom = [[UIButton alloc] init];
    UIImageView* imgViewCirculaire= [[UIImageView alloc] initWithFrame:CGRectMake(0,self.view.frame.size.height-100, self.view.frame.size.width, 36)];
    imgViewCirculaire.image = [UIImage imageNamed:@"post-comment.png"];
//    [self.view addSubview:imgViewCirculaire];
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(posterCom)];
    gestureRecognizer.delegate = self;
    
    [imgViewCirculaire addGestureRecognizer:gestureRecognizer];
    [self.view bringSubviewToFront:imgViewCirculaire];
    imgViewCirculaire.layer.zPosition=2;
    [self getCom];

}




-(void)posterCom{
    PostComTableViewController *post = [self.storyboard instantiateViewControllerWithIdentifier:@"PostComTableViewController"];
    post.postId=self.titreArticle;
    [self.navigationController pushViewController:post animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

-(void)getCom{
    
    
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://senego.com/?json=get_post&post_id=%@",self.titreArticle]]];
    NSLog(@"titre article = %@",self.titreArticle);
    
    id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    
    comArray =  [[jsonObjects valueForKeyPath:@"post"] objectForKey:@"comments"];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"nbr de com %lu",(unsigned long)comArray.count);
    return comArray.count ;
    
}

- (void)setupCell:(UITableViewCell *)cell withText:(NSString *)text {
    cell.textLabel.text = text;
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.font = [UIFont systemFontOfSize:TITLE_FONT_SIZE];
    cell.detailTextLabel.text = @"dd";
    cell.detailTextLabel.numberOfLines = 0;
    cell.detailTextLabel.font = [UIFont fontWithName:@"Arial" size:6];
    
}

- (void)setupModel {
    NSString *text = @"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
    comArray = [NSMutableArray array];
    for (int i=0; i<NUM_ROWS-1; i++) {
        [comArray addObject:[NSString stringWithFormat:@"%@...", [text substringToIndex:30*(i+1)]]];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ViewControllerDummyCustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"commentaire"];
    
    UIView *separatorTop = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width+10, 10)];
    separatorTop.backgroundColor = [UIColor colorWithRed:0.859 green:0.855 blue:0.827 alpha:1] /*#dbdad3*/;
    UIView *separatorRight = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width -10, 0, 10, cell.frame.size.height)];
    separatorRight.backgroundColor = [UIColor colorWithRed:0.859 green:0.855 blue:0.827 alpha:1] /*#dbdad3*/;
    UIView *separatorLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, cell.frame.size.height)];
    separatorLeft.backgroundColor = [UIColor colorWithRed:0.859 green:0.855 blue:0.827 alpha:1] /*#dbdad3*/;
    
//    [cell.contentView addSubview:separatorLeft];
//    [cell.contentView addSubview:separatorRight];
//    
//    [cell.contentView addSubview:separatorTop];
    
    if (cell == nil) {
        cell = [[ViewControllerDummyCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"commentaire"];

    }
    
    //        NSLog(@"CATEGORIE");
    NSString *encodedString =[[comArray valueForKey:@"content"] objectAtIndex:indexPath.row ] ;
    DBGHTMLEntityDecoder *decoder = [[DBGHTMLEntityDecoder alloc] init];
    decodedString = [decoder decodeString:encodedString];
    decodedString = [decodedString stringByReplacingOccurrencesOfString:@"<p>"  withString:@""];
    decodedString = [decodedString stringByReplacingOccurrencesOfString:@"</p>"  withString:@""];
    decodedString = [decodedString stringByReplacingOccurrencesOfString:@"<br />"  withString:@""];

    cell.titre.text = decodedString;
    cell.titre2.text = [[comArray valueForKey:@"date"] objectAtIndex:indexPath.row ];
    cell.auteur.text = [[comArray valueForKey:@"name"] objectAtIndex:indexPath.row ];
    
    cell.titre.numberOfLines = 0;
    cell.titre.font = [UIFont fontWithName:@"Arial" size:16];
    cell.titre.frame = CGRectMake(10, 40, self.view.frame.size.width-55, 50);
    [cell.titre sizeToFit];
//    cell.titre.adjustsFontSizeToFitWidth = YES;
//    cell.titre.lineBreakMode = NSLineBreakByWordWrapping;

    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    //1. Setup the CATransform3D structure
    CATransform3D rotation;
    rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
    CGFloat test =(90.0*M_PI)/180;
    rotation.m34 = 1.0/ -600;
    
    
    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = rotation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    //!!!FIX for issue #1 Cell position wrong------------
    if(cell.layer.position.x != 0){
        cell.layer.position = CGPointMake(0, cell.layer.position.y);
    }
    
    //4. Define the final state (After the animation) and commit the animation
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.9];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}



//
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    DSTableViewWithDynamicHeight *table = (DSTableViewWithDynamicHeight *)self.tableView;
     CGFloat height;

    height = [table heightForCellWithDefaultStyleAndText:[[comArray valueForKey:@"content"] objectAtIndex:indexPath.row ] font:[UIFont systemFontOfSize:15]];

    return height + 60;
}


@end
