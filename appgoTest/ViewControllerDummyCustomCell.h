//
//  ViewControllerDummyCustomCell.h
//  DSTableViewWithDynamicHeightDemo
//
//  Created by Daniel Saidi on 2014-02-20.
//  Copyright (c) 2014 Daniel Saidi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DSTableViewCellWithDynamicHeight.h"

@interface ViewControllerDummyCustomCell : UITableViewCell<DSTableViewCellWithDynamicHeight>

@property (strong, nonatomic) IBOutlet UILabel *titre;
@property (strong, nonatomic) IBOutlet UILabel *titre2;
@property (strong, nonatomic) IBOutlet UILabel *nbrdecom;
@property (strong, nonatomic) IBOutlet UILabel *categorie;

@property (strong, nonatomic) IBOutlet UILabel *commentaires;
@property (strong, nonatomic) IBOutlet UILabel *auteur;

@end
