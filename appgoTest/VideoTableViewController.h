//
//  VideoTableViewController.h
//  appgoTest
//
//  Created by Omar Doucouré on 20/04/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@class DFPBannerView;
@class CBStoreHouseRefreshControl;

@interface VideoTableViewController : UITableViewController <SlideNavigationControllerDelegate,UIToolbarDelegate, UIBarPositioningDelegate,UIScrollViewDelegate>


@property (nonatomic, strong) CBStoreHouseRefreshControl *storeHouseRefreshControl;

@property (nonatomic, strong) IBOutlet UISwitch *limitPanGestureSwitch;
@property (nonatomic, strong) IBOutlet UISwitch *slideOutAnimationSwitch;
@property (nonatomic, strong) IBOutlet UISwitch *shadowSwitch;
@property (nonatomic, strong) IBOutlet UISwitch *panGestureSwitch;
@property (nonatomic, strong) IBOutlet UISegmentedControl *portraitSlideOffsetSegment;
@property (nonatomic, strong) IBOutlet UISegmentedControl *landscapeSlideOffsetSegment;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *ds;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
//@property(nonatomic, weak) IBOutlet DFPBannerView *bannerView;
@property(nonatomic, weak) IBOutlet DFPBannerView *bannerView2;

@end
