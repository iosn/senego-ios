//
//  ViewControllerDummyCustomCell.m
//  DSTableViewWithDynamicHeightDemo
//
//  Created by Daniel Saidi on 2014-02-20.
//  Copyright (c) 2014 Daniel Saidi. All rights reserved.
//

#import "ViewControllerDummyCustomCell.h"

@implementation ViewControllerDummyCustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.textLabel.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor lightGrayColor];
    }
    return self;
}

+ (CGFloat)heightForCellWidth:(CGFloat)cellWidth {
    return cellWidth / 2;
};

@end
