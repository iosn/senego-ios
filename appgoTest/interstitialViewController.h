//
//  interstitialViewController.h
//  appgoTest
//
//  Created by Omar Doucouré on 28/02/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface interstitialViewController : UIViewController <UIApplicationDelegate>

-(void) playSound;


@end
