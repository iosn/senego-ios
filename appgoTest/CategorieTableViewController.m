//
//  CategorieTableViewController.m
//  appgoTest
//
//  Created by Omar Doucouré on 18/02/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//


#import "CategorieTableViewController.h"
#import "Parser.h"
#import "AppDelegate.h"
//#import "Articles.h"
#import "CustomCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DetailsArticleViewController.h"
#import "LeftMenuViewController.h"
#import "DBGHTMLEntityDecoder.h"
#import "UITableView+Wave.h"
#import "ODRefreshControl.h"
#import "Accueil.h"


@interface CategorieTableViewController () {
    AppDelegate* appDelegate;
    
}


@end



@implementation CategorieTableViewController
//@synthesize categorie;

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.emailPop) {
        NSString *emailTitle = @"Test Email";
        // Email Content
        NSString *messageBody = @"iOS programming is so fun!";
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:@"info@finetechnosoft.in"];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    
    ODRefreshControl *refreshControl = [[ODRefreshControl alloc] initInScrollView:self.tableView];
    [refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];

    self.tableView.dataSource = self;
    self.tableView.delegate= self;
    self.navigationController.navigationBar.barTintColor =[UIColor whiteColor] /*#32add6*/;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Play" size:32],
      NSFontAttributeName, nil]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handle_data) name:@"reload_categorie" object:nil];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor orangeColor]};
    appDelegate= [[UIApplication sharedApplication] delegate];
    NSString *predicateString = appDelegate.idMenuDelegate;
//    appDelegate.categorieData=[[NSMutableArray alloc] initWithArray:[Categories MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"categoriesId == %@",predicateString]]];
//    NSLog(@"categorie id delegte = %@",appDelegate.idMenuDelegate);
    [self.tableView reloadData];
    self.title =[NSString stringWithFormat:@"%@",self.Titrecategorie];
    [self.navigationController.toolbar setHidden:NO];

//    [self receiveTestNotification:nil];
    self.tableView.backgroundColor =  [UIColor colorWithRed:0.859 green:0.855 blue:0.827 alpha:1] /*#dbdad3*/;
    
    UIImageView* logoImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo-14-02.png"]];
    //    logoImage.frame= CGRectMake(0, 0, 50, 20);
    self.navigationItem.titleView = logoImage;
}

#pragma mark - SlideNavigationController Methods -
//
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

-(void)handle_data
{
    //    [self viewDidLoad];
    [self.tableView reloadData];
    //    [self.tableView setcontentffset:CGPointMake(0.0, self.tableView.contentSize.height - self.tableView.bounds.size.height)
    //                   animated:NO];
    
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionBottom
                                  animated:YES];
//    NSLog(@"categorie id delegte = %@",appDelegate.idMenuDelegate);

//    [self viewDidLoad];
}


- (void)dropViewDidBeginRefreshing:(ODRefreshControl *)refreshControl
{

    double delayInSeconds = 7;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [refreshControl endRefreshing];
        Parser *dataParser = [[Parser alloc] init];
        [dataParser getMenuDataTarget];
        [dataParser getCategorieAddTarget];
        
        [dataParser pubData];
    });
}


-(void)refreshTableView: (NSString *)status{
    //    if ([status isEqualToString:@"200"]) {
    //                [ZAActivityBar showSuccessWithStatus:@"Mise à jour avec succès"];
    //
    //    } else if ([status isEqualToString:@"500"]) {
    //        [ZAActivityBar showErrorWithStatus:@"Mise à jour échoué. Vérifiez votre accès internet."];
    //    }
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (section == 1) {

        NSString *predicateString = appDelegate.idMenuDelegate;
        appDelegate.accueilData=[[NSMutableArray alloc] initWithArray:[Accueil MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"categoriesTitle == %@",predicateString]]];
   
        return [[appDelegate.accueilData [1] nbrElement]  integerValue]-1;
        
    }
    
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    NSLog(@"categorie id = %@",[appDelegate.categorieData [indexPath.row]categoriesId]);
    
    static NSString *CellIdentifier=@"une";
    
    if (indexPath.section == 0) {
        CellIdentifier=@"une";
    }
    
    if (indexPath.section==1) {
        CellIdentifier=@"autres";
    }
    
    
    
    //    [self.tableView reloadData];
    CustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    UIView *separatorTop = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width+10, 5)];
    separatorTop.backgroundColor = [UIColor colorWithRed:0.859 green:0.855 blue:0.827 alpha:1] /*#dbdad3*/;
    UIView *separatorRight = [[UIView alloc] initWithFrame:CGRectMake(310, 0, 10, cell.frame.size.height)];
    separatorRight.backgroundColor = [UIColor colorWithRed:0.859 green:0.855 blue:0.827 alpha:1] /*#dbdad3*/;
    UIView *separatorLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, cell.frame.size.height)];
    separatorLeft.backgroundColor = [UIColor colorWithRed:0.859 green:0.855 blue:0.827 alpha:1] /*#dbdad3*/;
    
    [cell.contentView addSubview:separatorLeft];
    [cell.contentView addSubview:separatorRight];
    
    [cell.contentView addSubview:separatorTop];
    // j'appel la classe Custom cell, puis je lui donne un cell Id pour une reutilisation.
    
    if (cell == nil) {
        cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    //    appDelegate= [[UIApplication sharedApplication] delegate];
    //    appDelegate.categorieData= [[NSMutableArray alloc]initWithArray:[Categories MR_findAll] ];
    
    if (cell == nil) {
        cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
//    if ([appDelegate.idMenuDelegate isEqualToString:@"10"]) {
//        NSString *predicateString = @"Buzz du jour";
//        appDelegate.categorieData=[[NSMutableArray alloc] initWithArray:[Categories MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"categoriesTitle == %@",predicateString]]];
//    }
//    else {
    appDelegate= [[UIApplication sharedApplication] delegate];
    NSString *predicateString = appDelegate.idMenuDelegate;
    appDelegate.accueilData=[[NSMutableArray alloc] initWithArray:[Accueil MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"categoriesTitle == %@",predicateString]]];
//    }
    if (indexPath.section==0) {
        NSString *encodedString = [[appDelegate.accueilData objectAtIndex:indexPath.row] title_plain] ;
        DBGHTMLEntityDecoder *decoder = [[DBGHTMLEntityDecoder alloc] init];
        NSString *decodedString = [decoder decodeString:encodedString];
        
        cell.titre.text=decodedString;
        [cell.img sd_setImageWithURL:[NSURL URLWithString:[[appDelegate.accueilData objectAtIndex:0]attachmentsUrl]]  placeholderImage:[UIImage imageNamed:@"logo-app-450x111"]];
        //    NSLog(@"test  %@",[[appDelegate.articleData objectAtIndex:indexPath.row]attachmentsUrl]);
        NSString* date = [appDelegate.accueilData [indexPath.row] date];
        
        cell.img.frame = CGRectMake(15, 13, self.view.frame.size.width-32, 150);
        cell.auteur.text = [NSString stringWithFormat:@"Par %@ le %@",[appDelegate.accueilData [0]authorName],[date substringToIndex:13]];
        
    }
    if (indexPath.section == 1) {
        NSString *encodedString = [[appDelegate.accueilData objectAtIndex:indexPath.row+1] title_plain] ;
        DBGHTMLEntityDecoder *decoder = [[DBGHTMLEntityDecoder alloc] init];
        NSString *decodedString = [decoder decodeString:encodedString];
        cell.titre.text=decodedString;
        NSString* date = [appDelegate.accueilData [indexPath.row+1] date];
        cell.auteur.text=[NSString stringWithFormat:@"Par %@ le %@",[appDelegate.accueilData [indexPath.row+1]authorName],[date substringToIndex:13]];
        cell.img.alpha = .0f;
//        NSLog(@"%@",[appDelegate.menuData [indexPath.row] titre]);
        
        // Then fades it away after 2 seconds (the cross-fade animation will take 0.5s)
        [UIView animateWithDuration:0.5 delay:.0 options:0 animations:^{
            // Animate the alpha value of your imageView from 1.0 to 0.0 here
            cell.img.alpha = 1.0f;
        } completion:^(BOOL finished) {
            // Once the animation is completed and the alpha has gone to 0.0, hide the view for good
            cell.img.hidden = NO;
        }];
        
        cell.categorie.text=[[appDelegate.accueilData objectAtIndex:indexPath.row+1] categoriesTitle];
        if ([[[appDelegate.accueilData objectAtIndex:indexPath.row+1] categoriesTitle] isEqualToString:@"Vidéos"]) {
            UIImageView* playerVideo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"play_button.png"]];
            playerVideo.frame= CGRectMake(50, 30, 30, 30);
            [cell.contentView addSubview:playerVideo];
        }
        //        cell.auteur.text= [auteur]
        if (![[[appDelegate.accueilData objectAtIndex:indexPath.row+1] commentCount] isEqualToString:@"0"]) {
            cell.nbrdecom.text= [[appDelegate.accueilData objectAtIndex:indexPath.row+1] commentCount];
        }
        else {
            cell.nbrdecom.hidden= YES;
            cell.iconCom.hidden= YES;
        }
        [cell.img sd_setImageWithURL:[NSURL URLWithString:[[appDelegate.accueilData objectAtIndex:indexPath.row+1]attachmentsUrl]]  placeholderImage:[UIImage imageNamed:@"a1aWNvbnMvMS9mMjdhNDZmOTBhNDAyOGYzMjFjZTc1ZTVjMmZiMDdkMQ==.png"]];    }
    
    
    // Configure the cell...
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Hauteur de chaque cellule.
    if (indexPath.section == 0) {
        return 260;
    }
    
    return 85;
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//
//
//    //1. Setup the CATransform3D structure
//    CATransform3D rotation;
//    rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
//    CGFloat test =(90.0*M_PI)/180;
//    rotation.m34 = 1.0/ -600;
//
//
//    //2. Define the initial state (Before the animation)
//    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
//    cell.layer.shadowOffset = CGSizeMake(10, 10);
//    cell.alpha = 0;
//
//    cell.layer.transform = rotation;
//    cell.layer.anchorPoint = CGPointMake(0, 0.5);
//
//    //!!!FIX for issue #1 Cell position wrong------------
//    if(cell.layer.position.x != 0){
//        cell.layer.position = CGPointMake(0, cell.layer.position.y);
//    }
//
//    //4. Define the final state (After the animation) and commit the animation
//    [UIView beginAnimations:@"rotation" context:NULL];
//    [UIView setAnimationDuration:1];
//    cell.layer.transform = CATransform3DIdentity;
//    cell.alpha = 1;
//    cell.layer.shadowOffset = CGSizeMake(0, 0);
//    [UIView commitAnimations];
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DetailsArticleViewController *reader = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailsArticleViewController"];
    if (indexPath.section==1) {
        reader.index=indexPath.row+1;
        reader.typeArticle= @"categorie";
        reader.titreArticle =[appDelegate.accueilData [indexPath.row+1] title_plain];
        reader.categorie = [NSString stringWithFormat:@"%@",[[appDelegate.accueilData objectAtIndex:indexPath.row+1] categoriesTitle]];
        reader.idArticle = [appDelegate.accueilData [indexPath.row+1] idArticle];
        
        
    }
    else {
        reader.index=indexPath.row;
        reader.typeArticle= @"categorie";
        reader.titreArticle =[appDelegate.accueilData [indexPath.row] title_plain];
        reader.categorie = [NSString stringWithFormat:@"%@",[[appDelegate.accueilData objectAtIndex:indexPath.row] categoriesTitle]];
        reader.idArticle = [appDelegate.accueilData [indexPath.row] idArticle];

    }
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:reader];
    
    [self.navigationController presentViewController:navBar animated:YES completion:nil];
    
}



/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
