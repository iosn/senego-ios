//
//  AppDelegate.m
//  appgoTest
//
//  Created by Omar Doucouré on 15/02/2015.
//  Copyright (c) 2015 Omar Doucouré. All rights reserved.
//

#import "AppDelegate.h"
#import "Articles.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import <FBSDKShareKit/FBSDKShareKit.h>
#import "DetailsPushViewController.h"
#import "BuzzDuJourTableViewController.h"
#import "AccueilTableViewController.h"
#import "VideoTableViewController.h"
#import "SenegoTvTableViewController.h"
#import "BuzzDuJourTableViewController.h"
#import "PolitiqueTableViewController.h"
#import "RevueTableViewController.h"
#import "BuzzSemaineTableViewController.h"
#import "SportsTableViewController.h"
#import "InternationalTableViewController.h"
#import "AfriqueTableViewController.h"
#import "BuzzTableViewController.h"
#import "PeopleTableViewController.h"


/******* Set your tracking ID here *******/
static NSString *const kTrackingId = @"UA-67544330-1";
static NSString *const kAllowTracking = @"allowTracking";

//#import "GADInterstitial.h"
@import GoogleMobileAds;
#define GAD_SIMULATOR_ID @"Simulator"

@interface AppDelegate () <GADInterstitialDelegate>{
}

@property(nonatomic, strong) DFPInterstitial *interstitial;
@property(nonatomic, assign) BOOL okToWait;
@property(nonatomic, copy) void (^dispatchHandler)(GAIDispatchResult result);
//@property (strong, nonatomic) FeEqualize *equalizer;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    UILocalNotification *notification = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if (notification) {
        // launched from notification
        _launchFromPush = YES;
        
    } else {
        // from the springboard
        _launchFromPush = NO;
        NSString *valueToSave = @"false";
        [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"fromPush"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
    
    
    //        UILocalNotification *notification = launchOptions[UIApplicationLaunchOptionsLocalNotificationKey];
    
    //        if (notification) {
    // launched from notification
    //        } else {
    //            // from the springboard
    //            NSLog(@"from spring");
    //
    //        }
    
    
    [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"appGoTest"];
    //    [FBSDKLikeControl class];
    //    [FBSDKLoginButton class];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    //    return [[FBSDKApplicationDelegate sharedInstance] application:application
    //                                    didFinishLaunchingWithOptions:launchOptions];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    //    self.interstitial = [self createAndLoadInterstitial];
    
    LeftMenuViewController *leftMenu = (LeftMenuViewController*)[mainStoryboard
                                                                 instantiateViewControllerWithIdentifier: @"LeftMenuViewController"];
    
    [SlideNavigationController sharedInstance].leftMenu = leftMenu;
    
    
    
    //    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
    //        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
    //                                                                                             |UIRemoteNotificationTypeSound
    //                                                                                             |UIRemoteNotificationTypeAlert) categories:nil];
    //        [application registerUserNotificationSettings:settings];
    //    } else {
    //        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
    //        [application registerForRemoteNotificationTypes:myTypes];
    //    }
    //     [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
    //     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    //-- Set Notification
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:		(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval = 20;
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-67544330-1"];
    return YES;
}

- (void)sendHitsInBackground {
    self.okToWait = YES;
    __weak AppDelegate *weakSelf = self;
    __block UIBackgroundTaskIdentifier backgroundTaskId =
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        weakSelf.okToWait = NO;
    }];
    
    if (backgroundTaskId == UIBackgroundTaskInvalid) {
        return;
    }
    
    self.dispatchHandler = ^(GAIDispatchResult result) {
        // If the last dispatch succeeded, and we're still OK to stay in the background then kick off
        // again.
        if (result == kGAIDispatchGood && weakSelf.okToWait ) {
            [[GAI sharedInstance] dispatchWithCompletionHandler:weakSelf.dispatchHandler];
        } else {
            [[UIApplication sharedApplication] endBackgroundTask:backgroundTaskId];
        }
    };
    [[GAI sharedInstance] dispatchWithCompletionHandler:self.dispatchHandler];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}



- (GADInterstitial *)createAndLoadInterstitial {
    self.interstitial = [[DFPInterstitial alloc] init];
    self.interstitial.adUnitID = @"ca-app-pub-2842182435104673/2527999662";
    self.interstitial.delegate = self;
    [self.interstitial loadRequest:[DFPRequest request]];
    
    return _interstitial;
}


- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    //        self.interstitial = [self createAndLoadInterstitial];
}

//- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
////    NSLog(@"interstitialDidDismissScreen");
////    AccueilTableViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AccueilTableViewController"];
////    [self.navigationController pushViewController:vc animated:YES];
//}


- (void)interstitialDidReceiveAd:(GADInterstitial *)interstitial {
    // call presentFromRootViewController here...
    [interstitial presentFromRootViewController:self.window.rootViewController];
    //    [self dismissViewControllerAnimated:YES completion:NULL];
    //    [self createAndLoadInterstitial];
    
}


//- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
//{
//    //handle the actions
//    if ([identifier isEqualToString:@"declineAction"]){
//    }
//    else if ([identifier isEqualToString:@"answerAction"]){
//    }
//}

//- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
//{
//    //register to receive notifications
//    [application registerForRemoteNotifications];
//}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    
        NSLog(@"My token is: %@", deviceToken);
    NSString *token = [deviceToken description];
    token = [token stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"deviceToken"];
    
    NSData *postData = [[NSString stringWithFormat:@"token=%@", deviceToken] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    //Don't forget to change the API link to your own link
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:
                                          @"http://push2.sportiso.com/push/savetoken/?device_token=%@&device_type=ios&channels_id=1", token]]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    NSURLConnection *conn = [[NSURLConnection alloc]initWithRequest:request delegate:self];
    
    NSLog(@"My device token is: %@", deviceToken);
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    NSLog(@"applicationWillResignActive");
    
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
    
    NSString *total = [userInfo objectForKey:@"aps"];
    NSString *idPush = [total valueForKey:@"id"];
    NSString *idType = [total valueForKey:@"type"];
    
    if (idPush == nil) {
        NSLog(@"ya rien");
    }
    
    else {
        if (_launchFromPush == YES && idType== nil) {
            NSLog(@"from push mais pas de categorie");
            
            //            AccueilTableViewController * det = [mainStoryboard instantiateViewControllerWithIdentifier:@"AccueilTableViewController"];
            //            [navigationController pushViewController:det animated:YES];
            NSString *valueToSave = @"true";
            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"fromPush"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSString *valueToSave2 = idPush;
            [[NSUserDefaults standardUserDefaults] setObject:valueToSave2 forKey:@"pushId"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSUserDefaults standardUserDefaults] setObject:@"false" forKey:@"categorie"];
            
        }
        
        else  if ( idType != nil) {
            NSLog(@"launch yes categorie");
            NSString *valueToSave = @"true";
            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"fromPush"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSString *valueToSave2 = idPush;
            [[NSUserDefaults standardUserDefaults] setObject:valueToSave2 forKey:@"pushId"];
            
            UIApplicationState state = [[UIApplication sharedApplication] applicationState];
            if (state == UIApplicationStateBackground || state == UIApplicationStateActive )
            {
                
                if ([idPush isEqualToString:@"1"]) {
                    [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    AccueilTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"AccueilTableViewController"];
                    [navigationController pushViewController:buzz animated:YES];
                    
                }
                
                else  if ([idPush isEqualToString:@"2"]) {
                    [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    VideoTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"VideoTableViewController"];
                    [navigationController pushViewController:buzz animated:YES];
                    
                }
                
                else  if ([idPush isEqualToString:@"3"]) {
                    [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    SenegoTvTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"SenegoTvTableViewController"];
                    [navigationController pushViewController:buzz animated:YES];
                    
                }
                
                else   if ([idPush isEqualToString:@"4"]) {
                    [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    BuzzTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"BuzzTableViewController"];
                    [navigationController pushViewController:buzz animated:YES];
                    
                }
                
                
                else  if ([idPush isEqualToString:@"5"]) {
                    [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    PolitiqueTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"PolitiqueTableViewController"];
                    [navigationController pushViewController:buzz animated:YES];
                    
                }
                
                else  if ([idPush isEqualToString:@"6"]) {
                    [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    PeopleTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"PeopleTableViewController"];
                    [navigationController pushViewController:buzz animated:YES];
                    
                }
                
                else  if ([idPush isEqualToString:@"7"]) {
                    [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    SportsTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"SportsTableViewController"];
                    [navigationController pushViewController:buzz animated:YES];
                    
                }
                
                else    if ([idPush isEqualToString:@"8"]) {
                    [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    AfriqueTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"AfriqueTableViewController"];
                    [navigationController pushViewController:buzz animated:YES];
                    
                }
                
                else    if ([idPush isEqualToString:@"9"]) {
                    [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    InternationalTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"InternationalTableViewController"];
                    [navigationController pushViewController:buzz animated:YES];
                    
                }
                
                else  if ([idPush isEqualToString:@"10"]) {
                    [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    RevueTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"RevueTableViewController"];
                    [navigationController pushViewController:buzz animated:YES];
                    
                }
                
                else   if ([idPush isEqualToString:@"11"]) {
                    BuzzDuJourTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"BuzzDuJourTableViewController"];
                    [navigationController pushViewController:buzz animated:YES];
                    
                }
                
                else     if ([idPush isEqualToString:@"12"]) {
                    BuzzSemaineTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"BuzzSemaineTableViewController"];
                    [navigationController pushViewController:buzz animated:YES];
                    
                }
            }
        }
        //    else {
        //
        //        NSString *valueToSave = @"true";
        //
        //        if ([idPush isEqualToString:@"1"]) {
        //            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
        //            [[NSUserDefaults standardUserDefaults] synchronize];
        //
        //            AccueilTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"AccueilTableViewController"];
        //            [navigationController pushViewController:buzz animated:YES];
        //
        //        }
        //
        //        else  if ([idPush isEqualToString:@"2"]) {
        //            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
        //            [[NSUserDefaults standardUserDefaults] synchronize];
        //            VideoTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"VideoTableViewController"];
        //            [navigationController pushViewController:buzz animated:YES];
        //
        //        }
        //
        //        else  if ([idPush isEqualToString:@"3"]) {
        //            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
        //            [[NSUserDefaults standardUserDefaults] synchronize];
        //            SenegoTvTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"SenegoTvTableViewController"];
        //            [navigationController pushViewController:buzz animated:YES];
        //
        //        }
        //
        //        else   if ([idPush isEqualToString:@"4"]) {
        //            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
        //            [[NSUserDefaults standardUserDefaults] synchronize];
        //            BuzzTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"BuzzTableViewController"];
        //            [navigationController pushViewController:buzz animated:YES];
        //
        //        }
        //
        //
        //        else  if ([idPush isEqualToString:@"5"]) {
        //            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
        //            [[NSUserDefaults standardUserDefaults] synchronize];
        //            PolitiqueTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"PolitiqueTableViewController"];
        //            [navigationController pushViewController:buzz animated:YES];
        //
        //        }
        //
        //        else  if ([idPush isEqualToString:@"6"]) {
        //            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
        //            [[NSUserDefaults standardUserDefaults] synchronize];
        //            PeopleTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"PeopleTableViewController"];
        //            [navigationController pushViewController:buzz animated:YES];
        //
        //        }
        //
        //        else  if ([idPush isEqualToString:@"7"]) {
        //            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
        //            [[NSUserDefaults standardUserDefaults] synchronize];
        //            SportsTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"SportsTableViewController"];
        //            [navigationController pushViewController:buzz animated:YES];
        //
        //        }
        //
        //        else    if ([idPush isEqualToString:@"8"]) {
        //            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
        //            [[NSUserDefaults standardUserDefaults] synchronize];
        //            AfriqueTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"AfriqueTableViewController"];
        //            [navigationController pushViewController:buzz animated:YES];
        //
        //        }
        //
        //        else    if ([idPush isEqualToString:@"9"]) {
        //            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
        //            [[NSUserDefaults standardUserDefaults] synchronize];
        //            InternationalTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"InternationalTableViewController"];
        //            [navigationController pushViewController:buzz animated:YES];
        //
        //        }
        //
        //        else  if ([idPush isEqualToString:@"10"]) {
        //            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"categorie"];
        //            [[NSUserDefaults standardUserDefaults] synchronize];
        //
        //            RevueTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"RevueTableViewController"];
        //            [navigationController pushViewController:buzz animated:YES];
        //
        //        }
        //
        //        else   if ([idPush isEqualToString:@"11"]) {
        //            BuzzDuJourTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"BuzzDuJourTableViewController"];
        //            [navigationController pushViewController:buzz animated:YES];
        //
        //        }
        //
        //        else     if ([idPush isEqualToString:@"12"]) {
        //            BuzzSemaineTableViewController * buzz = [mainStoryboard instantiateViewControllerWithIdentifier:@"BuzzSemaineTableViewController"];
        //            [navigationController pushViewController:buzz animated:YES];
        //
        //        }
        
        else if (idType == nil) {
            NSLog(@"details push direct");
            
            DetailsPushViewController * det = [mainStoryboard instantiateViewControllerWithIdentifier:@"DetailsPushViewController"];
            det.typeArticle = @"push";
            det.idArticle = idPush;
            det.index = 0;
            [navigationController pushViewController:det animated:YES];
        }
        //        }
    }
    
    //    det.urlFace.text= [[appDelegate.vedetteData objectAtIndex:0] urlHtml];
    //    reader.titreArticle=[[appDelegate.vedetteData objectAtIndex:0] title];
    //    reader.nbrCommentaire.text=[[appDelegate.vedetteData objectAtIndex:indexPath.row] commentCount];
    //    reader.idArticle = [[appDelegate.vedetteData objectAtIndex:indexPath.row] idArticle];
    NSLog(@"background Notification -- %@",userInfo);
    //    }
    
}



-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    //When notification is pressed on background it will enter here
    
    UIAlertView * pushAlert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Vous venez de recevoir un push" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    //    [pushAlert show];
    
    //Get strings based on information on your json payload for example
    if([[userInfo objectForKey:@"keyword"] isEqualToString:@"value"]){
        //redirect/push a screen here for example
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    //        self.interstitial = [self createAndLoadInterstitial];
    NSLog(@"applicationDidEnterBackground");
    
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    NSLog(@"applicationWillEnterForeground");
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    self.interstitial = [self createAndLoadInterstitial];
    NSLog(@"applicationDidBecomeActive");
    [GAI sharedInstance].optOut =
    ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
    
    
    
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "test.appgoTest" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"appgoTest" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"appgoTest.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
